////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Shalender Kumar, Sujay Pandit, Lokhesh Kumar
Email id: sk29108@gmail.com, contact.sujaypandit@gmail.com, lokhesh.kumar@gmail.com
--------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
  `include "Logger.bsv"
package fpu_fma;
import normalize_fma :: * ;
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import DReg  :: *;
`include "fpu_parameters.bsv"

////////////////////////////////////////////////////////////////////////////////
/// Floating point fused multiple accumulate
////////////////////////////////////////////////////////////////////////////////

/*doc:func: Actual function that perform a*b-c for single precision. */
function ActionValue#(Tuple2#(FloatingPoint#(8,23),Exception)) fn_fpu_fma_sp(Tuple4#(
		 FloatingPoint#(8,23),
		 FloatingPoint#(8,23),
		 Maybe#(FloatingPoint#(8,23)),
		 RoundMode) operands)=actionvalue
      //Here this function computes (opA*opB+opC).
      match {.opA, .opB, .mopC, .rmode } = operands;
      CommonState#(8,23) s = CommonState {
	 res: tagged Invalid,
	 exc: defaultValue,
	 rmode: rmode
	 };
	`logLevel( tb, 0, $format("function called"))
      Bool acc = False;
      FloatingPoint#(8,23) opC = 0;

      if (mopC matches tagged Valid .opC_) begin
	 opC = opC_;
	 acc = True;
      end

	//zeroC indicates whether operand C is zero or not.
	Bool zeroC = (opC.exp == 0 && opC.sfd == 0);
	//If number is subnormal then exponent will become minimum exponent. Minimum exponent is -126 for sp and -1022 for dp. If number is not subnormal then unbias it. bias is 127 for sp and 1023 for dp.
      Int#(10) expA = isSubNormal(opA) ? fromInteger(minexp(opA)) : signExtend(unpack(unbias(opA)));
      Int#(10) expB = isSubNormal(opB) ? fromInteger(minexp(opB)) : signExtend(unpack(unbias(opB)));
      

      //significand is simply taken from sfd part of operand. Hidden bit is either 0 or 1. It is 0 for subnormal number and 1 for normal number.	subnormal number have exponent as zero.
      Bit#(24) sfdA = { getHiddenBit(opA), opA.sfd };
      Bit#(24) sfdB = { getHiddenBit(opB), opB.sfd };

      //sgnAB maintains sign of product. 
      Bool sgnAB = opA.sign != opB.sign;

      //infAB indicates whether result of product is infinity or not. 
      Bool infAB = isInfinity(opA) || isInfinity(opB);
      //zeroAB indicates product is zero or not.
      Bool zeroAB = isZero(opA) || isZero(opB);
      //bigEXP for underflow purpose.
      Bool bigEXP = False;
      //Product operation add exponents of both operands and multiply mantissa. 
      Int#(10) expAB = expA + expB;


	if(((isZero(opA) && isInfinity(opB)) || (isZero(opB) && isInfinity(opA)) ) && isNaN(opC))
		s.exc.invalid_op = True;
      //if opC is signalling NaN then don't go for addition part. quiet nan form of opC will be the final answer.
      if (isSNaN(opC)) begin
	 s.res = tagged Valid nanQuiet(opC);
	 s.exc.invalid_op = True;
      end
      //if opA is signalling NaN then it's quiet nan is the final answer. 
      else if (isSNaN(opA)) begin
	 s.res = tagged Valid nanQuiet(opA);
	 s.exc.invalid_op = True;
      end
      //if opB is signalling NaN then it's Quiet nan is the final answer. 
      else if (isSNaN(opB)) begin
	 s.res = tagged Valid nanQuiet(opB);
	 s.exc.invalid_op = True;
      end
      //Similar cases when opA, opB or opC is quiet NaN. But in these cases invalid flag will not set.
      else if (isQNaN(opC)) begin
	 s.res = tagged Valid opC;
      end
      else if (isQNaN(opA)) begin
	 s.res = tagged Valid opA;
      end
      else if (isQNaN(opB)) begin
	 s.res = tagged Valid opB;
      end
      
      else if ((isInfinity(opA) && isZero(opB)) || (isZero(opA) && isInfinity(opB)) || (isInfinity(opC) && infAB && (opC.sign != sgnAB))) begin
	 s.res = tagged Valid qnan();
	 s.exc.invalid_op = True;
      end
      //if opC is infinity then it is final result.
      else if (isInfinity(opC)) begin
	 s.res = tagged Valid opC;
      end
      //if opA or opB is infinity then result is infinity. 
      else if (infAB) begin
	 s.res = tagged Valid infinity(sgnAB);
      end
      //if opC and product is zero then result is also zero. 
      else if (isZero(opC) && zeroAB && (opC.sign == sgnAB)) begin
	 s.res = tagged Valid opC;
      end

      //sfdAB contains multiplication of significand of opA and significand of opB.
      Bit#(48) sfdAB = primMul(sfdA, sfdB);
      Bit#(50) sfdAb = 0;			//sfdAb for addition.


   // passthrough stage for multiply register retiming
	

      FloatingPoint#(8,23) ab = defaultValue;
	//if s.res is invalid that means it didn't enter any special case. 
	if (s.res matches tagged Invalid)
	begin
		//if product exponent is greater than maximum(127 for sp and 1023 for dp) exponent then no need for normalization, product exponent and product mantissa remains as it is. 
		if (expAB > fromInteger(maxexp(ab))) 
		begin
			ab.sign = sgnAB;
			ab.exp = maxBound - 1;
			ab.sfd = maxBound;
			sfdAb = {sfdAB,2'b00};
		end
		//checking for minimum possible exponent. It indicates after shift also exponent cannot be in a valid range. 
		else if (expAB < (fromInteger(minexp_subnormal(ab))-2)) 
		begin
			ab.sign = sgnAB;
			ab.exp = 0;
			ab.sfd = 0;
			sfdAb = {sfdAB,2'b00};
			if (|sfdAB == 1) 
			begin
				bigEXP = True;
			end
		end
		else 
		//go for normalization. 
		begin
			let shift = fromInteger(minexp(ab)) - expAB;
			//shift > 0 indicates expAB is smaller than minimum exponent. For true condition set expAB as minimum possible exponent. 
			if (shift > 0) 
			begin
               //XXX XXX XXX Removed subnormal logic
          // subnormal
`ifdef denormal_support

				Bit#(1) sfdlsb = |(sfdAB << (fromInteger(valueOf(48)) - shift));

				sfdAB = sfdAB >> shift;
				sfdAB[0] = sfdAB[0] | sfdlsb;
				expAB = 'd-126;
`else
				ab.sfd = 0;
				sfdAB = 0;
`endif
				ab.exp = 0;
			end
	    		//simply add bias in product exponent. product exponent is addition of exponent of opA and exponent of opB. 
	    		else 
			begin
	       			ab.exp = cExtend(expAB + fromInteger(bias(ab)));
	    		end
	    		ab.sign = sgnAB;

			if(|sfdAB == 0)
				expAB = 'd-127;
		    	//do normalization
		    	let y <- normalize1_sp(tuple2(ab, sfdAB));
			//normalization1_sp function gives exception, normalized result. 
		    	ab = tpl_1(y);
		    	s.exc = s.exc | tpl_2(y);
			sfdAb = tpl_3(y);
			expAB = isSubNormal(ab) ? fromInteger(minexp(ab)) : signExtend(unpack(unbias(ab)));
	 	end
      end


//******************************************* Adder starts *********************************************************

      //compute expC similar to expA,expB. 
      Int#(10) expC = isSubNormal(opC) ? fromInteger(minexp(opC)) : signExtend(unpack(unbias(opC)));


      `ifdef denormal_support
         opC.sfd = opC.sfd;
      `else
         if (isSubNormal(opC))
         opC.sfd = 0;
      `endif


      //significand of opC.
      Bit#(50) sfdC = {1'b0,getHiddenBit(opC), opC.sfd, 25'b0};


 
      Bool sub = opC.sign != ab.sign; //opC should be added or subtracted. 

	Int#(10) exp = ?;
	Int#(10) shift = ?;
	Bit#(50) x  = ?;
	Bit#(50) y  = ?;
	
	Bool sgn = ?;
	FloatingPoint#(8,23) out = defaultValue;

	//Smaller opernad among opAB and opC will become y and larger will x.
	//later x+y and x-y is performed for final result. 
	if ((!acc) || (expAB > expC) || ((expAB == expC) && (sfdAb > sfdC))) 
	begin
		exp = expAB;
		shift = expAB - expC;
		x = sfdAb;
	    	y = sfdC;
	   	sgn = ab.sign;
	end
	else 
	begin
		exp = expC;
		shift = expC - expAB;
		x = sfdC;
		y = sfdAb;
		sgn = opC.sign;
      end		

	//y is shifted to make exponent of opAB and opC equal.
	if (s.res matches tagged Invalid) 
	begin
		if (shift < fromInteger(50)) 
		begin
			Bit#(50) guard = 0;
			guard = y << ((50) - shift);
			y = y >> shift;
			y[0] = y[0] | (|guard);
		end
		else if (|y == 1) 
		begin
			y = 1;
		end
	end

	let sum = x + y;
	let diff = x - y;
	//Checking for overflow while adding.
	//during addition if and extra carry is generated then it is overflow. And it will set overflow and inexact flag. 
	if((x[49] == 1 || y[49] == 1) && sum[49] == 0 && !sub) 
	begin
		s.exc.overflow = True;
		s.exc.inexact = True;
	end
		
    

	Bit#(2) guard = 0;
	if (s.res matches tagged Invalid) 
	begin
		Bit#(50) sfd;
		//final significand is either sum or diff. It is decided by value of sub. 
		sfd = sub ? diff : sum;

		out.sign = sgn;
		//bias added in exponent. 
		out.exp = cExtend(exp + fromInteger(bias(out)));


		//if opC is zero then final result is product of opA and opB. 
		if(zeroC)
		begin
			out = ab;
		end

		// normalization after add/sub phase. 
		let y <- normalize2_sp(tuple4(exp,out, sfd,s.rmode));

		//this normalzation function gives result, exception flags and guard bit. guard bits are used for rounding. 		
		out = tpl_1(y);
		guard = tpl_2(y);
		s.exc = s.exc | tpl_3(y);
		//prevents by setting underflow flag during product phase. 
		if(bigEXP)
			s.exc.underflow = False;
	end

	if (s.res matches tagged Valid .x) 
	begin
		out = x;
	end
	else 
	begin
		let y1 = out;
		//do rounding. rounding is based on rounding mode and guard bit(generated by normalization function).
		let y = round(s.rmode, out, guard);	//rounding
		out = tpl_1(y);
		s.exc = s.exc | tpl_2(y);


		//underflow for zero exponent. 
		if(out.exp == 0 && s.exc.inexact && !s.exc.overflow)
			s.exc.underflow = True;


		// adjust sign for exact zero result
		if (acc && isZero(out) && !s.exc.inexact && sub) 
		begin
			out.sign = (s.rmode == Rnd_Minus_Inf);
		end

		//in round_nearest_even and maxMax rounding mode, overflow set final result to +inf.
		if(s.exc.overflow == True && (s.rmode == Rnd_Nearest_Even ||s.rmode == Rnd_Nearest_Away_Zero))
		begin
			s.exc.underflow = False;
			out.exp = '1;
			out.sfd = '0;
		end
		
		//in round_plus_inf for overflow, set infinity in case of positive result and largest negative number in case of negative result. 
		else if(s.exc.overflow == True && s.rmode == Rnd_Plus_Inf)
		begin
			if(ab.sign)
			begin
				out.exp = 'b11111110;
				out.sfd = '1;	
			end
			else
			begin
				out.exp = '1;
				out.sfd = '0;	
			end
		end
		//in round_minus_inf for overflow, set minus infinity in case of negative result and largest positive number in case of positive result. 
		else if(s.exc.overflow == True && s.rmode == Rnd_Minus_Inf)
		begin
			if(ab.sign)
			begin
				out.exp = '1;
				out.sfd = '0;	
			end
			else
			begin
				out.exp = 'b11111110;
				out.sfd = '1;	
			end
		end
		//in round_zero rounding mode for overflow set maximum normal number. 
		else if(s.exc.overflow == True && s.rmode == Rnd_Zero)
		begin
			out.exp = 'b11111110;
			out.sfd = '1;	
		end
		//set result as zero if product of opA and opB is zero and opC is also zero. 
		else if( (isZero(opA)||isZero(opB)) && isZero(opC))
		begin
			out.exp = 0;
			out.sfd = 0;
		end

		//exponent less than -126 is very small exponent and we cannot represent such a number so set it zero. But round up rounding mode set it to leaset number in case of positive result.
		else if(isZero(opC) && expAB < -126 && !ab.sign  && s.rmode == Rnd_Plus_Inf)
		begin
			out.exp = 0;
			out.sfd = 1;
			s.exc.underflow = True;
		end
		else if(isZero(opC) && expAB < -126 && ab.sign && s.rmode == Rnd_Minus_Inf)
		begin
			out.exp = 0;
			out.sfd = 1;
			s.exc.underflow = True;
			s.exc.inexact = True;
		end

		else if(isZero(opC) && expAB < -126)
		begin
			out.exp = 0;
			out.sfd = 0;
			s.exc.underflow = True;
			s.exc.inexact = True;
		end
	end
   return tuple2(canonicalize(out),s.exc);
endactionvalue;






/*doc:func: Actual function that perform a*b-c for double precision. */
//fn_fpu_fma_dp function is similar to fn_fpu_fma_sp function only difference is, it is performed on double precision number. 
function ActionValue#(Tuple2#(FloatingPoint#(11,52),Exception)) fn_fpu_fma_dp(Tuple4#(
		 FloatingPoint#(11,52),
		 FloatingPoint#(11,52),
		 Maybe#(FloatingPoint#(11,52)),
		 RoundMode) operands)=actionvalue

      match {.opA, .opB, .mopC, .rmode } = operands;
      CommonState#(11,52) s = CommonState {
	 res: tagged Invalid,
	 exc: defaultValue,
	 rmode: rmode
	 };

      Bool acc = False;
      FloatingPoint#(11,52) opC = 0;

      if (mopC matches tagged Valid .opC_) begin
	 opC = opC_;
	 acc = True;
      end
	`ifdef verbose `logLevel( tb, 0, $format("opA %h, opB %h, opC %h",opA, opB, opC)) `endif

	Bool zeroC = (opC.exp == 0 && opC.sfd == 0);
      Int#(13) expA = isSubNormal(opA) ? fromInteger(minexp(opA)) : signExtend(unpack(unbias(opA)));
      Int#(13) expB = isSubNormal(opB) ? fromInteger(minexp(opB)) : signExtend(unpack(unbias(opB)));
/*      if (isSubNormal(opB))
	 opB.sfd = 0;
      if (isSubNormal(opC))
	 opC.sfd = 0;*/
      
      Bit#(53) sfdA = { getHiddenBit(opA), opA.sfd };
      Bit#(53) sfdB = { getHiddenBit(opB), opB.sfd };
      `ifdef verbose `logLevel( tb, 0, $format("expA %h %d",expA,expA)) `endif
      `ifdef verbose `logLevel( tb, 0, $format("expB %h %d",expB,expB)) `endif
      `ifdef verbose `logLevel( tb, 0, $format("sfdA %h",sfdA)) `endif
      `ifdef verbose `logLevel( tb, 0, $format("sfdB %h",sfdB)) `endif
      Bool sgnAB = opA.sign != opB.sign;
      Bool infAB = isInfinity(opA) || isInfinity(opB);
      Bool zeroAB = isZero(opA) || isZero(opB);
      Bool bigEXP = False;
      Int#(13) expAB = expA + expB;
      `ifdef verbose `logLevel( tb, 0, $format("expAB %h %d",expAB,expAB)) `endif




	if(((isZero(opA) && isInfinity(opB)) || (isZero(opB) && isInfinity(opA)) ) && isNaN(opC))
		s.exc.invalid_op = True;
      if (isSNaN(opC)) begin
	 s.res = tagged Valid nanQuiet(opC);
	 s.exc.invalid_op = True;
      end
      else if (isSNaN(opA)) begin
	 s.res = tagged Valid nanQuiet(opA);
	 s.exc.invalid_op = True;
      end
      else if (isSNaN(opB)) begin
	 s.res = tagged Valid nanQuiet(opB);
	 s.exc.invalid_op = True;
      end
      else if (isQNaN(opC)) begin
	 s.res = tagged Valid opC;
      end
      else if (isQNaN(opA)) begin
	 s.res = tagged Valid opA;
      end
      else if (isQNaN(opB)) begin
	 s.res = tagged Valid opB;
      end
      else if ((isInfinity(opA) && isZero(opB)) || (isZero(opA) && isInfinity(opB)) || (isInfinity(opC) && infAB && (opC.sign != sgnAB))) begin
	 // product of zero and infinity or addition of opposite sign infinity
	 s.res = tagged Valid qnan();
	 s.exc.invalid_op = True;
      end
      else if (isInfinity(opC)) begin
	 s.res = tagged Valid opC;
      end
      else if (infAB) begin
	 s.res = tagged Valid infinity(sgnAB);
      end
      else if (isZero(opC) && zeroAB && (opC.sign == sgnAB)) begin
	 s.res = tagged Valid opC;
      end


      Bit#(106) sfdAB = primMul(sfdA, sfdB);
      Bit#(108) sfdAb = 0;

      `ifdef verbose `logLevel( tb, 0, $format("sfdAB %h",sfdAB)) `endif
   // passthrough stage for multiply register retiming
 

   // normalize multiplication result
	

      FloatingPoint#(11,52) ab = defaultValue;
	if (s.res matches tagged Invalid)
//	if (False)
	begin

		if (expAB > fromInteger(maxexp(ab))) 
		begin
			ab.sign = sgnAB;
			ab.exp = maxBound - 1;
			ab.sfd = maxBound;

//			s.exc.overflow = True;
//			s.exc.inexact = True;
			sfdAb = {sfdAB,2'b00};
		end
		else if (expAB < (fromInteger(minexp_subnormal(ab))-2)) 
		begin
			ab.sign = sgnAB;
			ab.exp = 0;
			ab.sfd = 0;
			sfdAb = {sfdAB,2'b00};
			if (|sfdAB == 1) 
			begin
				bigEXP = True;
//				s.exc.underflow = True;
//				s.exc.inexact = True;
			end
		end
		else 
		begin
			let shift = fromInteger(minexp(ab)) - expAB;
			if (shift > 0) 
			begin
               //XXX XXX XXX Removed subnormal logic
          // subnormal
`ifdef denormal_support

				Bit#(1) sfdlsb = |(sfdAB << (fromInteger(valueOf(106)) - shift));

				sfdAB = sfdAB >> shift;
				sfdAB[0] = sfdAB[0] | sfdlsb;
				expAB = 'd-1022;
`else
				ab.sfd = 0;
				sfdAB = 0;
`endif
				ab.exp = 0;
			end
	    		else 
			begin
	       			ab.exp = cExtend(expAB + fromInteger(bias(ab)));
	    		end
	    		ab.sign = sgnAB;


			if(|sfdAB == 0)
				expAB = 'd-1023;
		    	let y <- normalize1_dp(tuple2(ab, sfdAB));
		    	ab = tpl_1(y);
		    	s.exc = s.exc | tpl_2(y);
			sfdAb = tpl_3(y);
			expAB = isSubNormal(ab) ? fromInteger(minexp(ab)) : signExtend(unpack(unbias(ab)));
	 	end
      end


//******************************************* Adder starts *********************************************************

      Int#(13) expC = isSubNormal(opC) ? fromInteger(minexp(opC)) : signExtend(unpack(unbias(opC)));


      `ifdef denormal_support
         opC.sfd = opC.sfd;
      `else
         if (isSubNormal(opC))
         opC.sfd = 0;
      `endif

	
      Bit#(108) sfdC = {1'b0,getHiddenBit(opC), opC.sfd, 54'b0};



      Bool sub = opC.sign != ab.sign;

	Int#(13) exp = ?;
	Int#(13) shift = ?;
	Bit#(108) x  = ?;
	Bit#(108) y  = ?;
	
	Bool sgn = ?;
	FloatingPoint#(11,52) out = defaultValue;
	if ((!acc) || (expAB > expC) || ((expAB == expC) && (sfdAb > sfdC))) 
	begin
		exp = expAB;
		shift = expAB - expC;
		x = sfdAb;
	    	y = sfdC;
	   	sgn = ab.sign;
	end
	else 
	begin
		exp = expC;
		shift = expC - expAB;
		x = sfdC;
		y = sfdAb;
		sgn = opC.sign;
      end		

	if (s.res matches tagged Invalid) 
	begin
		if (shift < fromInteger(108)) 
		begin
			Bit#(108) guard = 0;

//			if(shift > 108)
//				guard[0] = |y;
//			else
				guard = y << ((108) - shift);
			y = y >> shift;
			y[0] = y[0] | (|guard);
		end
		else if (|y == 1) 
		begin
			y = 1;
		end
	end
	`ifdef verbose `logLevel( tb, 0, $format("x %h",x)) `endif
	`ifdef verbose `logLevel( tb, 0, $format("y %h",y)) `endif
	let sum = x + y;
	let diff = x - y;
	if((x[107] == 1 || y[107] == 1) && sum[107] == 0 && !sub) 
	begin
		s.exc.overflow = True;
		s.exc.inexact = True;
	end
		
    

	Bit#(2) guard = 0;
	`ifdef verbose `logLevel( tb, 0, $format("expC %h %d",expC,expC)) `endif
	`ifdef verbose `logLevel( tb, 0, $format("expAB %h %d",expAB,expAB)) `endif
	`ifdef verbose `logLevel( tb, 0, $format("exp %h %d",exp,exp)) `endif
	if (s.res matches tagged Invalid) 
	begin
		Bit#(108) sfd;

		sfd = sub ? diff : sum;

		out.sign = sgn;
		out.exp = cExtend(exp + fromInteger(bias(out)));
		
		
		if(zeroC)
		begin
			out = ab;
		end

		let y <- normalize2_dp(tuple4(exp,out, sfd,s.rmode));	//normalize   2

		out = tpl_1(y);
		`ifdef verbose `logLevel( tb, 0, $format("out %h ", out)) `endif
		guard = tpl_2(y);
		s.exc = s.exc | tpl_3(y);
		if(bigEXP)
			s.exc.underflow = False;
	end

	if (s.res matches tagged Valid .x) 
	begin
		out = x;
	end
	else 
	begin
		let y1 = out;
		let y = round(s.rmode, out, guard);	//rounding
		out = tpl_1(y);
		`ifdef verbose `logLevel( tb, 0, $format("out %h ",out)) `endif
		s.exc = s.exc | tpl_2(y);



		if(out.exp == 0 && s.exc.inexact && !s.exc.overflow)
			s.exc.underflow = True;


		// adjust sign for exact zero result
		if (acc && isZero(out) && !s.exc.inexact && sub) 
		begin
			out.sign = (s.rmode == Rnd_Minus_Inf);
		end


		if(s.exc.overflow == True && (s.rmode == Rnd_Nearest_Even ||s.rmode == Rnd_Nearest_Away_Zero))
		begin
			s.exc.underflow = False;
			out.exp = '1;
			out.sfd = '0;
		end
		else if(s.exc.overflow == True && s.rmode == Rnd_Plus_Inf)
		begin
			if(ab.sign)
			begin
				out.exp = 'b11111111110;
				out.sfd = '1;	
			end
			else
			begin
				out.exp = '1;
				out.sfd = '0;	
			end
		end
		else if(s.exc.overflow == True && s.rmode == Rnd_Minus_Inf)
		begin
			if(ab.sign)
			begin
				out.exp = '1;
				out.sfd = '0;	
			end
			else
			begin
				out.exp = 'b11111111110;
				out.sfd = '1;	
			end
		end
		else if(s.exc.overflow == True && s.rmode == Rnd_Zero)
		begin
			out.exp = 'b11111111110;
			out.sfd = '1;	
		end
		else if( (isZero(opA)||isZero(opB)) && isZero(opC))
		begin
			out.exp = 0;
			out.sfd = 0;
		end
		else if(isZero(opC) && expAB < -1022 && !ab.sign  && s.rmode == Rnd_Plus_Inf)
		begin
			out.exp = 0;
			out.sfd = 1;
			s.exc.underflow = True;
		end
		else if(isZero(opC) && expAB < -1022 && ab.sign && s.rmode == Rnd_Minus_Inf)
		begin
			out.exp = 0;
			out.sfd = 1;
			s.exc.underflow = True;
			s.exc.inexact = True;
		end
		else if(isZero(opC) && expAB < -1022)
		begin
			out.exp = 0;
			out.sfd = 0;
			s.exc.underflow = True;
			s.exc.inexact = True;
		end
	end
	`ifdef verbose `logLevel( tb, 0, $format("out %h ",out)) `endif
   return tuple2(canonicalize(out),s.exc);
   
endactionvalue;


   interface Ifc_fpu_fma#(numeric type e, numeric type m, numeric type nos);
      method Action send(Tuple4#(
         FloatingPoint#(e,m),
         FloatingPoint#(e,m),
         Maybe#(FloatingPoint#(e,m)),
         RoundMode) operands);
//      method Tuple2#(Bit#(1),Tuple2#(FloatingPoint#(e,m),Exception)) receive();
      method ReturnType#(e,m) receive();
   endinterface
   module mk_fpu_fma(Ifc_fpu_fma#(e,m,nos))
      provisos(
         Add#(a__, TLog#(TAdd#(1, TAdd#(TAdd#(m, 1), TAdd#(m, 1)))), TAdd#(e, 1)),
         Add#(b__, TLog#(TAdd#(1, TAdd#(m, 5))), TAdd#(e, 1))
      );

      //Integer number_of_stages = 4;  
      Vector#(nos,Reg#(Tuple2#(FloatingPoint#(e,m),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
      Vector#(nos,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      rule rl_pipeline;
         for(Integer i = 1 ; i <= valueOf(nos) -1 ; i = i+1)
         begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule
      method Action send(Tuple4#(
            FloatingPoint#(e,m),
            FloatingPoint#(e,m),
            Maybe#(FloatingPoint#(e,m)),
            RoundMode) operands);		
               rg_stage_out[0] <= fn_fpu_fma(operands);
               rg_stage_valid[0] <= 1;

      endmethod
      method ReturnType#(e,m) receive();
return ReturnType{valid:rg_stage_valid[valueOf(nos)-1],value:tpl_1(rg_stage_out[valueOf(nos)-1]) ,ex:tpl_2(rg_stage_out[valueOf(nos)-1])};

      endmethod 
   endmodule

   // (*synthesize*)
   module mk_fpu_fma_sp_instance(Ifc_fpu_fma#(8,23,`STAGES_FMA_SP));
      let ifc();
      mk_fpu_fma _temp(ifc);
      return (ifc);
   endmodule
   // (*synthesize*)
   module mk_fpu_fma_dp_instance(Ifc_fpu_fma#(11,52,`STAGES_FMA_DP));
      let ifc();
      mk_fpu_fma _temp(ifc);
      return (ifc);
   endmodule





//Old function. Now not in use.
   function Tuple2#(FloatingPoint#(e,m),Exception) fn_fpu_fma(Tuple4#(
		 FloatingPoint#(e,m),
		 FloatingPoint#(e,m),
		 Maybe#(FloatingPoint#(e,m)),
		 RoundMode) operands)
		 provisos(
      Add#(e,2,ebits),
      Add#(m,1,mbits),
      Add#(m,5,m5bits),
      Add#(mbits,mbits,mmbits),
      // per request of bsc
      Add#(1, a__, mmbits),
      Add#(m, b__, mmbits),
      Add#(c__, TLog#(TAdd#(1, mmbits)), TAdd#(e, 1)),
      Add#(d__, TLog#(TAdd#(1, m5bits)), TAdd#(e, 1)),
      Add#(1, TAdd#(1, TAdd#(m, 3)), m5bits)
      );

      match { .opB, .opC, .mopA, .rmode } = operands;
      CommonState#(e,m) s = CommonState {
	 res: tagged Invalid,
	 exc: defaultValue,
	 rmode: rmode
	 };

      Bool acc = False;
      FloatingPoint#(e,m) opA = 0;

      if (mopA matches tagged Valid .opA_) begin
	 opA = opA_;
	 acc = True;
      end

      Int#(ebits) expB = isSubNormal(opB) ? fromInteger(minexp(opB)) : signExtend(unpack(unbias(opB)));
      Int#(ebits) expC = isSubNormal(opC) ? fromInteger(minexp(opC)) : signExtend(unpack(unbias(opC)));
      
/*      if (isSubNormal(opB))
	 opB.sfd = 0;
      if (isSubNormal(opC))
	 opC.sfd = 0;*/
      
      Bit#(mbits) sfdB = { getHiddenBit(opB), opB.sfd };
      Bit#(mbits) sfdC = { getHiddenBit(opC), opC.sfd };

      Bool sgnBC = opB.sign != opC.sign;
      Bool infBC = isInfinity(opB) || isInfinity(opC);
      Bool zeroBC = isZero(opB) || isZero(opC);
      Int#(ebits) expBC = expB + expC;

      if (isSNaN(opA)) begin
	 s.res = tagged Valid nanQuiet(opA);
	 s.exc.invalid_op = True;
      end
      else if (isSNaN(opB)) begin
	 s.res = tagged Valid nanQuiet(opB);
	 s.exc.invalid_op = True;
      end
      else if (isSNaN(opC)) begin
	 s.res = tagged Valid nanQuiet(opC);
	 s.exc.invalid_op = True;
      end
      else if (isQNaN(opA)) begin
	 s.res = tagged Valid opA;
      end
      else if (isQNaN(opB)) begin
	 s.res = tagged Valid opB;
      end
      else if (isQNaN(opC)) begin
	 s.res = tagged Valid opC;
      end
      else if ((isInfinity(opB) && isZero(opC)) || (isZero(opB) && isInfinity(opC)) || (isInfinity(opA) && infBC && (opA.sign != sgnBC))) begin
	 // product of zero and infinity or addition of opposite sign infinity
	 s.res = tagged Valid qnan();
	 s.exc.invalid_op = True;
      end
      else if (isInfinity(opA)) begin
	 s.res = tagged Valid opA;
      end
      else if (infBC) begin
	 s.res = tagged Valid infinity(sgnBC);
      end
      else if (isZero(opA) && zeroBC && (opA.sign == sgnBC)) begin
	 s.res = tagged Valid opA;
      end


      let sfdBC = primMul(sfdB, sfdC);


   // passthrough stage for multiply register retiming
 

   // normalize multiplication result
  

      FloatingPoint#(e,m) bc = defaultValue;
      Bit#(2) guardBC = ?;

      if (s.res matches tagged Invalid) begin
	 if (expBC > fromInteger(maxexp(bc))) begin
	    bc.sign = sgnBC;
	    bc.exp = maxBound - 1;
	    bc.sfd = maxBound;
	    guardBC = '1;

	    s.exc.overflow = True;
	    s.exc.inexact = True;
	 end
	 else if (expBC < (fromInteger(minexp_subnormal(bc))-2)) begin
	    bc.sign = sgnBC;
	    bc.exp = 0;
	    bc.sfd = 0;
	    guardBC = 0;

	    if (|sfdBC == 1) begin
	       guardBC = 1;
	       s.exc.underflow = True;
	       s.exc.inexact = True;
	    end
	 end
	 else begin
	    let shift = fromInteger(minexp(bc)) - expBC;
	    if (shift > 0) begin
               //XXX XXX XXX Removed subnormal logic
          // subnormal
`ifdef denormal_support
	       Bit#(1) sfdlsb = |(sfdBC << (fromInteger(valueOf(mmbits)) - shift));

	       sfdBC = sfdBC >> shift;
          sfdBC[0] = sfdBC[0] | sfdlsb;
`else
               bc.sfd = 0;
               sfdBC = 0;
          //guardBC = 0;
`endif
          bc.exp = 0;
	    end
	    else begin
	       bc.exp = cExtend(expBC + fromInteger(bias(bc)));
	    end

	    bc.sign = sgnBC;
	    let y = normalize(bc, sfdBC);
	    bc = tpl_1(y);
	    guardBC = tpl_2(y);
	    s.exc = s.exc | tpl_3(y);
	 end
      end


     
      Int#(ebits) expA = isSubNormal(opA) ? fromInteger(minexp(opA)) : signExtend(unpack(unbias(opA)));
      expBC = isSubNormal(bc) ? fromInteger(minexp(bc)) : signExtend(unpack(unbias(bc)));
      `ifdef denormal_support
         opA.sfd = opA.sfd;
      `else
         if (isSubNormal(opA))
         opA.sfd = 0;
      `endif
        
      Bit#(m5bits) sfdA = {1'b0, getHiddenBit(opA), opA.sfd, 3'b0};
      sfdBC = {1'b0, getHiddenBit(bc), bc.sfd, guardBC, 1'b0};

      Bool sub = opA.sign != bc.sign;

      Int#(ebits) exp = ?;
      Int#(ebits) shift = ?;
      Bit#(m5bits) x = ?;
      Bit#(m5bits) y = ?;
      Bool sgn = ?;

      if ((!acc) || (expBC > expA) || ((expBC == expA) && (sfdBC > sfdA))) begin
	 exp = expBC;
	 shift = expBC - expA;
	 x = sfdBC;
    y = sfdA;
   sgn = bc.sign;
      end
      else begin
	 exp = expA;
	 shift = expA - expBC;
	 x = sfdA;
	 y = sfdBC;
	 sgn = opA.sign;
      end

   
      if (s.res matches tagged Invalid) begin
	 if (shift < fromInteger(valueOf(m5bits))) begin
	    Bit#(m5bits) guard;

	    guard = y << (fromInteger(valueOf(m5bits)) - shift);
	    y = y >> shift;
	    y[0] = y[0] | (|guard);
	 end
	 else if (|y == 1) begin
	    y = 1;
	 end
      end

     
      let sum = x + y;
      let diff = x - y;

    

      FloatingPoint#(e,m) out = defaultValue;
      Bit#(2) guard = 0;

      if (s.res matches tagged Invalid) begin
	 Bit#(m5bits) sfd;

	 sfd = sub ? diff : sum;

	 out.sign = sgn;
	 out.exp = cExtend(exp + fromInteger(bias(out)));

	 let y = normalize(out, sfd);
	 out = tpl_1(y);
	 guard = tpl_2(y);
	 s.exc = s.exc | tpl_3(y);
      end


      if (s.res matches tagged Valid .x) begin
	 out = x;
      end
      else begin
	 let y = round(s.rmode, out, guard);
	 out = tpl_1(y);
	 s.exc = s.exc | tpl_2(y);

	 // adjust sign for exact zero result
	 if (acc && isZero(out) && !s.exc.inexact && sub) begin
	    out.sign = (s.rmode == Rnd_Minus_Inf);
	 end
      end

   if(s.exc.overflow == True && s.rmode == Rnd_Nearest_Even) begin
	out.exp = '1;
	out.sfd = '0;
   end
/*   else if(s.exc.underflow == True) begin
	Bool sign = True;
	Bit#(e) expo = 'hff;
	Bit#(m) man = 'b00000000000000000000000;
	let result = FloatingPoint{sign : sign,exp: expo,sfd: man};
	return tuple2(result,s.exc);   	
   end*/
   return tuple2(canonicalize(out),s.exc);
   endfunction






endpackage

