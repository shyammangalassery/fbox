////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Mouna Krishna
Email id: mounakrishna2398@gmail.com
--------------------------------------------------------------------------------------------------

Div_sp latency - 22 cycles.
Div_dp latency - 37 cycles.
*/
////////////////////////////////////////////////////////////////////////////////
//  Filename      : fpu_srt_radix4_div.bsv
////////////////////////////////////////////////////////////////////////////////
package fpu_srt_radix4_div;

import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import DReg              ::*;
//import div_qds ::*;
import normalize_div_hierarchical :: *;
  `include "Logger.bsv"

//`ifdef fpu_hierarchical

interface Ifc_div_qds;
   method Tuple4#(Int#(7), Int#(7), Int#(7), Int#(7)) qds_lut(Bit#(3) divisor_msb3);
endinterface

function Reg#(t) readOnlyReg(t r);
   return (interface Reg;
      method t _read = r;
      method Action _write(t x) = noAction;
  endinterface);
endfunction
/*doc:module:Returns the seletion constants for digit selection*/
module mk_div_qds(Ifc_div_qds);
   Reg#(Int#(7)) rg_m_1[8];
   rg_m_1[0] = readOnlyReg(-13);
   rg_m_1[1] = readOnlyReg(-15);
   rg_m_1[2] = readOnlyReg(-16);
   rg_m_1[3] = readOnlyReg(-18);
   rg_m_1[4] = readOnlyReg(-20);
   rg_m_1[5] = readOnlyReg(-20);
   rg_m_1[6] = readOnlyReg(-22);
   rg_m_1[7] = readOnlyReg(-24);

   Reg#(Int#(7)) rg_m0[8];
   rg_m0[0] = readOnlyReg(-4);
   rg_m0[1] = readOnlyReg(-6);
   rg_m0[2] = readOnlyReg(-6);
   rg_m0[3] = readOnlyReg(-6);
   rg_m0[4] = readOnlyReg(-8);
   rg_m0[5] = readOnlyReg(-8);
   rg_m0[6] = readOnlyReg(-8);
   rg_m0[7] = readOnlyReg(-8);

   Reg#(Int#(7)) rg_m1[8];
   rg_m1[0] = readOnlyReg(4);
   rg_m1[1] = readOnlyReg(4);
   rg_m1[2] = readOnlyReg(4);
   rg_m1[3] = readOnlyReg(4);
   rg_m1[4] = readOnlyReg(6);
   rg_m1[5] = readOnlyReg(6);
   rg_m1[6] = readOnlyReg(8);
   rg_m1[7] = readOnlyReg(8);

   Reg#(Int#(7)) rg_m2[8];
   rg_m2[0] = readOnlyReg(12);
   rg_m2[1] = readOnlyReg(14);
   rg_m2[2] = readOnlyReg(15);
   rg_m2[3] = readOnlyReg(16);
   rg_m2[4] = readOnlyReg(18);
   rg_m2[5] = readOnlyReg(20);
   rg_m2[6] = readOnlyReg(20);
   rg_m2[7] = readOnlyReg(24);

   method Tuple4#(Int#(7), Int#(7), Int#(7), Int#(7)) qds_lut(Bit#(3) divisor_msb3);
      return tuple4(rg_m_1[divisor_msb3], rg_m0[divisor_msb3], rg_m1[divisor_msb3], rg_m2[divisor_msb3]);
   endmethod
endmodule

////////////////////////////////////////////////////////////////////////////////
/// Divide
////////////////////////////////////////////////////////////////////////////////

interface Ifc_divider_sp;
   method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) operands);
   method ReturnType#(8,23) receive(); 
endinterface

interface Ifc_divider_dp;
   method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) operands);
   method ReturnType#(11,52) receive(); 
endinterface

(*synthesize*)
module mk_divider_sp(Ifc_divider_sp);
   /*doc:reg: Stores the valid bit, final quotient and exceptions*/
   Reg#(Tuple3#(Bit#(1),FloatingPoint#(8,23),Exception)) rg_result <- mkDReg(tuple3(unpack(0),unpack(0),unpack(0)));
   /*doc:reg: The reg which stores the operands taken from send method*/
   Reg#(Tuple3#(FloatingPoint#(8,23), FloatingPoint#(8,23), RoundMode)) rg_operands <- mkReg(tuple3(unpack(0),unpack(0),unpack(0)));
   /*doc:reg: To control which rule to fire*/
   Reg#(Bit#(2)) rg_en1 <- mkReg(0);
   Reg#(Bit#(2)) rg_en2 <- mkReg(0);
   /*doc:reg: Stores the divisor - HiddenBit + Mantissa(23) + 2 bits for convergence + 1 bit to match rg_p_a size. The carry bit is added locally rl_loop*/
   Reg#(Bit#(27)) rg_divisor <- mkReg(0);
   Reg#(Bit#(27)) rg_not_divisor <- mkReg(0);
   /*doc:reg: Stores partial remainder and operand A, 1 bit to keep size even + carry_bit + 2bits_for_convergence + dividend(A)*/
   Reg#(Bit#(28)) rg_p_a <- mkReg(0);
   Reg#(Bit#(28)) rg_p_a_cry <- mkReg(0);
   /*doc:reg: Register for holding positive quotient digits. It's 29 bits to get more precision used for rounding. Updated based on the On The Fly method.*/
   Reg#(Bit#(29)) rg_a <-mkReg(0);
   /*doc:reg: Register for holding negative quotient digits. It's 29 bits to get more precision used for rounding. Updated based on the On The Fly method.*/
   Reg#(Bit#(29)) rg_b <-mkReg(0);
   /*doc:reg: holds the final quotient */
   Reg#(Bit#(29)) rg_final_rsfd <- mkReg(0);
   /*doc:reg: Register to track the iterations*/
   Reg#(Bit#(4)) rg_count <-mkReg(0);
   /*doc:reg: Stores the exception*/
   Reg#(Exception) rg_final_exc <- mkReg(defaultValue);
   /*doc:reg: To just store the final value without flags and valid bit*/
   Reg#(FloatingPoint#(8,23)) rg_final_result <- mkReg(defaultValue);
   /*doc:reg: To store the mantissa seperately*/
   Reg#(Bit#(24)) rg_sfdA <- mkReg(0);
   Reg#(Bit#(24)) rg_sfdB <- mkReg(0);
   /*doc:reg: To store exponent*/
   Reg#(Bit#(10)) rg_newexp <- mkReg(0);
   /*doc:reg: To store the shift required for subnormal mantissas*/
   Reg#(Bit#(5)) rg_final_shift <- mkReg(0);
   Reg#(Bit#(5)) rg_final_zeros <- mkReg(0);
   /*doc:reg: To differentiate results from division of no_exception inputs and exception inputs. Stores the exception result when tagged valid and vice versa.*/
   Reg#(Maybe#(FloatingPoint#(8,23))) rg_final_out <- mkReg(tagged Invalid);
   /*doc:reg: Stores the constants required for quotient selection.*/
   Reg#(Int#(7)) rg_m_1 <- mkReg(0);
   Reg#(Int#(7)) rg_m0 <- mkReg(0);
   Reg#(Int#(7)) rg_m1 <- mkReg(0);
   Reg#(Int#(7)) rg_m2 <- mkReg(0);
   /*doc:reg:Stores the guard bits calculated at normalisation stage.*/
   Reg#(Bit#(2)) rg_guard <- mkReg(0);

   /*doc:reg: stores whether the remainder is zero or not.*/
   Reg#(Bool) bl_zero <- mkReg(False);
   /*doc: A LUT which has the constants used for quotent selection.*/
   let constants_lut <- mk_div_qds();

   /*doc:function:Returns the next quotient digit. Calculated by subtracting the remainder from the selection constants and checking the sign of the result.*/
   function Tuple2#(Bit#(2), Bit#(1)) fn_digit_select(Int#(7) m_1, Int#(7) m0, Int#(7) m1, Int#(7) m2, Bit#(7) p_a, Bit#(7) p_a_cry);

      Bit#(8) final_m_1, final_m0, final_m1, final_m2;
      final_m_1 = pack(signExtend(unpack(p_a + p_a_cry)) - signExtend(m_1));
      final_m0 = pack(signExtend(unpack(p_a + p_a_cry)) - signExtend(m0));
      final_m1 = pack(signExtend(unpack(p_a + p_a_cry)) - signExtend(m1));
      final_m2 = pack(signExtend(unpack(p_a + p_a_cry)) - signExtend(m2));

      Bit#(2) s;
      Bit#(1) s_sign;
      if (final_m2[7] == 0) begin s = 2; s_sign = 0; end
      else if (final_m1[7] == 0) begin s = 1; s_sign = 0; end
      else if (final_m0[7] == 0) begin s = 0; s_sign = 0; end
      else if (final_m_1[7] == 0) begin s = 1; s_sign = 1; end
      else begin s = 2; s_sign = 1; end

      return tuple2(s, s_sign);
   endfunction
   //******************************************************* Rules **************************************************************************
   /*doc:rule: The number of MSB zeros are calculated and the manitssas are shifted accordingly.*/
   rule rl_initial0(rg_en1 == 1);
      rg_en1 <= 2;
      match {.in1, .in2, .rmode} = rg_operands;
      Bit#(24) sfdA = {getHiddenBit(in1), in1.sfd};
      Bit#(24) sfdB = {getHiddenBit(in2), in2.sfd};
      //compute MSB 0s of sfdA. 
`ifdef count_zeros_specialize
      Bit#(24) inp = sfdA;
   `ifdef even_split
      Vector#(12, Bit#(1)) v1 = unpack(inp[23:12]);
   `else
      Vector#(15, Bit#(1)) v1 = unpack(inp[23:9]);
   `endif
      Bit#(5) result1=0;                                  
      Bool done1 = False;
   `ifdef even_split
      for( Integer p1 = 11; p1 >=0; p1 = p1 - 1) begin
   `else 
      for( Integer p1 = 14; p1 >=0; p1 = p1 - 1) begin
   `endif
         if ((v1[p1] == 0)&&(!done1))  
            result1 = result1+1 ;
         else
            done1 = True;
      end
      Bit#(5) z0 = (result1);
   `ifdef even_split
      Vector#(12, Bit#(1)) v2 = unpack(inp[11:0]);
   `else
      Vector#(9, Bit#(1)) v2 = unpack(inp[8:0]);
   `endif
      Bit#(5) result2=0;                                   
      Bool done2 = False;
   `ifdef even_split
      for( Integer p2 = 11; p2 >=0; p2 = p2 - 1) begin
   `else
      for( Integer p2 = 8; p2 >=0; p2 = p2 - 1) begin
   `endif
         if ((v2[p2] == 0)&&(!done2))  
            result2 = result2+1 ;
         else
            done2 = True;
      end
      Bit#(5) z1 = result2;
   `ifdef even_split
      Bit#(5) zerosA = (|inp[23:12]==1)?z0:((|inp[11:0]==1)?(z1+12):24);	
   `else
      Bit#(5) zerosA = (|inp[23:9]==1)?z0:((|inp[8:0]==1)?(z1+15):24);	
   `endif

      //compute MSB 0s of sfdB		
      Bit#(24) inp1 = sfdB;
   `ifdef even_split
      Vector#(12, Bit#(1)) v3 = unpack(inp1[23:12]); 
   `else
      Vector#(15, Bit#(1)) v3 = unpack(inp1[23:9]);
   `endif
      Bit#(5) result3=0;                                  
      Bool done3 = False;
   `ifdef even_split
      for( Integer p3 = 11; p3 >=0; p3 = p3 - 1) begin
   `else
      for( Integer p3 = 14; p3 >=0; p3 = p3 - 1) begin
   `endif
         if ((v3[p3] == 0)&&(!done3))  
            result3 = result3+1 ;
         else
            done3 = True;
      end
      Bit#(5) z2 = (result3);
   `ifdef even_split
      Vector#(12, Bit#(1)) v4 = unpack(inp1[11:0]); 
   `else
      Vector#(9, Bit#(1)) v4 = unpack(inp1[8:0]);
   `endif
      Bit#(5) result4=0;                                   
      Bool done4 = False;
   `ifdef even_split
      for( Integer p4 = 11; p4 >=0; p4 = p4 - 1) begin
   `else
      for( Integer p4 = 8; p4 >=0; p4 = p4 - 1) begin
   `endif
         if ((v4[p4] == 0)&&(!done4))  
            result4 = result4+1 ;
         else
            done4 = True;
      end
      Bit#(5) z3 = result4;
   `ifdef even_split
      Bit#(5) zerosB = (|inp1[23:12]==1)?z2:((|inp1[11:0]==1)?(z3+12):24);
   `else
      Bit#(5) zerosB = (|inp1[23:9]==1)?z2:((|inp1[8:0]==1)?(z3+15):24);
   `endif
`else //calculating zeros using the build in function.
      Bit#(5) zerosA = pack(countZerosMSB(sfdA));
      Bit#(5) zerosB = pack(countZerosMSB(sfdB));
`endif
      sfdA = sfdA << zerosA;
      sfdB = sfdB << zerosB;
      Bit#(10) exp1 = isSubNormal(in1) ? fromInteger(minexp(in1)) : signExtend(unpack(unbias(in1)));
      Bit#(10) exp2 = isSubNormal(in2) ? fromInteger(minexp(in2)) : signExtend(unpack(unbias(in2)));
      Bit#(10) newexp = (exp1 - zeroExtend(unpack(pack(zerosA)))) - (exp2 - zeroExtend(unpack(pack(zerosB))));
      rg_newexp <= newexp;		
      rg_sfdA <= sfdA;
      rg_sfdB <= sfdB;
    endrule:rl_initial0

    /*doc:rule: Checks whehter it is an exception and assign registers accordingly.  */
    rule rl_initial(rg_en1 == 2);
       rg_en1 <= 3;
       match {.in1, .in2, .rmode} = rg_operands;
       Maybe#(FloatingPoint#(8,23)) out = tagged Invalid;
       Exception exc = defaultValue;
       FloatingPoint#(8,23) result = defaultValue;
       Bit#(5) shift = 0;
       let sfdA = rg_sfdA;
       let sfdB = rg_sfdB;
       let newexp = rg_newexp;
       Bit#(10) lv_maxexp = fromInteger(maxexp(in1)+1);
       Bool bl_maxexp = (newexp[9] == 1) ? False : (newexp[8:0] > lv_maxexp[8:0]);
       Bit#(10) lv_minexp_subnormal = fromInteger(minexp_subnormal(in1))-2;
       Bool bl_minexp_subnormal = (newexp[9] == 0) ? False : (newexp[8:0] < lv_minexp_subnormal[8:0]);
       Bit#(10) lv_minexp = fromInteger(minexp(result));
       Bool bl_minexp = (newexp[9] == 0) ? False : (newexp[8:0] < lv_minexp[8:0]);
		 
       // calculate the sign
       result.sign = in1.sign != in2.sign;
       
       //checking for exceptions
       if (isSNaN(in1)) begin
          out = tagged Valid nanQuiet(in1);
          exc.invalid_op = True;
       end
       else if (isSNaN(in2)) begin
          out = tagged Valid nanQuiet(in2);
          exc.invalid_op = True;
       end
       else if (isQNaN(in1)) begin
          out = tagged Valid in1;
       end
       else if (isQNaN(in2)) begin
          out = tagged Valid in2;
       end
       else if ((isInfinity(in1) && isInfinity(in2)) || (isZero(in1) && isZero(in2))) begin
          out = tagged Valid qnan();
          exc.invalid_op = True;
       end
       else if (isZero(in2) && !isInfinity(in1)) begin
          out = tagged Valid infinity(result.sign);
          exc.divide_0 = True;
       end
       else if (isInfinity(in1)) begin
          out = tagged Valid infinity(result.sign);
       end
       else if (isZero(in1) || isInfinity(in2)) begin
          out = tagged Valid zero(result.sign);
       end
       else if (bl_maxexp) begin
          result.exp = maxBound - 1;
          result.sfd = maxBound;
          exc.overflow = True;
          exc.inexact = True;
          let y = round(rmode, result, '1);
          out = tagged Valid tpl_1(y);
          exc = exc | tpl_2(y);
       end
       else if (bl_minexp_subnormal) begin
          result.exp = 0;
          result.sfd = 0;
          exc.underflow = True;
          exc.inexact = True;
          let y = round(rmode, result, 'b01);
          out = tagged Valid tpl_1(y);
          exc = exc | tpl_2(y);
       end
       else if (bl_minexp) begin
          result.exp = 0;
          shift = cExtend(fromInteger(minexp(result)) - newexp);
       end
       else begin
          result.exp = cExtend(newexp + fromInteger(bias(result)));
       end
       
       //If no exceptions then rg_divisor and rg_p_a are assigned accordingly.
       if (out matches tagged Invalid) begin
          Bit#(27) temp_d = {sfdB, 3'b000};
          rg_divisor <= temp_d;
          rg_not_divisor <= -temp_d;
          Bit#(TAdd#(1,27)) temp_p_a = {4'b0000, sfdA};
          rg_p_a <= temp_p_a;
          rg_p_a_cry <= 0;
          rg_a <= 0;
          rg_b <= 0;
       end
       
       // selecting the constants required for quotient selection.
       match {.m_1, .m0, .m1, .m2} = constants_lut.qds_lut(sfdB[22:20]);
       rg_m_1 <= m_1;
       rg_m0 <= m0;
       rg_m1 <= m1;
       rg_m2 <= m2;
  
       rg_final_out <= out;
       rg_final_exc <= exc;
       rg_final_result <= result;
       rg_final_shift <= shift;
    endrule:rl_initial

    /*doc:rule: Calculates the division of the mantissaas based on SRT radix4 algorithm.*/
    rule rl_loop(rg_en1 == 3 && rg_count <= fromInteger(13));
       rg_count <= rg_count + 1;
       let p_a = rg_p_a;
       let p_a_cry = rg_p_a_cry;
       let divisor = rg_divisor;
       let not_divisor = rg_not_divisor;
       Bit#(3) divisor_msb3 = divisor[25:23];
       Bit#(2) q = 0; 
       Bit#(1) q_sign = 0;

       // function that returns the next quotient digit based on selection constants and partial remainder
       let lv_qds = fn_digit_select(rg_m_1, rg_m0, rg_m1, rg_m2, p_a[27:21], p_a_cry[27:21]);

       q = tpl_1(lv_qds);
       q_sign = tpl_2(lv_qds);

       Bit#(28) y = ?;
       p_a = p_a<<2;
       p_a_cry = p_a_cry<<2;
       let qp = rg_a;
       let qm = rg_b;
       Bit#(29) qp_new, qm_new;
       
       //rg_b and rg_a keep negative and positive quotients respectively. These registers are updated based on the On The Fly method.
       // Also calculate the y to update the partial remainder.
       if(q == 2) begin
          if(q_sign == 1) begin
             y = {1'b0, divisor} << 1;
             qp_new = {qm[28:2], 2'b10};
             qm_new = {qm[28:2], 2'b01};
          end
          else begin
             y = {1'b1, not_divisor} << 1;
             qp_new = {qp[28:2], 2'b10};
             qm_new = {qp[28:2], 2'b01};
          end
       end
       else if(q==1) begin
          if(q_sign == 1) begin
             y = {1'b0, divisor};
             qp_new = {qm[28:2], 2'b11};
             qm_new = {qm[28:2], 2'b10};
          end
          else begin
             y = {1'b1, not_divisor};
             qp_new = {qp[28:2], 2'b01};
             qm_new = qp;
          end
       end
       else begin
          qp_new = qp;
          qm_new = {qm[28:2], 2'b11};
          y = 0;
       end

       Bit#(28) p_a_new = ?;
       Bit#(28) p_a_cry_new = ?;

       p_a_cry_new[0] = 0;
       for (Integer i=0; i<27; i=i+1) begin
          Bit#(2) temp = {1'b0, p_a[i]} + {1'b0, p_a_cry[i]} + {1'b0, y[i]};
          p_a_new[i] = temp[0];
          p_a_cry_new[i+1] = temp[1];
       end
       p_a_new[27] = p_a[27] + p_a_cry[27] + y[27];

       //$display("rg_count:%h: q_pos is %h, q_neg is %h, p_a is %h, q is %h", rg_count, lv_q_pos, lv_q_neg, p_a[27:21]+p_a_cry[27:21], lv_q_pos-lv_q_neg);
       rg_p_a <= p_a_new;
       rg_p_a_cry <= p_a_cry_new;
       rg_a <= qp_new<<2;
       rg_b <= qm_new<<2;
    endrule: rl_loop

    /*doc:rule: Final quotient calculation*/
    rule rl_final1(rg_en2 == 0 && rg_count > fromInteger(valueOf(13)));
       rg_en2 <= 1;
       let q1 = rg_a;
       Bool zero = (rg_p_a + rg_p_a_cry == 0);
       Bool sign = unpack((rg_p_a + rg_p_a_cry)[27]);
       let shift = rg_final_shift;
       let out = rg_final_out;	
       Bit#(5) final_zeros = ?;
       if (out matches tagged Invalid) begin 
          q1 = q1 - zeroExtend(pack(sign));
          final_zeros = (q1[28:27]==2'b00)?2: ((q1[28:27]==2'b01 || q1[28:27]==2'b10)?1:0);
          final_zeros = final_zeros + shift;
       end

       rg_final_rsfd <= q1;
       rg_final_zeros <= final_zeros;
       bl_zero <= zero;
    endrule:rl_final1
  
    rule rl_final1_1(rg_en2 == 1);
       rg_en2 <= 2;
       let rmode = tpl_3(rg_operands);
       let shift = rg_final_shift;
       let out = rg_final_out;	
       let result = rg_final_result;
       let exc = rg_final_exc;
       let zero = bl_zero;
       let q1 = rg_final_rsfd;
       let final_zeros = rg_final_zeros;
       Bit#(29) rsfd = ?;
       if (shift < fromInteger(valueOf(29))) begin
          Bit#(29) qdbits1 = q1;
          Bit#(1) sfdlsb = |(qdbits1 << (fromInteger(valueOf(29)) - shift));
          rsfd = cExtend(q1 >> shift);
          rsfd[0] = rsfd[0] | sfdlsb;
       end
       else begin
          Bit#(1) sfdlsb = |(pack(q1));
          rsfd = 0;
          rsfd[0] = sfdlsb;
       end
       if (!zero) begin
          rsfd[0] = 1;
       end
         
       if (result.exp == maxBound) begin
          if (truncateLSB(rsfd) == 2'b00) begin
             rsfd = rsfd << 1;
             result.exp = result.exp - 1;
             final_zeros = final_zeros - 1;
          end
          else begin
             result.exp = maxBound - 1;
             result.sfd = maxBound;
             exc.overflow = True;
             exc.inexact = True;
             let y = round(rmode, result, '1);
             out = tagged Valid tpl_1(y);
             exc = exc | tpl_2(y);
          end
       end
       rg_final_exc <= exc;
       rg_final_result <= result;
       rg_final_rsfd <= rsfd;
       rg_final_out <= out;
       rg_final_zeros <= final_zeros;
    endrule 

    /*doc:rule: Final exception checking, normalisation*/
    rule rl_final2(rg_en2 == 2);
       rg_en2 <= 3;
       let result = rg_final_result;
       let exc = rg_final_exc;
       let out = rg_final_out;
       let rsfd = rg_final_rsfd;
       let final_zeros = rg_final_zeros;
       Bit#(2) guard = ?;

       if (out matches tagged Invalid) begin
          match {.out_, .guard_, .exc_} = fn_normalize_sp(result, rsfd, final_zeros);
          result = out_;
          guard = guard_;
          exc = exc | exc_;
       end
     
       rg_final_out <= out;
       rg_final_exc <= exc;
       rg_final_result <= result;
       rg_guard <= guard;
    endrule:rl_final2

    /*doc:rule: Rounding*/
    rule rl_final3(rg_en2 == 3);
       rg_en2 <= 0;
       rg_count <= 0;
       rg_en1 <= 0;
       let result = rg_final_result;
       let exc = rg_final_exc;
       let rmode = tpl_3(rg_operands);
       let out = rg_final_out;
       let guard = rg_guard;
       if (out matches tagged Valid .x) begin
          result = x;
       end
       else begin
          match {.out_, .exc_} = round(rmode,result,guard);
          result = out_;
          exc = exc | exc_;
       end
          rg_result <= tuple3(1,canonicalize(result),exc);
    endrule:rl_final3
    
    //********************************************* Methods *****************************************************************************************
    /*doc:method: To get the inputs and rounding mode*/
    method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) operands);
       rg_operands <= operands;
       rg_en1 <= 1;
       rg_en2 <= 0;
    endmethod
    /*doc:method: To return the result*/
    method ReturnType#(8,23) receive(); 
       let y = ReturnType{valid:tpl_1(rg_result),value:tpl_2(rg_result),ex:tpl_3(rg_result)};
       return y;
    endmethod
endmodule


(*synthesize*)
module mk_divider_dp(Ifc_divider_dp);
   /*doc:reg: Stores the valid bit, final quotient and exceptions*/
   Reg#(Tuple3#(Bit#(1),FloatingPoint#(11,52),Exception)) rg_result <- mkDReg(tuple3(unpack(0),unpack(0),unpack(0)));
   /*doc:reg: The reg which stores the operands taken from send method*/
   Reg#(Tuple3#(FloatingPoint#(11,52), FloatingPoint#(11,52), RoundMode)) rg_operands <- mkReg(tuple3(unpack(0),unpack(0),unpack(0)));
   /*doc: TO control which rule to fire*/
   Reg#(Bit#(2)) rg_en <- mkReg(0);
   Reg#(Bit#(2)) rg_en1 <- mkReg(0);
   Reg#(Bit#(2)) rg_en2 <- mkReg(0);
   /*doc: Stores the divisor- HiddenBit + Mantissa(52) + 2 bits for convergence*/
   Reg#(Bit#(55)) rg_divisor <- mkReg(0);
   Reg#(Bit#(55)) rg_not_divisor <- mkReg(0);
   /*doc: Stores partial remainder and operand A, carry_bit + 2bits_for_convergence + dividend(A)*/
   Reg#(Bit#(56)) rg_p_a <- mkReg(0);
   Reg#(Bit#(56)) rg_p_a_cry <- mkReg(0);
   /*doc:reg: Register for holding positive quotient digits. It is extended to 58 bits for rounding*/
   //Reg#(Bit#(58)) rg_q_pos <-mkReg(0);
   Reg#(Bit#(58)) rg_a <-mkReg(0);
   /*doc:reg: Register for holding negative quotient digits. It is extended to 58 bits for rounding*/
   //Reg#(Bit#(58)) rg_q_neg <-mkReg(0);
   Reg#(Bit#(58)) rg_b <-mkReg(0);
   /*doc:reg: holds the final quotient*/
   Reg#(Bit#(58)) rg_final_rsfd <- mkReg(0);
   Reg#(Bit#(58)) rg_final_q1 <- mkReg(0);
   /*doc:reg: Register to track the number of iterations taken place.*/
   Reg#(Bit#(5)) rg_count <-mkReg(0);
   /*doc:reg: Stores the exception*/
   Reg#(Exception) rg_final_exc <- mkReg(defaultValue);
   /*doc:reg: To just store the final value without flags and valid bit*/
   Reg#(FloatingPoint#(11,52)) rg_final_result <- mkReg(defaultValue);
   /*doc:reg: To store the mantissa seperately*/
   Reg#(Bit#(53)) rg_sfdA <- mkReg(0);
   Reg#(Bit#(53)) rg_sfdB <- mkReg(0);
   /*doc:reg: Get the MSB zeros in inputs for normalisation*/
   Reg#(Bit#(TLog#(TAdd#(1, 53)))) rg_zerosA <- mkReg(0);
   Reg#(Bit#(TLog#(TAdd#(1, 53)))) rg_zerosB <- mkReg(0);
   /*doc:reg: To store exponent*/
   Reg#(Bit#(13)) rg_newexp <- mkReg(0);
   /*doc:reg: To store the shift required for subnormal mantissas*/
   Reg#(Bit#(11)) rg_final_shift <- mkReg(0);
   Reg#(Bit#(11)) rg_final_zeros <- mkReg(0);
   /*doc:reg: To differentiate results from division of no_exception inputs and exception inputs. 
			  Stores the exception result when tagged valid and vice versa.*/
   Reg#(Maybe#(FloatingPoint#(11,52))) rg_final_out <- mkReg(tagged Invalid);

   Reg#(Bit#(2)) rg_guard <- mkReg(0);

   Reg#(Int#(7)) rg_m_1 <- mkReg(0);
   Reg#(Int#(7)) rg_m0 <- mkReg(0);
   Reg#(Int#(7)) rg_m1 <- mkReg(0);
   Reg#(Int#(7)) rg_m2 <- mkReg(0);

   Reg#(Bool) bl_zero <- mkReg(False);

   let constants_lut <- mk_div_qds;

   /*doc:function:Returns the next quotient digit. Calculated by subtracting the remainder from the selection constants and checking the sign of the result.*/
   function Tuple2#(Bit#(2), Bit#(1)) fn_digit_select(Int#(7) m_1, Int#(7) m0, Int#(7) m1, Int#(7) m2, Bit#(7) p_a, Bit#(7) p_a_cry);

      Bit#(8) final_m_1, final_m0, final_m1, final_m2;
      final_m_1 = pack(signExtend(unpack(p_a + p_a_cry)) - signExtend(m_1));
      final_m0 = pack(signExtend(unpack(p_a + p_a_cry)) - signExtend(m0));
      final_m1 = pack(signExtend(unpack(p_a + p_a_cry)) - signExtend(m1));
      final_m2 = pack(signExtend(unpack(p_a + p_a_cry)) - signExtend(m2));

      Bit#(2) s;
      Bit#(1) s_sign;
      if (final_m2[7] == 0) begin s = 2; s_sign = 0; end
      else if (final_m1[7] == 0) begin s = 1; s_sign = 0; end
      else if (final_m0[7] == 0) begin s = 0; s_sign = 0; end
      else if (final_m_1[7] == 0) begin s = 1; s_sign = 1; end
      else begin s = 2; s_sign = 1; end

      return tuple2(s, s_sign);
   endfunction
   //******************************************************* Rules **************************************************************************

   /*doc:rule: The number of MSB zeros are calculated and the manitssas are shifted accordingly.*/
   rule rl_initial0(rg_en1 == 1);
      rg_en1 <= 2;
      match {.in1, .in2, .rmode} = rg_operands;
      Bit#(53) sfdA = {getHiddenBit(in1), in1.sfd};
      Bit#(53) sfdB = {getHiddenBit(in2), in2.sfd};

      // compute MSB 0s of sfd
   `ifdef count_zeros_specialize_dp
      Bit#(53) inp = sfdA;
      Vector#(12, Bit#(1)) v1 = unpack(inp[52:41]);                        
      Bit#(6) result1=0;                                  
      Bool done1 = False;
      for( Integer p1 = 11; p1 >=0; p1 = p1 - 1) begin
         if ((v1[p1] == 0)&&(!done1))  
            result1 = result1+1 ;
         else
            done1 = True;
      end
      Bit#(6) z0 = (result1);
   
      Vector#(12, Bit#(1)) v2 = unpack(inp[40:29]);
      Bit#(6) result2=0;                                   
      Bool done2 = False;
      for( Integer p2 = 11; p2 >=0; p2 = p2 - 1) begin
         if ((v2[p2] == 0)&&(!done2))  
            result2 = result2+1 ;
         else
           done2 = True;
      end
      Bit#(6) z1 = result2;
   
      Vector#(12, Bit#(1)) v3 = unpack(inp[28:17]);
      Bit#(6) result3=0;                                   
      Bool done3 = False;
      for( Integer p3 = 11; p3 >=0; p3 = p3 - 1) begin
         if ((v3[p3] == 0)&&(!done3))  
            result3 = result3+1 ;
         else
            done3 = True;
      end
      Bit#(6) z2 = result3;

      Vector#(12, Bit#(1)) v4 = unpack(inp[16:5]);
      Bit#(6)  result4=0;                                   
      Bool done4 = False;
      for( Integer p4 = 11; p4 >=0; p4 = p4 - 1) begin
         if ((v4[p4] == 0)&&(!done4))  
            result4 = result4+1 ;
         else
            done4 = True;
      end
      Bit#(6)  z3 = result4; 

      Vector#(5, Bit#(1)) v4_1 = unpack(inp[4:0]);
      Bit#(6)  result4_1=0;                                   
      Bool done4_1 = False;
      for( Integer p4_1 = 4; p4_1 >=0; p4_1 = p4_1 - 1) begin
         if ((v4_1[p4_1] == 0)&&(!done4_1))  
            result4_1 = result4_1+1 ;
         else
            done4_1 = True;
      end
      Bit#(6)  z3_1 = result4_1;

      Bit#(6) zerosA= (|inp[52:41]==1)?z0:((|inp[40:29]==1)?(z1+12):((|inp[28:17]==1)?(z2+24):((|inp[16:5]==1)?(z3+36):(|inp[4:0]==1)?z3_1+48:53)));

      //compute MSB 0s of sfdB
      Bit#(53) inp1 = sfdB;         
      Vector#(12, Bit#(1)) v5 = unpack(inp1[52:41]);
      Bit#(6) result5=0;                                  
      Bool done5 = False;
      for( Integer p5 = 11; p5 >=0; p5 = p5 - 1) begin
         if ((v5[p5] == 0)&&(!done5))  
            result5 = result5+1 ;
         else
            done5 = True;
      end
      Bit#(6) z4 = (result5);
   
      Vector#(12, Bit#(1)) v6 = unpack(inp1[40:29]);
      Bit#(6) result6=0;                                   
      Bool done6 = False;
      for( Integer p6 = 11; p6 >=0; p6 = p6 - 1) begin
         if ((v6[p6] == 0)&&(!done6)) 
           result6 = result6+1 ;
         else
           done6 = True;
      end
      Bit#(6) z5 = result6;

      Vector#(12, Bit#(1)) v7 = unpack(inp1[28:17]);      
      Bit#(6) result7=0;                                   
      Bool done7 = False;
      for( Integer p7 = 11; p7 >=0; p7 = p7 - 1) begin
         if ((v7[p7] == 0)&&(!done7))  
            result7 = result7+1 ;
         else
            done7 = True;
      end
      Bit#(6) z6 = result7;

      Vector#(12, Bit#(1)) v8 = unpack(inp1[16:5]);
      Bit#(6)  result8=0;                                   
      Bool done8 = False;
      for( Integer p8 = 11; p8 >=0; p8 = p8 - 1) begin
         if ((v8[p8] == 0)&&(!done8))  
            result8 = result8+1 ;
         else
            done8 = True;
      end
      Bit#(6)  z7 = result8; 

      Vector#(5, Bit#(1)) v8_1 = unpack(inp1[4:0]);
      Bit#(6)  result8_1=0;                                   
      Bool done8_1 = False;
      for( Integer p8_1 = 4; p8_1 >=0; p8_1 = p8_1 - 1) begin
         if ((v8_1[p8_1] == 0)&&(!done8_1))  
            result8_1 = result8_1+1 ;
         else
            done8_1 = True;
      end
      Bit#(6)  z7_1 = result8_1;

      Bit#(6) zerosB= (|inp1[52:41]==1)?z4:((|inp1[40:29]==1)?(z5+12):((|inp1[28:17]==1)?(z6+24):((|inp1[16:5]==1)?(z7+36):(|inp1[4:0]==1)?z7_1+48:53)));
      
   `else  //Calculate the MSB zeros using built in function.   
      Bit#(6) zerosA = pack(countZerosMSB(sfdA));
      Bit#(6) zerosB = pack(countZerosMSB(sfdB));
   `endif
      sfdA = sfdA << zerosA;
      sfdB = sfdB << zerosB;
      rg_zerosA <= zerosA;
      rg_zerosB <= zerosB;
      rg_sfdA <= sfdA;
      rg_sfdB <= sfdB;
      //$display("zeros are %d, %d", zerosA, zerosB);   
   endrule:rl_initial0


   /*doc:rule: Calculates the exponent*/	
   rule rl_initial1(rg_en1 == 2);	
      rg_en1 <= 3;
      match {.in1, .in2, .rmode} = rg_operands;
      let zerosA = rg_zerosA;
      let zerosB = rg_zerosB;

      Bit#(13) exp1 = isSubNormal(in1) ? fromInteger(minexp(in1)) : signExtend(unpack(unbias(in1)));
      Bit#(13) exp2 = isSubNormal(in2) ? fromInteger(minexp(in2)) : signExtend(unpack(unbias(in2)));
      Bit#(13) newexp = (exp1 - zeroExtend(unpack(pack(zerosA)))) - (exp2 - zeroExtend(unpack(pack(zerosB))));
      rg_newexp <= newexp;
    endrule:rl_initial1

	 /*doc:rule: Checks whehter it is an exception and assign registers accordingly.  */
    rule rl_initial(rg_en == 1 && rg_en1 == 3);
       rg_en <= 2;
       match {.in1, .in2, .rmode} = rg_operands;
       Maybe#(FloatingPoint#(11,52)) out = tagged Invalid;
       Exception exc = defaultValue;
       FloatingPoint#(11,52) result = defaultValue;
       Bit#(11) shift = 0;
       let sfdA = rg_sfdA;
       let sfdB = rg_sfdB;
       let newexp = rg_newexp;
       Bit#(13) lv_maxexp = fromInteger(maxexp(in1)+1);
       //$display("%d", lv_maxexp);
       Bool bl_maxexp = (newexp[12] == 1) ? False : (newexp[11:0] > lv_maxexp[11:0]);
       Bit#(13) lv_minexp_subnormal = fromInteger(minexp_subnormal(in1))-2;
       Bool bl_minexp_subnormal = (newexp[12] == 0) ? False : (newexp[11:0] < lv_minexp_subnormal[11:0]);
       Bit#(13) lv_minexp = fromInteger(minexp(result));
       Bool bl_minexp = (newexp[12] == 0) ? False : (newexp[11:0] < lv_minexp[11:0]);
		 
       // calculate the sign
       result.sign = in1.sign != in2.sign;
       //checking for exceptions
       if (isSNaN(in1)) begin
          out = tagged Valid nanQuiet(in1);
          exc.invalid_op = True;
       end
       else if (isSNaN(in2)) begin
          out = tagged Valid nanQuiet(in2);
          exc.invalid_op = True;
       end
       else if (isQNaN(in1)) begin
          out = tagged Valid in1;
       end
       else if (isQNaN(in2)) begin
          out = tagged Valid in2;
       end
       else if ((isInfinity(in1) && isInfinity(in2)) || (isZero(in1) && isZero(in2))) begin
          out = tagged Valid qnan();
          exc.invalid_op = True;
       end
       else if (isZero(in2) && !isInfinity(in1)) begin
          out = tagged Valid infinity(result.sign);
          exc.divide_0 = True;
       end
       else if (isInfinity(in1)) begin
          out = tagged Valid infinity(result.sign);
       end
       else if (isZero(in1) || isInfinity(in2)) begin
          out = tagged Valid zero(result.sign);
       end
       else if (bl_maxexp) begin
          result.exp = maxBound - 1;
          result.sfd = maxBound;
          exc.overflow = True;
          exc.inexact = True;
          let y = round(rmode, result, '1);
          out = tagged Valid tpl_1(y);
          exc = exc | tpl_2(y);
       end
       else if (bl_minexp_subnormal) begin
          result.exp = 0;
          result.sfd = 0;
          exc.underflow = True;
          exc.inexact = True;
          let y = round(rmode, result, 'b01);
          out = tagged Valid tpl_1(y);
          exc = exc | tpl_2(y);
       end
       else if (bl_minexp) begin
          result.exp = 0;
          shift = cExtend(fromInteger(minexp(result)) - newexp);
       end
       else begin
          result.exp = cExtend(newexp + fromInteger(bias(result)));
       end
       
       //If no exceptions then rg_divisor and rg_p_a are assigned accordingly.
       if (out matches tagged Invalid) begin
          Bit#(55) temp_d = {sfdB, 2'b00};
          rg_divisor <= temp_d;
          rg_not_divisor <= -temp_d;
          Bit#(TAdd#(1,55)) temp_p_a =  {3'b000, sfdA};
          rg_p_a <= temp_p_a;
          rg_p_a_cry <= 0;
          rg_a <= 0;
          rg_b <= 0;
       end

       match {.m_1, .m0, .m1, .m2} = constants_lut.qds_lut(sfdB[51:49]);
       rg_m_1 <= m_1;
       rg_m0 <= m0;
       rg_m1 <= m1;
       rg_m2 <= m2;

       rg_final_out <= out;
       rg_final_exc <= exc;
       rg_final_result <= result;
       rg_final_shift <= shift;
    endrule:rl_initial
    
    /*doc:rule: Calculates the division of the mantissaas based on SRT radix4 algorithm.*/
    rule rl_loop(rg_en ==2 && rg_count <= fromInteger(27));
       rg_count <= rg_count + 1;
       let p_a = rg_p_a;
       let p_a_cry = rg_p_a_cry;
       let divisor = rg_divisor;
       let not_divisor = rg_not_divisor;
       Bit#(2) q = 0; 
       Bit#(1) q_sign = 0;

       //Gets the next quotient digit.
       let lv_qds = fn_digit_select(rg_m_1, rg_m0, rg_m1, rg_m2, p_a[55:49], p_a_cry[55:49]);

       q = tpl_1(lv_qds);
       q_sign = tpl_2(lv_qds);

       Bit#(56) y = ?;
       let qp = rg_a;
       let qm = rg_b;
       Bit#(58) qp_new, qm_new;
       p_a = p_a<<2;
       p_a_cry = p_a_cry << 2;

       if(q == 2) begin
          if(q_sign == 1) begin
             y = {1'b0, divisor} << 1;
             qp_new = {qm[57:2], 2'b10};
             qm_new = {qm[57:2], 2'b01};
          end
          else begin
             y = {1'b1, not_divisor} << 1;
             qp_new = {qp[57:2], 2'b10};
             qm_new = {qp[57:2], 2'b01};
          end
       end
       else if(q==1) begin
          if(q_sign == 1) begin
             y = {1'b0, divisor};
             qp_new = {qm[57:2], 2'b11};
             qm_new = {qm[57:2], 2'b10};
          end
          else begin
             y = {1'b1, not_divisor};
             qp_new = {qp[57:2], 2'b01};
             qm_new = qp;
          end
       end
       else begin
          qp_new = qp;
          qm_new = {qm[57:2], 2'b11};
          y = 0;
       end

       Bit#(56) p_a_new, p_a_cry_new;
       p_a_cry_new[0] = 0;
       for (Integer i=0; i<55; i=i+1) begin
          Bit#(2) temp = zeroExtend(p_a[i]) + zeroExtend(p_a_cry[i]) + zeroExtend(y[i]);
          p_a_new[i] = temp[0];
          p_a_cry_new[i+1] = temp[1];
       end
       p_a_new[55] = p_a[55] + p_a_cry[55] + y[55];

       rg_p_a <= p_a_new;
       rg_p_a_cry <= p_a_cry_new;
       rg_a <= qp_new<<2;
       rg_b <= qm_new<<2;
       //$display("rg_count:%h: q_pos is %h, q_neg is %h, p_a is %h, q is %h, q_sign is %h", rg_count, qp, qm, p_a[55:49]+p_a_cry[55:49], q, q_sign);
    endrule:rl_loop

    /*doc:rule: Final quotient calculation*/
    rule rl_final1(rg_en2 == 0 && rg_count > fromInteger(valueOf(27)));
       rg_en2 <= 1;
       let q1 = rg_a;
       Bit#(56) p_a = rg_p_a;
       Bit#(56) p_a_cry = rg_p_a_cry;
       Bool zero = (p_a + p_a_cry == 0);
       Bool sign = unpack((p_a + p_a_cry)[55]);
       let shift = rg_final_shift;
       let out = rg_final_out;	
       Bit#(11) final_zeros = ?;
       if (out matches tagged Invalid) begin 
          q1 = q1 - zeroExtend(pack(sign));
          final_zeros = (q1[57:56]==2'b00)?2: ((q1[57:56]==2'b01 || q1[57:56]==2'b10)?1:0);
       end

       rg_final_q1 <= q1;
       bl_zero <= !zero;
       rg_final_zeros <= final_zeros;
    endrule:rl_final1
  
    rule rl_final1_1 (rg_en2 == 1);
       rg_en2 <= 2;
       let q1 = rg_final_q1;
       let result = rg_final_result;
       let exc = rg_final_exc;
       let rmode = tpl_3(rg_operands);
       let out = rg_final_out;
       let rem_zero = bl_zero;
       let shift = rg_final_shift;
       Bit#(58) rsfd = ?;
       Bit#(11) final_zeros = rg_final_zeros;
       final_zeros = final_zeros + shift;
       if (shift < fromInteger(valueOf(58))) begin
          Bit#(1) sfdlsb = |(pack(q1 << (fromInteger(valueOf(58)) - shift)));
          rsfd = q1 >> shift;
          rsfd[0] = rsfd[0] | sfdlsb;
       end
       else begin
          Bit#(1) sfdlsb = |(pack(q1));
          rsfd = 0;
          rsfd[0] = sfdlsb;
       end
       if (rem_zero) begin
          rsfd[0] = 1;
       end

       if (result.exp == maxBound) begin
          if (truncateLSB(rsfd) == 2'b00) begin
             rsfd = rsfd << 1;
             result.exp = result.exp - 1;
             final_zeros = final_zeros - 1;
          end
          else begin
             result.exp = maxBound - 1;
             result.sfd = maxBound;
             exc.overflow = True;
             exc.inexact = True;
             let y = round(rmode, result, '1);
             out = tagged Valid tpl_1(y);
             exc = exc | tpl_2(y);
          end
       end
       rg_final_rsfd <= rsfd;
       rg_final_exc <= exc;
       rg_final_result <= result;
       rg_final_out <= out;
       rg_final_zeros <= final_zeros;
    endrule 
    /*doc:rule: Final exception checking, normalisation*/
    rule rl_final2(rg_en2 == 2);
       rg_en2 <= 3;
       let result = rg_final_result;
       let exc = rg_final_exc;
       let rmode = tpl_3(rg_operands);
       let out = rg_final_out;
       let rsfd = rg_final_rsfd;
       let final_zeros = rg_final_zeros;
       Bit#(2) guard = ?;

       if (out matches tagged Invalid) begin
          match {.out_, .guard_, .exc_} = fn_normalize_dp(result, rsfd, final_zeros);
          result = out_;
          guard = guard_;
          exc = exc | exc_;
       end

       rg_final_result <= result;
       rg_guard <= guard;
       rg_final_exc <= exc;
       rg_final_out <= out;
    endrule: rl_final2

    /*doc:rule:Rounding*/
    rule rl_final3(rg_en2 == 3);
       rg_en2 <= 0;
       rg_count <= 0;
       rg_en <= 0;
       let rmode = tpl_3(rg_operands);
       let out = rg_final_out;
       let exc = rg_final_exc;
       let guard = rg_guard;
       let result =  rg_final_result;
       if (out matches tagged Valid .x) begin
          result = x;
       end
       else begin
          match {.out_, .exc_} = round(rmode,result,guard);
          result = out_;
          exc = exc | exc_;
       end
          rg_result <= tuple3(1,canonicalize(result),exc);
   endrule:rl_final3
   
   //********************************************* Methods *****************************************************************************************
	/*doc:method: To get the inputs and rounding mode*/
   method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) operands);
      rg_en <= 1;
      rg_operands <= operands;
      rg_en1 <= 1;
      rg_en2 <= 0;
   endmethod
   /*doc:method: To return the result*/
   method ReturnType#(11,52) receive(); 
      let y = ReturnType{valid:tpl_1(rg_result),value:tpl_2(rg_result),ex:tpl_3(rg_result)};
      return y;
   endmethod

endmodule
//`endif
endpackage
