////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Neel Gala, Shalender Kumar, Sujay Pandit, Lokhesh Kumar, Charulatha Narashiman
Email id: neelgala@gmail.com, cs18m050@smail.iitm.ac.in, contact.sujaypandit@gmail.com, lokhesh.kumar@gmail.com, charuswathi112@gmail.com
--------------------------------------------------------------------------------------------------
*/
////////////////////////////////////////////////////////////////////////////////
//  Filename      : fpu_div_sqrt.bsv
////////////////////////////////////////////////////////////////////////////////
package fpu_div_sqrt_hierarchical;
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import DReg              ::*;
  `include "Logger.bsv"

`ifdef fpu_hierarchical

////////////////////////////////////////////////////////////////////////////////
/// Square Root
////////////////////////////////////////////////////////////////////////////////

interface Ifc_sqrt_sp;
   method Action send(Tuple2#(FloatingPoint#(8,23), RoundMode) operands);
   method ReturnType#(8,23) receive(); 
endinterface
	
interface Ifc_sqrt_dp;
   method Action send(Tuple2#(FloatingPoint#(11,52), RoundMode) operands);
   method ReturnType#(11,52) receive(); 
endinterface

// Sqrt SP
(*synthesize*)
module mk_sqrt_sp(Ifc_sqrt_sp);

   Reg#(Tuple3#(Bit#(1),FloatingPoint#(8,23),Exception)) rg_result <- mkDReg(tuple3(unpack(0),unpack(0),unpack(0)));
   Reg#(Tuple2#(UInt#(60),Bool)) rg_result1 <- mkReg(tuple2(unpack(0),unpack(0)));
   Reg#(Tuple2#(FloatingPoint#(8,23), RoundMode)) rg_operands <- mkReg(tuple2(unpack(0),unpack(0)));

   Reg#(Bit#(60)) rg_s <- mkReg(0);
   Reg#(Bit#(60)) rg_r <- mkReg(0);
   Reg#(Bit#(60)) rg_b <- mkReg(0);
   Reg#(Bit#(60)) rg_sum <- mkReg(0);
   Reg#(Bit#(60)) rg_sum1<- mkReg(0);
   Reg#(Maybe#(Bit#(60))) rg_res <- mkReg(tagged Invalid);
   Reg#(Bit#(60)) rg_opsfd <- mkReg(0);
   Reg#(Bit#(2)) rg_en <- mkReg(0);
   Reg#(Bool) rg_en1 <- mkReg(False);
   Reg#(UInt#(TLog#(60))) rg_count <- mkReg(0);
   Reg#(UInt#(TLog#(TAdd#(1, 60)))) rg_shift <- mkReg(0);
   Reg#(Bool) rg_valid <-mkDReg(False);
   Reg#(Maybe#(FloatingPoint#(8,23))) rg_final_result <- mkReg(tagged Invalid);
   Reg#(FloatingPoint#(8,23)) rg_final_out <- mkReg(unpack(0));
   Reg#(Exception) rg_final_exc <- mkReg(defaultValue);
   Reg#(Bool) rg_cmp <- mkReg(False);


   //*************************************************** rules ********************************************************************************

   rule rl_initial1(rg_en == 2);
      
      rg_en <= 3;
      Bit#(60) b=zExtendLSB(2'b01);
      Bit#(60) s = pack(rg_opsfd);
      
      // counting the MSB 0s of s
      Bit#(60) inp = s;
      Vector#(15, Bit#(1)) v1 = unpack(inp[59:45]);                        
      UInt#(6) result1=0;                                  
      Bool done1 = False;
      for( Integer p1 = 14; p1 >=0; p1 = p1 - 1)  begin
         if ((v1[p1] == 0)&&(!done1))  
            result1 = result1+1 ;
         else
            done1 = True;
      end
      UInt#(6) z0 = (result1);
   
      Vector#(15, Bit#(1)) v2 = unpack(inp[44:30]);                       
      UInt#(6) result2=0;                                   
      Bool done2 = False;
      for( Integer p2 = 14; p2 >=0; p2 = p2 - 1) begin
         if ((v2[p2] == 0)&&(!done2))  
            result2 = result2+1 ;
         else
            done2 = True;
      end
      UInt#(6) z1 = result2;
   
      Vector#(15, Bit#(1)) v3 = unpack(inp[29:15]);                       
      UInt#(6) result3=0;                                   
      Bool done3 = False;
      for( Integer p3 = 14; p3 >=0; p3 = p3 - 1) begin
         if ((v3[p3] == 0)&&(!done3))  
            result3 = result3+1 ;
         else
            done3 = True;
      end
      UInt#(6) z2 = result3;

      Vector#(15, Bit#(1)) v4 = unpack(inp[14:0]);                       
      UInt#(6)  result4=0;                                   
      Bool done4 = False;
      for( Integer p4 = 14; p4 >=0; p4 = p4 - 1) begin
         if ((v4[p4] == 0)&&(!done4))  
            result4 = result4+1 ;
         else
            done4 = True;
      end
      UInt#(6)  z3 = result4;
   
      //MSB 0s of s0 computed
      UInt#(6) s0= (|inp[59:45]==1)?z0:((|inp[44:30]==1)?(z1+15):((|inp[29:15]==1)?(z2+30):((|inp[14:0]==1)?(z3+45):60)));
      UInt#(6) b0= 'h01;

      if (s0 > 0) begin
         let shift = (s0 - b0);
	 if ((shift & 1) == 1)
	    shift = shift + 1;
	 b = b >> shift;
      end

      rg_r <= 0;
      rg_b <= b;
      rg_s <= s;
   endrule:rl_initial1

   rule rl_loop0(rg_en == 3 && rg_count <= fromInteger(valueOf(60)) && !rg_en1);
      
      rg_en1 <= True;
      let sum = rg_r + rg_b;
      rg_sum <= sum;
      rg_cmp <= (rg_s >= sum);
   endrule:rl_loop0

   rule rl_loop1(rg_en == 3 && rg_count <= fromInteger(valueOf(60)) && !rg_en1);

      let r = rg_r;
      r = r >> 1;
      let sum = r + rg_b;
      rg_sum1 <= sum;
   endrule:rl_loop1

   rule rl_loop(rg_en == 3 && rg_count <= fromInteger(valueOf(60)) && rg_en1);
      rg_en1 <= False;
      rg_count <= rg_count + 2;
      Maybe#(Bit#(60)) res = tagged Invalid;
      Bit#(60) s;
      Bit#(60) r;
      Bit#(60) b=0;
      r = rg_r;
      s = rg_s;
      b = rg_b;
      res = rg_res;
      res = tagged Valid r;
      if(b!=0) begin

         let sum = rg_sum;
	 r = r >> 1;
	 if(rg_cmp) begin
	    s = s - sum;
	    r = rg_sum1;
	 end
         b = b >> 2;
      end
      rg_b <= b;
      rg_r <= r;
      rg_s <= s;
      rg_res <= res;		
   endrule :rl_loop

   rule rl_initial(rg_en == 1);

      rg_en <= 2;
      match {.op, .rmode} = rg_operands;
      Exception exc = defaultValue;
      Maybe#(FloatingPoint#(8,23)) result = tagged Invalid;
      let out = op;
      Bit#(30) sfd = zExtendLSB({1'b0, getHiddenBit(op), op.sfd});
      
      if (isSNaN(op)) begin
         exc.invalid_op = True;
	 result = tagged Valid nanQuiet(op);
      end
      else if (isQNaN(op) || isZero(op) || (isInfinity(op) && (op.sign == False))) begin
	 result = tagged Valid op;
      end
      else if (op.sign) begin
	 exc.invalid_op = True;
	 result = tagged Valid qnan();
      end
      else begin
	 Int#(TAdd#(8,2)) exp = isSubNormal(op) ? fromInteger(minexp(op)) : signExtend(unpack(unbias(out)));
	     
         //counting MSB 0s of sfd
         Bit#(30) inp = sfd;
         Vector#(15, Bit#(1)) v1 = unpack(inp[29:15]);                        
         UInt#(5) result1=0;                                  
         Bool done1 = False;
         for( Integer p1 = 14; p1 >=0; p1 = p1 - 1) begin
            if ((v1[p1] == 0)&&(!done1))  
               result1 = result1+1 ;
            else
               done1 = True;
         end
         UInt#(5) z0 = (result1);
   
         Vector#(15, Bit#(1)) v2 = unpack(inp[14:0]);                        
         UInt#(5) result2=0;                                   
         Bool done2 = False;
         for( Integer p2 = 14; p2 >=0; p2 = p2 - 1) begin
            if ((v2[p2] == 0)&&(!done2))  
               result2 = result2+1 ;
            else
               done2 = True;
         end
         UInt#(5) z1 = result2;
         //MSB 0s of sfd computed
         UInt#(5) zeros = (|inp[29:15]==1)?z0:((|inp[14:0]==1)?(z1+15):30);
	 sfd = sfd << (zeros - 1);
	 exp = exp - zeroExtend(unpack(pack(zeros)));
	 out.exp = truncate(pack(exp >> 1) + fromInteger(bias(out)) + 1);
	 if ((exp & 1) == 0) begin
	    sfd = sfd >> 1;
	 end
      end
      
      UInt#(TMul#(30,2)) s=0;
      Bit#(TMul#(30,2)) opsfd = zExtendLSB(sfd);
      Bool inexact = False;
      
      if (result matches tagged Invalid) begin
         rg_opsfd <= opsfd;
      end
      rg_final_result <= result;
      rg_final_out <= out;
      rg_final_exc <= exc;

   endrule:rl_initial

   rule rl_final(rg_count > fromInteger(valueOf(60)));

      rg_count <= 0;
      rg_en <= 0;
      Bit#(TAdd#(30,1)) sfd1 = ?;
      let result = rg_final_result;
      let out = rg_final_out;
      let exc = rg_final_exc;
      let rmode = tpl_2(rg_operands);
      if (result matches tagged Invalid) begin
         let {s, inexact} = tuple2((fromMaybe(0,rg_res)),(rg_s != 0));
	 sfd1 = truncate(pack(s));
	 if (inexact) begin
            sfd1[0] = 1;
	 end
      end
      Bit#(2) guard = ?;
      if (result matches tagged Invalid) begin
	 match {.out_, .guard_, .exc_} = normalize(out, sfd1);
	 out = out_;
	 guard = guard_;
	 exc = exc_;
      end
      if (result matches tagged Valid .x) begin
         out = x;
      end
      else begin
         match {.out_, .exc_} = round(rmode, out, guard);
	 out = out_;
	 exc = exc | exc_;
      end
      rg_result <= tuple3(1'b1, canonicalize(out), exc);

   endrule:rl_final
   //******************************************************************************************************************************************

   method Action send(Tuple2#(FloatingPoint#(8,23), RoundMode) operands);
      rg_en <= 1;
      rg_count <= 0;
      rg_operands <= operands;
   endmethod
	
   method ReturnType#(8,23) receive(); 
      let x = ReturnType{valid:tpl_1(rg_result),value:tpl_2(rg_result),ex:tpl_3(rg_result)};
      return x;
   endmethod
endmodule

// Sqrt DP
(*synthesize*)
module mk_sqrt_dp(Ifc_sqrt_dp);
   Reg#(Tuple3#(Bit#(1),FloatingPoint#(11,52),Exception)) rg_result <- mkDReg(tuple3(unpack(0),unpack(0),unpack(0)));
   Reg#(Tuple2#(UInt#(116),Bool)) rg_result1 <- mkReg(tuple2(unpack(0),unpack(0)));
   Reg#(Tuple2#(FloatingPoint#(11,52), RoundMode)) rg_operands <- mkReg(tuple2(unpack(0),unpack(0)));

   Reg#(Bit#(116)) rg_s <- mkReg(0);
   Reg#(Bit#(116)) rg_r <- mkReg(0);
   Reg#(Bit#(116)) rg_b <- mkReg(0);
   Reg#(Bit#(116)) rg_sum <- mkReg(0);
   Reg#(Bit#(116)) rg_sum1<- mkReg(0);
   Reg#(Maybe#(Bit#(116))) rg_res <- mkReg(tagged Invalid);
   Reg#(Bit#(116)) rg_opsfd <- mkReg(0);
   Reg#(Bit#(2)) rg_en <- mkReg(0);
   Reg#(Bool) rg_en1 <- mkReg(False);
   Reg#(UInt#(TLog#(116)))rg_count <- mkReg(0);
   Reg#(UInt#(TLog#(TAdd#(1, 116)))) rg_shift <- mkReg(0);
   Reg#(Bool) rg_valid <-mkDReg(False);
   Reg#(Maybe#(FloatingPoint#(11,52))) rg_final_result <- mkReg(tagged Invalid);
   Reg#(FloatingPoint#(11,52)) rg_final_out <- mkReg(unpack(0));
   Reg#(Exception) rg_final_exc <- mkReg(defaultValue);
   Reg#(Bool) rg_cmp <- mkReg(False);

   //*************************************************** rules ********************************************************************************

   rule rl_initial1(rg_en == 2);

      rg_en <= 3;
      Bit#(116) b=zExtendLSB(2'b01);
      Bit#(116) s = pack(rg_opsfd);
      // compute MSB 0s of s
      Bit#(116) inp = s;
      Vector#(16, Bit#(1)) v1 = unpack(inp[115:100]);                        
      UInt#(7)  result1=0;                                  
      Bool done1 = False;
      for( Integer p1 = 15; p1 >=0; p1 = p1 - 1) begin
         if ((v1[p1] == 0)&&(!done1))  
            result1 = result1+1 ;
         else
            done1 = True;
      end
      UInt#(7) z0 = (result1);
   
      Vector#(16, Bit#(1)) v2 = unpack(inp[99:84]);                       
      UInt#(7) result2=0;                                   
      Bool done2 = False;
      for( Integer p2 = 15; p2 >=0; p2 = p2 - 1) begin
         if ((v2[p2] == 0)&&(!done2))  
            result2 = result2+1 ;
         else
            done2 = True;
      end
      UInt#(7) z1 = result2;
   
      Vector#(16, Bit#(1)) v3 = unpack(inp[83:68]);                        
      UInt#(7) result3=0;                                   
      Bool done3 = False;
      for( Integer p3 = 15; p3 >=0; p3 = p3 - 1) begin
         if ((v3[p3] == 0)&&(!done3))  
            result3 = result3+1 ;
         else
            done3 = True;
      end
      UInt#(7) z2 = result3;
   
      Vector#(16, Bit#(1)) v4 = unpack(inp[67:52]);                       
      UInt#(7) result4=0;                                   
      Bool done4 = False;
      for( Integer p4 = 15; p4 >=0; p4 = p4 - 1) begin
         if ((v4[p4] == 0)&&(!done4))  
            result4 = result4+1 ;
         else
            done4 = True;
      end
      UInt#(7) z3 = result4;

      Vector#(16, Bit#(1)) v5 = unpack(inp[51:36]);                        
      UInt#(7) result5=0;                                   
      Bool done5 = False;
      for( Integer p5 = 15; p5 >=0; p5 = p5 - 1) begin
         if ((v5[p5] == 0)&&(!done5))  
            result5 = result5+1 ;
         else
            done5 = True;
      end
      UInt#(7) z4 = result5;

      Vector#(16, Bit#(1)) v6 = unpack(inp[35:20]);                        
      UInt#(7) result6=0;                                   
      Bool done6 = False;
      for( Integer p6 = 15; p6 >=0; p6 = p6 - 1) begin
         if ((v6[p6] == 0)&&(!done6))  
            result6 = result6+1 ;
         else
            done6 = True;
      end
      UInt#(7) z5 = result6;

      Vector#(16, Bit#(1)) v7 = unpack(inp[19:4]);                        
      UInt#(7) result7=0;                                   
      Bool done7 = False;
      for( Integer p7 = 15; p7 >=0; p7 = p7 - 1) begin
         if ((v7[p7] == 0)&&(!done7))  
            result7 = result7+1 ;
         else
            done7 = True;
      end
      UInt#(7) z6 = result7;

      Vector#(4, Bit#(1)) v8 = unpack(inp[3:0]);                        
      UInt#(7) result8=0;                                   
      Bool done8 = False;
      for( Integer p8 = 3; p8 >=0; p8 = p8 - 1) begin
        if ((v8[p8] == 0)&&(!done8))  
           result8 = result8+1 ;
        else
           done8 = True;
      end
      UInt#(7) z7 = result8;
      // MSB 0s of s computed
      UInt#(7) s0= (|inp[115:100]==1)?z0:((|inp[99:84]==1)?(z1+16):((|inp[83:68]==1)?(z2+32):((|inp[67:52]==1)?(z3+48):((|inp[51:36]==1)?(z4+64):((|inp[35:20]==1)?(z5+80):((|inp[19:4]==1)?(z6+96):((|inp[3:0]==1)?(z7+112):116)))))));
      UInt#(7) b0= 'h01;
      if (s0 > 0) begin
         let shift = (s0 - b0);
	 if ((shift & 1) == 1)
	    shift = shift + 1;
	 b = b >> shift;
      end
      rg_r <= 0;
      rg_b <= b;
      rg_s <= s;
   endrule:rl_initial1


   rule rl_loop0(rg_en == 3 && rg_count <= fromInteger(valueOf(116)) && !rg_en1);

      rg_en1 <= True;
      let sum = rg_r + rg_b;
      rg_sum <= sum;
      rg_cmp <= (rg_s >= sum);
   endrule:rl_loop0

   rule rl_loop1(rg_en == 3 && rg_count <= fromInteger(valueOf(116)) && !rg_en1);
      
      let r = rg_r;
      r = r >> 1;
      let sum = r + rg_b;
      rg_sum1 <= sum;
   endrule:rl_loop1

   rule rl_loop(rg_en == 3 && rg_count <= fromInteger(valueOf(116)) && rg_en1);
      rg_en1 <= False;
      rg_count <= rg_count + 2;
      Maybe#(Bit#(116)) res = tagged Invalid;
      Bit#(116) s;
      Bit#(116) r;
      Bit#(116) b=0;
      r = rg_r;
      s = rg_s;
      b = rg_b;
      res = rg_res;
      res = tagged Valid r;
      if(b!=0) begin
         let sum = rg_sum;
         r = r >> 1;
         if(rg_cmp) begin
	    s = s - sum;
            r = rg_sum1;
	 end
         b = b >> 2;
       end
       rg_b <= b;
       rg_r <= r;
       rg_s <= s;
       rg_res <= res;		
   endrule :rl_loop

   rule rl_initial(rg_en == 1);

      rg_en <= 2;
      match {.op, .rmode} = rg_operands;
      Exception exc = defaultValue;
      Maybe#(FloatingPoint#(11,52)) result = tagged Invalid;
      let out = op;
      Bit#(58) sfd = zExtendLSB({1'b0, getHiddenBit(op), op.sfd});
      
      if (isSNaN(op)) begin
         exc.invalid_op = True;
	 result = tagged Valid nanQuiet(op);
      end
      else if (isQNaN(op) || isZero(op) || (isInfinity(op) && (op.sign == False))) begin
	 result = tagged Valid op;
      end
      else if (op.sign) begin
	 exc.invalid_op = True;
	 result = tagged Valid qnan();
      end
      else begin
	 Int#(TAdd#(11,2)) exp = isSubNormal(op) ? fromInteger(minexp(op)) : signExtend(unpack(unbias(out)));
         //computing MSB 0s of sfd
	 Bit#(58) inp = sfd;
         Vector#(16, Bit#(1)) v1 = unpack(inp[57:42]);                        //counting MSB 0's of input
         UInt#(6)  result1=0;                                  
         Bool done1 = False;
         for( Integer p1 = 15; p1 >=0; p1 = p1 - 1) begin
            if ((v1[p1] == 0)&&(!done1))  
               result1 = result1+1 ;
            else
               done1 = True;
         end
         UInt#(6) z0 = (result1);
   
         Vector#(16, Bit#(1)) v2 = unpack(inp[41:26]);                        //counting MSB 0's of input
         UInt#(6) result2=0;                                   
         Bool done2 = False;
         for( Integer p2 = 15; p2 >=0; p2 = p2 - 1) begin
            if ((v2[p2] == 0)&&(!done2))  
               result2 = result2+1 ;
            else
               done2 = True;
         end
         UInt#(6) z1 = result2;
   
         Vector#(16, Bit#(1)) v3 = unpack(inp[25:10]);                        //counting MSB 0's of input
         UInt#(6) result3=0;                                   
         Bool done3 = False;
         for( Integer p3 = 15; p3 >=0; p3 = p3 - 1)  begin
            if ((v3[p3] == 0)&&(!done3))  
               result3 = result3+1 ;
            else
               done3 = True;
         end
         UInt#(6) z2 = result3;
   
         Vector#(10, Bit#(1)) v4 = unpack(inp[9:0]);                        //counting MSB 0's of input
         UInt#(6) result4=0;                                   
         Bool done4 = False;
         for( Integer p4 = 9; p4 >=0; p4 = p4 - 1)  begin
            if ((v4[p4] == 0)&&(!done4))  
               result4 = result4+1 ;
            else
               done4 = True;
         end
         UInt#(6) z3 = result4;
         //  MSB 0s of sfd computed
         UInt#(6) zeros= (|inp[57:42]==1)?z0:((|inp[41:26]==1)?(z1+16):((|inp[25:10]==1)?(z2+32):((|inp[9:0]==1)?(z3+48):58)));
	 sfd = sfd << (zeros - 1);
	 exp = exp - zeroExtend(unpack(pack(zeros)));
	 out.exp = truncate(pack(exp >> 1) + fromInteger(bias(out)) + 1);
	 if ((exp & 1) == 0) begin
	    sfd = sfd >> 1;
	 end
      end
      
      UInt#(TMul#(58,2)) s=0;
      Bit#(TMul#(58,2)) opsfd = zExtendLSB(sfd);
      Bool inexact = False;
      if (result matches tagged Invalid) begin
         rg_opsfd <= opsfd;
      end
      rg_final_result <= result;
      rg_final_out <= out;
      rg_final_exc <= exc;

   endrule:rl_initial
 
   rule rl_final(rg_count > fromInteger(valueOf(116)));

      rg_count <= 0;
      rg_en <= 0;
      Bit#(TAdd#(58,1)) sfd1 = ?;
      let result = rg_final_result;
      let out = rg_final_out;
      let exc = rg_final_exc;
      let rmode = tpl_2(rg_operands);
      if (result matches tagged Invalid) begin
         let {s, inexact} = tuple2((fromMaybe(0,rg_res)),(rg_s != 0));
	 sfd1 = truncate(pack(s));
	 if (inexact) begin
            sfd1[0] = 1;
	 end
      end     
      Bit#(2) guard = ?;
      if (result matches tagged Invalid) begin
	 match {.out_, .guard_, .exc_} = normalize(out, sfd1);
	 out = out_;
	 guard = guard_;
	 exc = exc_;
      end
      if (result matches tagged Valid .x) begin
	 out = x;
      end
      else begin
	 match {.out_, .exc_} = round(rmode, out, guard);
	 out = out_;
	 exc = exc | exc_;
      end
      rg_result <= tuple3(1'b1, canonicalize(out), exc);

   endrule:rl_final
   //******************************************************************************************************************************************
   
   method Action send(Tuple2#(FloatingPoint#(11,52), RoundMode) operands); 
      rg_en <= 1;
      rg_count <= 0;
      rg_operands <= operands;
   endmethod
	
   method ReturnType#(11,52) receive(); 
      let x = ReturnType{valid:tpl_1(rg_result),value:tpl_2(rg_result),ex:tpl_3(rg_result)};
      return x;
   endmethod
endmodule



////////////////////////////////////////////////////////////////////////////////
/// Divide
////////////////////////////////////////////////////////////////////////////////

interface Ifc_divider_sp;
   method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) operands);
   method ReturnType#(8,23) receive(); 
endinterface

interface Ifc_divider_dp;
   method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) operands);
   method ReturnType#(11,52) receive(); 
endinterface

//Divider SP
(*synthesize*)
module mk_divider_sp(Ifc_divider_sp);
       
   Reg#(Tuple3#(Bit#(1),FloatingPoint#(8,23),Exception)) rg_result <- mkDReg(tuple3(unpack(0),unpack(0),unpack(0)));
   Reg#(Tuple3#(FloatingPoint#(8,23), FloatingPoint#(8,23), RoundMode)) rg_operands <- mkReg(tuple3(unpack(0),unpack(0),unpack(0)));
   Reg#(Bit#(TLog#(TAdd#(28,1)))) rg_count <-mkReg(0);
   Reg#(Bit#(TAdd#(1,28))) rg_q<- mkReg(0);
   Reg#(Bit#(TAdd#(1,28))) rg_d <- mkReg(0);
   Reg#(Bit#(TAdd#(2,56))) rg_r <-mkReg(0);
   Reg#(Bit#(TAdd#(2,56))) rg_bigd <- mkReg(0);
   Reg#(Bit#(2)) rg_en <- mkReg(0);
   Reg#(Maybe#(FloatingPoint#(8,23))) rg_final_out <- mkReg(tagged Invalid);
   Reg#(Exception) rg_final_exc <- mkReg(defaultValue);
   Reg#(FloatingPoint#(8,23)) rg_final_result <- mkReg(defaultValue);
   Reg#(Bit#(8)) rg_final_shift <- mkReg(0);
   Reg#(Bit#(2)) rg_en1 <- mkReg(0);
   Reg#(Bool) rg_en2 <- mkReg(False);
   Reg#(Bit#(24)) rg_sfdA <- mkReg(0);
   Reg#(Bit#(24)) rg_sfdB <- mkReg(0);
   Reg#(Bit#(10)) rg_newexp <- mkReg(0);
   Reg#(Bit#(56))  rg_opA <- mkReg(0);
   Reg#(Bit#(28))  rg_opB <- mkReg(0);
   Reg#(Bit#(TLog#(TAdd#(1, 24)))) rg_zerosA <- mkReg(0);
   Reg#(Bit#(TLog#(TAdd#(1, 24)))) rg_zerosB <- mkReg(0);
   Reg#(Bit#(29)) rg_final_rsfd <- mkReg(0);


   //******************************************************* Rules **************************************************************************
   rule rl_initial0(rg_en1 == 1);
      rg_en1 <= 2;
      match {.in1, .in2, .rmode} = rg_operands;
      Bit#(24) sfdA = {getHiddenBit(in1), in1.sfd};
      Bit#(24) sfdB = {getHiddenBit(in2), in2.sfd};
      //compute MSB 0s of sfdA
      Bit#(24) inp = sfdA;
      Vector#(15, Bit#(1)) v1 = unpack(inp[23:9]);                        
      Bit#(5) result1=0;                                  
      Bool done1 = False;
      for( Integer p1 = 14; p1 >=0; p1 = p1 - 1) begin
         if ((v1[p1] == 0)&&(!done1))  
            result1 = result1+1 ;
         else
            done1 = True;
      end
      Bit#(5) z0 = (result1);
   
      Vector#(9, Bit#(1)) v2 = unpack(inp[8:0]);                        
      Bit#(5) result2=0;                                   
      Bool done2 = False;
      for( Integer p2 = 8; p2 >=0; p2 = p2 - 1) begin
         if ((v2[p2] == 0)&&(!done2))  
            result2 = result2+1 ;
         else
            done2 = True;
      end
      Bit#(5) z1 = result2;
      Bit#(5) zerosA = (|inp[23:9]==1)?z0:((|inp[8:0]==1)?(z1+15):24);	

      //compute MSB 0s of sfdB		
      Bit#(24) inp1 = sfdB;
      Vector#(15, Bit#(1)) v3 = unpack(inp1[23:9]);                       
      Bit#(5) result3=0;                                  
      Bool done3 = False;
      for( Integer p3 = 14; p3 >=0; p3 = p3 - 1) begin
         if ((v3[p3] == 0)&&(!done3))  
            result3 = result3+1 ;
         else
            done3 = True;
      end
      Bit#(5) z2 = (result3);
   
      Vector#(9, Bit#(1)) v4 = unpack(inp1[8:0]);                       
      Bit#(5) result4=0;                                   
      Bool done4 = False;
      for( Integer p4 = 8; p4 >=0; p4 = p4 - 1) begin
         if ((v4[p4] == 0)&&(!done4))  
            result4 = result4+1 ;
         else
            done4 = True;
      end
      Bit#(5) z3 = result4;
      Bit#(5) zerosB = (|inp1[23:9]==1)?z2:((|inp1[8:0]==1)?(z3+15):24);
      sfdA = sfdA << zerosA;
      sfdB = sfdB << zerosB;
      rg_zerosA <= zerosA;
      rg_zerosB <= zerosB;
      rg_sfdA <= sfdA;
      rg_sfdB <= sfdB;
   endrule:rl_initial0
   
   rule rl_initial1(rg_en1 == 2);	
      rg_en1 <= 3;
      match {.in1, .in2, .rmode} = rg_operands;
      let zerosA = rg_zerosA;
      let zerosB = rg_zerosB;
      let sfdA = rg_sfdA;
      let sfdB = rg_sfdB;
      Bit#(10) exp1 = isSubNormal(in1) ? fromInteger(minexp(in1)) : signExtend(unpack(unbias(in1)));
      Bit#(10) exp2 = isSubNormal(in2) ? fromInteger(minexp(in2)) : signExtend(unpack(unbias(in2)));
      Bit#(10) newexp = (exp1 - zeroExtend(unpack(pack(zerosA)))) - (exp2 - zeroExtend(unpack(pack(zerosB))));
      Bit#(56) opA = zExtendLSB({ 1'b0, sfdA });
      Bit#(28) opB = zExtend({ sfdB, 4'b0000 });
      rg_opA <= opA;
      rg_opB <= opB;
      rg_newexp <= newexp;
		
    endrule:rl_initial1

    rule rl_initial(rg_en == 1 && rg_en1 == 3);
       rg_en <= 2;
       match {.in1, .in2, .rmode} = rg_operands;
       Maybe#(FloatingPoint#(8,23)) out = tagged Invalid;
       Exception exc = defaultValue;
       FloatingPoint#(8,23) result = defaultValue;
       Bit#(8) shift = 0;
       let sfdA = rg_sfdA;
       let sfdB = rg_sfdB;
       //Int#(10) newexp = unpack(rg_newexp);
		 let newexp = rg_newexp;
       let opA = rg_opA;
       let opB = rg_opB;
		 Bit#(10) temp1 = fromInteger(maxexp(in1)+1);
		 Bool temp1_bl = (newexp[9] == 1) ? False : (newexp[8:0] > temp1[8:0]);
		 Bit#(10) temp2 = fromInteger(minexp_subnormal(in1))-2;
		 Bool temp2_bl = (newexp[9] == 0) ? False : (newexp[8:0] < temp2[8:0]);
		 Bit#(10) temp3 = fromInteger(minexp(result));
		 Bool temp3_bl = (newexp[9] == 0) ? False : (newexp[8:0] < temp3[8:0]);
		 
       // calculate the sign
       result.sign = in1.sign != in2.sign;
       if (isSNaN(in1)) begin
			 out = tagged Valid nanQuiet(in1);
			 exc.invalid_op = True;
       end
       else if (isSNaN(in2)) begin
			 out = tagged Valid nanQuiet(in2);
			 exc.invalid_op = True;
       end
       else if (isQNaN(in1)) begin
			 out = tagged Valid in1;
       end
		 else if (isQNaN(in2)) begin
			 out = tagged Valid in2;
       end
     	 else if ((isInfinity(in1) && isInfinity(in2)) || (isZero(in1) && isZero(in2))) begin
			 out = tagged Valid qnan();
			 exc.invalid_op = True;
       end
       else if (isZero(in2) && !isInfinity(in1)) begin
			 out = tagged Valid infinity(result.sign);
			 exc.divide_0 = True;
       end
       else if (isInfinity(in1)) begin
			 out = tagged Valid infinity(result.sign);
       end
       else if (isZero(in1) || isInfinity(in2)) begin
			 out = tagged Valid zero(result.sign);
       end
		 else if (temp1_bl) begin
			 result.exp = maxBound - 1;
			 result.sfd = maxBound;
          exc.overflow = True;
			 exc.inexact = True;
			 let y = round(rmode, result, '1);
			 out = tagged Valid tpl_1(y);
			 exc = exc | tpl_2(y);
       end
		 else if (temp2_bl) begin
			 result.exp = 0;
			 result.sfd = 0;
			 exc.underflow = True;
			 exc.inexact = True;
			 let y = round(rmode, result, 'b01);
			 out = tagged Valid tpl_1(y);
			 exc = exc | tpl_2(y);
       end
		 else if (temp3_bl) begin
			 result.exp = 0;
			 shift = cExtend(fromInteger(minexp(result)) - newexp);
       end
       else begin
			 result.exp = cExtend(newexp + fromInteger(bias(result)));
       end
		
       if (out matches tagged Invalid) begin
	  Bit#(TAdd#(2,56)) temp_r = cExtend(opA);
	  rg_r <= temp_r;
	  Bit#(TAdd#(1,28)) temp_d = cExtend(opB);
	  rg_d<= temp_d;
	  Bit#(TAdd#(2,56)) temp_bigd = cExtendLSB(temp_d);
	  rg_bigd <= temp_bigd;
       end
       rg_final_out <= out;
       rg_final_exc <= exc;
       rg_final_result <= result;
       rg_final_shift <= shift;
   endrule:rl_initial
   
   rule rl_loop(rg_en == 2 && rg_count <= fromInteger(valueOf(28)));
	
      rg_count <= rg_count + 1;
      let r = rg_r;
      let q = rg_q;
      let bigd = rg_bigd;
      if (r[57] == 0) begin
	 q = (q << 1) | 1;
	 r = (r << 1) - bigd;
      end
      else begin
	 q = (q << 1);
	 r = (r << 1) + bigd;
      end
      rg_q <= q;
      //rg_r <= pack(r);
		rg_r <= r;
	
   endrule:rl_loop
	
   rule rl_final1(!rg_en2 && rg_count > fromInteger(valueOf(28)));
	
      rg_en2 <= True;
      Bit#(29) rsfd = ?;
      let q1 = rg_q;
      let r1 = rg_r;
      let d1 = rg_d;
      let out = rg_final_out;
      let exc = rg_final_exc;
      let result = rg_final_result;
      let shift = rg_final_shift;
      let rmode = tpl_3(rg_operands);
      if (out matches tagged Invalid) begin
	 q1 = q1 + (-(~q1));
	 if (r1[57] == 1) begin
	    q1 = q1 - 1;
	    r1 = r1 + cExtendLSB(d1);
	 end
         Bit#(TAdd#(1,28)) qq = unpack(q1);
	 Bit#(TAdd#(1,28)) rr = cExtendLSB(r1);
	 let q = qq;
	 let p = rr;
	 if (shift < fromInteger(valueOf(29))) begin
	    Bit#(29) qdbits1 = extend(q);
	    Bit#(1) sfdlsb = |(pack(qdbits1 << (fromInteger(valueOf(29)) - shift)));
	    rsfd = cExtend(q >> shift);
	    rsfd[0] = rsfd[0] | sfdlsb;
	 end
	 else begin
	    Bit#(1) sfdlsb = |(pack(q));
	    rsfd = 0;
	    rsfd[0] = sfdlsb;
	 end
	 if (p != 0) begin
	    rsfd[0] = 1;
	 end
      end

      rg_final_result <= result;
      rg_final_rsfd <= rsfd;
      rg_final_exc <= exc;
      rg_final_out <= out;
   endrule:rl_final1
   
   rule rl_final2(rg_en2 && rg_count > fromInteger(valueOf(28)));
      rg_en2 <= False;
      rg_count <= 0;
      rg_en <= 0;
      let result = rg_final_result;
      let exc = rg_final_exc;
      let rmode = tpl_3(rg_operands);
      let out = rg_final_out;
      let rsfd = rg_final_rsfd;
      Bit#(2) guard = ?;
      if (result.exp == maxBound) begin
	  if (truncateLSB(rsfd) == 2'b00) begin
	     rsfd = rsfd << 1;
	     result.exp = result.exp - 1;
	  end
	  else begin
	    result.exp = maxBound - 1;
	    result.sfd = maxBound;
	    exc.overflow = True;
	    exc.inexact = True;
	    let y = round(rmode, result, '1);
	    out = tagged Valid tpl_1(y);
	    exc = exc | tpl_2(y);
	  end
       end
       if (out matches tagged Invalid) begin
	
	  match {.out_, .guard_, .exc_} = normalize(result, rsfd);
	  result = out_;
	  guard = guard_;
	  exc = exc | exc_;
       end
       if (out matches tagged Valid .x) begin
	  result = x;
       end
       else begin
	  match {.out_, .exc_} = round(rmode,result,guard);
	  result = out_;
	  exc = exc | exc_;
       end
       if(rg_count > fromInteger(valueOf(28)))
	  rg_result <= tuple3(1,canonicalize(result),exc);
	
   endrule:rl_final2
   //********************************************************************************************************************************************************
   method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) operands);
      rg_en <= 1;
      rg_operands <= operands;
      rg_en1 <= 1;
   endmethod
   method ReturnType#(8,23) receive(); 
      let x = ReturnType{valid:tpl_1(rg_result),value:tpl_2(rg_result),ex:tpl_3(rg_result)};
      return x;
   endmethod
endmodule


// Divider DP
(*synthesize*)
module mk_divider_dp(Ifc_divider_dp);

   Reg#(Tuple3#(Bit#(1),FloatingPoint#(11,52),Exception)) rg_result <- mkDReg(tuple3(unpack(0),unpack(0),unpack(0)));
   Reg#(Tuple3#(FloatingPoint#(11,52), FloatingPoint#(11,52), RoundMode)) rg_operands <- mkReg(tuple3(unpack(0),unpack(0),unpack(0)));
   Reg#(Bit#(TLog#(TAdd#(57,1)))) rg_count <-mkReg(0);
   Reg#(Bit#(TAdd#(1,57))) rg_q<- mkReg(0);
   Reg#(Bit#(TAdd#(1,57))) rg_d <- mkReg(0);
   Reg#(Bit#(TAdd#(2,114))) rg_r <-mkReg(0);
   Reg#(Bit#(TAdd#(2,114))) rg_bigd <- mkReg(0);
   Reg#(Bit#(2)) rg_en <- mkReg(0);
   Reg#(Maybe#(FloatingPoint#(11,52))) rg_final_out <- mkReg(tagged Invalid);
   Reg#(Exception) rg_final_exc <- mkReg(defaultValue);
   Reg#(FloatingPoint#(11,52)) rg_final_result <- mkReg(defaultValue);
   Reg#(Bit#(11)) rg_final_shift <- mkReg(0);
   Reg#(Bit#(2)) rg_en1 <- mkReg(0);
   Reg#(Bool) rg_en2 <- mkReg(False);
   Reg#(Bit#(53)) rg_sfdA <- mkReg(0);
   Reg#(Bit#(53)) rg_sfdB <- mkReg(0);
   Reg#(Bit#(13)) rg_newexp <- mkReg(0);
   Reg#(Bit#(114))  rg_opA <- mkReg(0);
   Reg#(Bit#(57))  rg_opB <- mkReg(0);
   Reg#(Bit#(TLog#(TAdd#(1, 53)))) rg_zerosA <- mkReg(0);
   Reg#(Bit#(TLog#(TAdd#(1, 53)))) rg_zerosB <- mkReg(0);
   Reg#(Bit#(58)) rg_final_rsfd <- mkReg(0);

   //******************************************************* Rules **************************************************************************
   rule rl_initial0(rg_en1 == 1);
      rg_en1 <= 2;
      match {.in1, .in2, .rmode} = rg_operands;
      Bit#(53) sfdA = {getHiddenBit(in1), in1.sfd};
      Bit#(53) sfdB = {getHiddenBit(in2), in2.sfd};
      // compute MSB 0s of sfd
      Bit#(53) inp = sfdA;
      Vector#(15, Bit#(1)) v1 = unpack(inp[52:38]);                        
      Bit#(6) result1=0;                                  
      Bool done1 = False;
      for( Integer p1 = 14; p1 >=0; p1 = p1 - 1) begin
         if ((v1[p1] == 0)&&(!done1))  
            result1 = result1+1 ;
         else
            done1 = True;
      end
      Bit#(6) z0 = (result1);
   
      Vector#(15, Bit#(1)) v2 = unpack(inp[37:23]);                       
      Bit#(6) result2=0;                                   
      Bool done2 = False;
      for( Integer p2 = 14; p2 >=0; p2 = p2 - 1) begin
         if ((v2[p2] == 0)&&(!done2))  
            result2 = result2+1 ;
         else
           done2 = True;
      end
      Bit#(6) z1 = result2;
   
      Vector#(15, Bit#(1)) v3 = unpack(inp[22:8]);                        
      Bit#(6) result3=0;                                   
      Bool done3 = False;
      for( Integer p3 = 14; p3 >=0; p3 = p3 - 1) begin
         if ((v3[p3] == 0)&&(!done3))  
            result3 = result3+1 ;
         else
            done3 = True;
      end
      Bit#(6) z2 = result3;

      Vector#(8, Bit#(1)) v4 = unpack(inp[7:0]);                        
      Bit#(6)  result4=0;                                   
      Bool done4 = False;
      for( Integer p4 = 7; p4 >=0; p4 = p4 - 1) begin
         if ((v4[p4] == 0)&&(!done4))  
            result4 = result4+1 ;
         else
            done4 = True;
      end
      Bit#(6)  z3 = result4;
      Bit#(6) zerosA= (|inp[52:38]==1)?z0:((|inp[37:23]==1)?(z1+15):((|inp[22:8]==1)?(z2+30):((|inp[7:0]==1)?(z3+45):53)));
  
      //compute MSB 0s of sfdB
      Bit#(53) inp1 = sfdB;         
      Vector#(15, Bit#(1)) v5 = unpack(inp1[52:38]);                        
      Bit#(6) result5=0;                                  
      Bool done5 = False;
      for( Integer p5 = 14; p5 >=0; p5 = p5 - 1) begin
         if ((v5[p5] == 0)&&(!done5))  
            result5 = result5+1 ;
         else
            done5 = True;
      end
      Bit#(6) z4 = (result5);
   
      Vector#(15, Bit#(1)) v6 = unpack(inp1[37:23]);                        
      Bit#(6) result6=0;                                   
      Bool done6 = False;
      for( Integer p6 = 14; p6 >=0; p6 = p6 - 1) begin
         if ((v6[p6] == 0)&&(!done6)) 
           result6 = result6+1 ;
         else
           done6 = True;
      end
      Bit#(6) z5 = result6;
   
      Vector#(15, Bit#(1)) v7 = unpack(inp1[22:8]);                        
      Bit#(6) result7=0;                                   
      Bool done7 = False;
      for( Integer p7 = 14; p7 >=0; p7 = p7 - 1) begin
         if ((v7[p7] == 0)&&(!done7))  
            result7 = result7+1 ;
         else
            done7 = True;
      end
      Bit#(6) z6 = result7;

      Vector#(8, Bit#(1)) v8 = unpack(inp1[7:0]);                        
      Bit#(6)  result8=0;                                   
      Bool done8 = False;
      for( Integer p8 = 7; p8 >=0; p8 = p8 - 1) begin
         if ((v8[p8] == 0)&&(!done8))  
            result8 = result8+1 ;
         else
            done8 = True;
      end
      Bit#(6)  z7 = result8;
      Bit#(6) zerosB= (|inp1[52:38]==1)?z4:((|inp1[37:23]==1)?(z5+15):((|inp1[22:8]==1)?(z6+30):((|inp1[7:0]==1)?(z7+45):53)));
      sfdA = sfdA << zerosA;
      sfdB = sfdB << zerosB;
      rg_zerosA <= zerosA;
      rg_zerosB <= zerosB;
      rg_sfdA <= sfdA;
      rg_sfdB <= sfdB;
		
   endrule:rl_initial0

   rule rl_initial1(rg_en1 == 2);
      rg_en1 <= 3;
      match {.in1, .in2, .rmode} = rg_operands;
      let zerosA = rg_zerosA;
      let zerosB = rg_zerosB;
      let sfdA = rg_sfdA;
      let sfdB = rg_sfdB;
      Bit#(13) exp1 = isSubNormal(in1) ? fromInteger(minexp(in1)) : signExtend(unpack(unbias(in1)));
      Bit#(13) exp2 = isSubNormal(in2) ? fromInteger(minexp(in2)) : signExtend(unpack(unbias(in2)));
      Bit#(13) newexp = (exp1 - zeroExtend(unpack(pack(zerosA)))) - (exp2 - zeroExtend(unpack(pack(zerosB))));
      Bit#(114) opA = zExtendLSB({ 1'b0, sfdA });
      Bit#(57) opB = zExtend({ sfdB, 4'b0000 });
      rg_opA <= opA;
      rg_opB <= opB;
      rg_newexp <= newexp;
		
   endrule:rl_initial1

   rule rl_initial(rg_en == 1 && rg_en1 == 3);
      rg_en <= 2;
      match {.in1, .in2, .rmode} = rg_operands;
      Maybe#(FloatingPoint#(11,52)) out = tagged Invalid;
      Exception exc = defaultValue;
      FloatingPoint#(11,52) result = defaultValue;
      Bit#(11) shift = 0;
      let sfdA = rg_sfdA;
      let sfdB = rg_sfdB;
      let newexp = rg_newexp;	
      let opA = rg_opA;
      let opB = rg_opB;
      result.sign = in1.sign != in2.sign;

		Bit#(13) temp1 = fromInteger(maxexp(in1)+1);
		Bool temp1_bl = (newexp[12] == 1) ? False : (newexp[11:0] > temp1[11:0]);
		Bit#(13) temp2 = fromInteger(minexp_subnormal(in1))-2;
		Bool temp2_bl = (newexp[12] == 0) ? False : (newexp[11:0] < temp2[11:0]);
		Bit#(13) temp3 = fromInteger(minexp(result));
		Bool temp3_bl = (newexp[12] == 0) ? False : (newexp[11:0] < temp3[11:0]);

      if (isSNaN(in1)) begin
         out = tagged Valid nanQuiet(in1);
	 exc.invalid_op = True;
      end
      else if (isSNaN(in2)) begin
	 out = tagged Valid nanQuiet(in2);
	 exc.invalid_op = True;
      end
      else if (isQNaN(in1)) begin
	 out = tagged Valid in1;
      end
      else if (isQNaN(in2)) begin
         out = tagged Valid in2;
      end
      else if ((isInfinity(in1) && isInfinity(in2)) || (isZero(in1) && isZero(in2))) begin
	 out = tagged Valid qnan();
	 exc.invalid_op = True;
      end
      else if (isZero(in2) && !isInfinity(in1)) begin
	 out = tagged Valid infinity(result.sign);
	 exc.divide_0 = True;
      end
      else if (isInfinity(in1)) begin
	 out = tagged Valid infinity(result.sign);
      end
      else if (isZero(in1) || isInfinity(in2)) begin
	 out = tagged Valid zero(result.sign);
      end
		else if (temp1_bl) begin
	 result.exp = maxBound - 1;
         result.sfd = maxBound;	
	 exc.overflow = True;
	 exc.inexact = True;
	 let y = round(rmode, result, '1);
	 out = tagged Valid tpl_1(y);
	 exc = exc | tpl_2(y);
      end
		else if (temp2_bl) begin
	 result.exp = 0;
	 result.sfd = 0;
	 exc.underflow = True;
	 exc.inexact = True;
	 let y = round(rmode, result, 'b01);
	 out = tagged Valid tpl_1(y);
	 exc = exc | tpl_2(y);
      end
		else if (temp3_bl) begin
         result.exp = 0;
	 shift = cExtend(fromInteger(minexp(result)) - newexp);
      end
      else begin
         result.exp = cExtend(newexp + fromInteger(bias(result)));
      end
		
      if (out matches tagged Invalid) begin
	 Bit#(TAdd#(2,114)) temp_r = cExtend(opA);
	 rg_r <= temp_r;
	 Bit#(TAdd#(1,57)) temp_d = cExtend(opB);
	 rg_d<= temp_d;
	 Bit#(TAdd#(2,114)) temp_bigd = cExtendLSB(temp_d);
	 rg_bigd <= temp_bigd;
      end
      rg_final_out <= out;
      rg_final_exc <= exc;
      rg_final_result <= result;
      rg_final_shift <= shift;
    endrule:rl_initial

    rule rl_loop(rg_en == 2 && rg_count <= fromInteger(valueOf(57)));
       rg_count <= rg_count + 1;
       let r = rg_r;
       let q = rg_q;
       let bigd = rg_bigd;
       if (r[115] == 0) begin
	  q = (q << 1) | 1;
	  r = (r << 1) - bigd;
       end
       else begin
	  q = (q << 1);
	  r = (r << 1) + bigd;
       end
       rg_q <= q;
       rg_r <= r;
	
   endrule:rl_loop
	
   rule rl_final1(!rg_en2 && rg_count > fromInteger(valueOf(57)));
	
      rg_en2 <= True;
      Bit#(58) rsfd = ?;
      let q1 = rg_q;
      let r1 = rg_r;
      let d1 = rg_d;
      let out = rg_final_out;
      let exc = rg_final_exc;
      let result = rg_final_result;
      let shift = rg_final_shift;
      let rmode = tpl_3(rg_operands);

      if (out matches tagged Invalid) begin
         q1 = q1 + (-(~q1));
	 if (r1[115] == 1) begin
	    q1 = q1 - 1;
	    r1 = r1 + cExtendLSB(d1);
	 end
         Bit#(TAdd#(1,57)) qq = unpack(pack(q1));
	 Bit#(TAdd#(1,57)) rr = cExtendLSB(r1);
	 let q = qq;
	 let p = rr;
	 if (shift < fromInteger(valueOf(58))) begin
	    Bit#(58) qdbits1 = extend(q);
	    Bit#(1) sfdlsb = |(pack(qdbits1 << (fromInteger(valueOf(58)) - shift)));
	    rsfd = cExtend(q >> shift);
	    rsfd[0] = rsfd[0] | sfdlsb;
	 end
	 else begin
	    Bit#(1) sfdlsb = |(pack(q));
	    rsfd = 0;
	    rsfd[0] = sfdlsb;
	 end
	 if (p != 0) begin
	    rsfd[0] = 1;
	 end
      end

      rg_final_result <= result;
      rg_final_rsfd <= rsfd;
      rg_final_exc <= exc;
      rg_final_out <= out;
   endrule:rl_final1

   rule rl_final2(rg_en2 && rg_count > fromInteger(valueOf(57)));
      rg_en2 <= False;
      rg_count <= 0;
      rg_en <= 0;
      let result = rg_final_result;
      let exc = rg_final_exc;
      let rmode = tpl_3(rg_operands);
      let out = rg_final_out;
      let rsfd = rg_final_rsfd;
      Bit#(2) guard = ?;
      if (result.exp == maxBound) begin
	 if (truncateLSB(rsfd) == 2'b00) begin
	    rsfd = rsfd << 1;
	    result.exp = result.exp - 1;
	 end
	 else begin
	    result.exp = maxBound - 1;
	    result.sfd = maxBound;
	    exc.overflow = True;
	    exc.inexact = True;
	    let y = round(rmode, result, '1);
	    out = tagged Valid tpl_1(y);
	    exc = exc | tpl_2(y);
	  end
       end
       if (out matches tagged Invalid) begin
	  match {.out_, .guard_, .exc_} = normalize(result, rsfd);
	  result = out_;
	  guard = guard_;
	  exc = exc | exc_;
       end
       if (out matches tagged Valid .x) begin
	  result = x;
       end
       else begin
	  match {.out_, .exc_} = round(rmode,result,guard);
	  result = out_;
	  exc = exc | exc_;
       end
       if(rg_count > fromInteger(valueOf(57)))
	  rg_result <= tuple3(1,canonicalize(result),exc);
	
   endrule:rl_final2
   //********************************************************************************************************************************************************
   method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) operands);
      rg_en <= 1;
      rg_operands <= operands;
      rg_en1 <= 1;
   endmethod
   method ReturnType#(11,52) receive(); 
      let x = ReturnType{valid:tpl_1(rg_result),value:tpl_2(rg_result),ex:tpl_3(rg_result)};
      return x;
    endmethod
endmodule
`endif
endpackage
