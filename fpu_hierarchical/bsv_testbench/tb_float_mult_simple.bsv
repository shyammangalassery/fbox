package tb_float_mult_simple;

  import fpu_multiplier_hierarchical :: * ;
  import fpu_common :: *;
  `include "../modules_fp/bsv_testbench/Logger.bsv"

  (*synthesize*)
  module mktb_float_mult_simple();
    Ifc_fpu_multiplier_sp ifc_mult <- mk_fpu_multiplier_sp();
    Reg#(Bit#(8)) rg_cycle <- mkReg(0);

    //sp_testcases
   
    rule rl_send0(rg_cycle==0); //sending first set of inputs in first cycle 
      FloatingPoint#(8, 23) f,g;
      RoundMode r1 = Rnd_Plus_Inf;

      f.sign = True;
      f.exp = 8'h01;
      f.sfd = 23'h000001;
      
      g.sign = False;
      g.exp = 8'h7e;
      g.sfd = 23'h7ffffd;


      ifc_mult.send(tuple3(f,g, r1));
      $display("cycle:",rg_cycle," ",fshow(f),fshow(g));

      //`logLevel( tb, 1, $format("TB sqrt: cycle %d: send operands ", rg_cycle, fshow(f)))
    endrule
    
    rule rl_send2(rg_cycle==2);   //sending first set of inputs in third cycle
      FloatingPoint#(8, 23) f,g;
      RoundMode r1 = Rnd_Plus_Inf;

      f.sign = True;
      f.exp = 8'h01;
      f.sfd = 23'h000001;
      
      g.sign = False;
      g.exp = 8'h7e;
      g.sfd = 23'h7ffffd;


      ifc_mult.send(tuple3(f,g, r1));
      $display("cycle:",rg_cycle," ",fshow(f),fshow(g));

    endrule
    
    rule rl_send3(rg_cycle==3); //sending first set of inputs in fourth cycle
      FloatingPoint#(8, 23) f,g;
      RoundMode r1 = Rnd_Plus_Inf;

      f.sign = True;
      f.exp = 8'h01;
      f.sfd = 23'h000001;
      
      g.sign = False;
      g.exp = 8'h7e;
      g.sfd = 23'h7ffffd;


      ifc_mult.send(tuple3(f,g, r1));
      $display("cycle:",rg_cycle," ",fshow(f),fshow(g));

      //`logLevel( tb, 1, $format("TB sqrt: cycle %d: send operands ", rg_cycle, fshow(f)))
    endrule
    //dp_testcases
    /*rule rl_send0(rg_cycle==0); //sending first set of inputs in first cycle 
      FloatingPoint#(11, 52) f,g;
      RoundMode r1 = Rnd_Nearest_Even;

      f.sign = True;
      f.exp = 11'h001;
      f.sfd = 52'hffffff7ffffff;
      
      g.sign = True;
      g.exp = 11'h3fe;
      g.sfd = 52'h0000007ffffff;


      ifc_mult.send(tuple3(f,g, r1));
      $display("cycle:",rg_cycle," ",fshow(f),fshow(g));

      //`logLevel( tb, 1, $format("TB sqrt: cycle %d: send operands ", rg_cycle, fshow(f)))
    endrule
    
    rule rl_send1(rg_cycle==1); //sending first set of inputs in first cycle 
      FloatingPoint#(11, 52) f,g;
      RoundMode r1 = Rnd_Nearest_Even;

      f.sign = True;
      f.exp = 11'h001;
      f.sfd = 52'hfffffefffffff;
      
      g.sign = False;
      g.exp = 11'h3fe;
      g.sfd = 52'h000000fffffff;


      ifc_mult.send(tuple3(f,g, r1));
      $display("cycle:",rg_cycle," ",fshow(f),fshow(g));

      //`logLevel( tb, 1, $format("TB sqrt: cycle %d: send operands ", rg_cycle, fshow(f)))
    endrule*/

    rule rl_receive;
      ReturnType#(8, 23) r;

      r = ifc_mult.receive();
      
      $display("cycle:",rg_cycle," ",fshow(r));


      //`logLevel( tb, 1, $format("TB sqrt: cycle %d: receive result ", rg_cycle, fshow(r)))
    endrule

    rule rl_end;
      rg_cycle <= rg_cycle + 1;  //incrementing the clock
      if (rg_cycle > 8) begin
        $finish(0);
      end
    endrule

  endmodule
endpackage
