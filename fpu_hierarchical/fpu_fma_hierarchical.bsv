////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
------------------------------------------------------------------------------------------------------------------------------------------

Author: Shalender Kumar, Sujay Pandit, Lokhesh Kumar, Charulatha Narashiman, Nitya Ranganathan
Email id: sk29108@gmail.com, contact.sujaypandit@gmail.com, lokhesh.kumar@gmail.com, charuswathi112@gmail.com, nitya.ranganathan@gmail.com
Details: Separate SP and DP FMA modules originally derived from Bluespec libraries (register retiming required)
-------------------------------------------------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
`include "Logger.bsv"
package fpu_fma_hierarchical;
import normalize_fma_hierarchical :: * ;
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import DReg  :: *;
`include "fpu_parameters.bsv"

`ifdef fpu_hierarchical

////////////////////////////////////////////////////////////////////////////////
/// Floating point fused multiple accumulate
////////////////////////////////////////////////////////////////////////////////

   //******************************************* SP *********************************************************
   interface Ifc_fpu_fma_sp;
      /*doc:method:send method for receiving inputs in sp. */
      method Action send(Tuple4#(
         FloatingPoint#(8,23),
         FloatingPoint#(8,23),
         Maybe#(FloatingPoint#(8,23)),
         RoundMode) operands);
      /*doc:method:receive method for returning result in sp. */
      method ReturnType#(8,23) receive();
   endinterface
   
   (*synthesize*)
   /*doc:module:this module implements send and receive method in sp*/
   module mk_fpu_fma_sp(Ifc_fpu_fma_sp);
      Vector#(`STAGES_FMA_SP,Reg#(Tuple2#(FloatingPoint#(8,23),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
      Vector#(`STAGES_FMA_SP,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      Wire#(Tuple4#(FloatingPoint#(8,23),
				FloatingPoint#(8,23),
				Maybe#(FloatingPoint#(8,23)),
				RoundMode)) wr_operands <- mkDWire(tuple4(unpack(0), unpack(0), unpack(0), unpack(0)));
      Wire#(Tuple2#(FloatingPoint#(8,23),Exception)) wr_returnvalues <- mkDWire(tuple2(unpack(0), unpack(0)));
      
      //for stage 2 -> stage 3 
      Reg#(Bool) rg_sgnAB <- mkReg(False);
      Reg#(Bit#(50)) rg_sfdAb <- mkReg(0);
      Reg#(Int#(10)) rg_expAB <- mkReg(0);
      Reg#(FloatingPoint#(8, 23)) rg_opC <- mkReg(0);
      Reg#(FloatingPoint#(8, 23)) rg_opA <- mkReg(0);
      Reg#(FloatingPoint#(8, 23)) rg_opB <- mkReg(0);
      Reg#(CommonState#(8,23)) rg_s <- mkReg(unpack(0));
      Reg#(FloatingPoint#(8,23)) rg_ab <- mkReg(0);
      Reg#(Bool) rg_acc <- mkReg(False);
      Reg#(Bool) rg_zeroC <- mkReg(False);
      Reg#(Bool) rg_bigEXP <- mkReg(False);

      //for stage 1 -> stage 2
      Reg#(Bit#(24)) rg_sfdA <- mkReg(0);
      Reg#(Bit#(24)) rg_sfdB <- mkReg(0);
      Reg#(CommonState#(8,23)) rg_s1 <- mkReg(unpack(0));
      Reg#(Int#(10)) rg_expAB1 <- mkReg(0);
      Reg#(Bool) rg_sgnAB1 <- mkReg(False);
      Reg#(FloatingPoint#(8, 23)) rg_opC1 <- mkReg(0);
      Reg#(FloatingPoint#(8, 23)) rg_opA1 <- mkReg(0);
      Reg#(FloatingPoint#(8, 23)) rg_opB1 <- mkReg(0);
      Reg#(Bool) rg_acc1 <- mkReg(False);
      Reg#(Bool) rg_zeroC1 <- mkReg(False);
 
      //for stage 3 -> stage 4
      Reg#(CommonState#(8,23)) rg_s2 <- mkReg(unpack(0));
      Reg#(FloatingPoint#(8, 23)) rg_out <- mkReg(0);
      Reg#(Bit#(3)) rg_guard <- mkReg(0);
      Reg#(Bool) rg_acc2 <- mkReg(False);
      Reg#(FloatingPoint#(8,23)) rg_ab1 <- mkReg(0);
      Reg#(FloatingPoint#(8, 23)) rg_opA2 <- mkReg(0);
      Reg#(FloatingPoint#(8, 23)) rg_opB2 <- mkReg(0);
      Reg#(FloatingPoint#(8, 23)) rg_opC2 <- mkReg(0);
      Reg#(Bool) rg_sub <- mkReg(False);
      Reg#(Int#(10)) rg_expAB2 <- mkReg(0);
 
      /* rules rl_fpu_fma_sp_stage1 & rl_fpu_fma_sp_stage2 together compute opA*opB */
      rule rl_fpu_fma_sp_stage1;
     
         match {.opA, .opB, .mopC, .rmode } =  wr_operands;
         CommonState#(8,23) s = CommonState {res: tagged Invalid, exc: defaultValue, rmode: rmode};
         Bool acc = False;
         FloatingPoint#(8,23) opC = 0;

         if (mopC matches tagged Valid .opC_) begin
	    opC = opC_;
	    acc = True;
         end

	 //zeroC indicates whether operand C is zero or not
	 Bool zeroC = (opC.exp == 0 && opC.sfd == 0);
	 //If number is subnormal, then exponent will become minimum exponent, it is -126 for sp and -1022 for dp
         //If number is not subnormal, then unbias it, bias is 127 for sp and 1023 for dp
         Int#(10) expA = isSubNormal(opA) ? fromInteger(minexp(opA)) : signExtend(unpack(unbias(opA)));
         Int#(10) expB = isSubNormal(opB) ? fromInteger(minexp(opB)) : signExtend(unpack(unbias(opB)));
      
         //significand is simply taken from sfd part of operand. Hidden bit is either 0 or 1, it is 0 for subnormal number and 1 for normal number. subnormal number have exponent as zero.
         Bit#(24) sfdA = { getHiddenBit(opA), opA.sfd };
         Bit#(24) sfdB = { getHiddenBit(opB), opB.sfd };

         //sgnAB maintains sign of product
         Bool sgnAB = opA.sign != opB.sign;
         //infAB indicates whether result of product is infinity or not
         Bool infAB = isInfinity(opA) || isInfinity(opB);
         //zeroAB indicates product is zero or not.
         Bool zeroAB = isZero(opA) || isZero(opB);

         // OpA*OpB - add exponents of both the operands and multiplies their mantissas 
         Int#(10) expAB = expA + expB;

	 if(((isZero(opA) && isInfinity(opB)) || (isZero(opB) && isInfinity(opA)) ) && isNaN(opC))
	    s.exc.invalid_op = True;

         //if opC is signaling NaN, its quiet NaN is the reult
         if (isSNaN(opC)) begin
	    s.res = tagged Valid nanQuiet(opC);
	    s.exc.invalid_op = True;
         end
         //if opA is signaling NaN, it's quiet NaN is the result 
         else if (isSNaN(opA)) begin
	    s.res = tagged Valid nanQuiet(opA);
	    s.exc.invalid_op = True;
         end
         //if opB is signaling NaN, it's Quiet NaN is the result
         else if (isSNaN(opB)) begin
	    s.res = tagged Valid nanQuiet(opB);
	    s.exc.invalid_op = True;
         end
         //When opA, opB or opC is quiet NaN, the invalid flag will not be set
         else if (isQNaN(opC)) begin
	    s.res = tagged Valid opC;
         end
         else if (isQNaN(opA)) begin
	    s.res = tagged Valid opA;
         end
         else if (isQNaN(opB)) begin
	    s.res = tagged Valid opB;
         end
         else if ((isInfinity(opA) && isZero(opB)) || (isZero(opA) && isInfinity(opB)) || (isInfinity(opC) && infAB && (opC.sign != sgnAB))) begin
	    s.res = tagged Valid qnan();
	    s.exc.invalid_op = True;
         end
         //if opC is infinity, it is final result
         else if (isInfinity(opC)) begin
	    s.res = tagged Valid opC;
         end
         //if opA or opB is infinity, the result is infinity
         else if (infAB) begin
	    s.res = tagged Valid infinity(sgnAB);
         end
         //if opC and product is zero, the result is also zero
         else if (isZero(opC) && zeroAB && (opC.sign == sgnAB)) begin
	    s.res = tagged Valid opC;
         end

         rg_sfdA <= sfdA;
         rg_sfdB <= sfdB;
         rg_s1 <= CommonState{res:s.res, exc:s.exc, rmode:s.rmode};
         rg_expAB1 <= expAB;
         rg_sgnAB1 <= sgnAB;
         rg_opC1 <= opC;
         rg_opA1 <= opA;
         rg_opB1 <= opB;
         rg_acc1 <= acc;
         rg_zeroC1 <= zeroC;
      endrule //stage1

      rule fn_fpu_fma_sp_stage2;

         //get the values from stage1
         Bit#(24) sfdA = rg_sfdA ;
         Bit#(24) sfdB = rg_sfdB ;
         CommonState#(8,23) s = rg_s1; 
         Int#(10) expAB = rg_expAB1;
         Bool sgnAB = rg_sgnAB1;
         FloatingPoint#(8, 23) opC = rg_opC1;
         FloatingPoint#(8, 23) opB = rg_opB1;
         FloatingPoint#(8, 23) opA = rg_opA1;
         Bool acc = rg_acc1;
         Bool zeroC = rg_zeroC1;

         //sfdAB holds the result of multiplication of significand of opA and significand of opB
         Bit#(48) sfdAB = primMul(sfdA, sfdB);
         Bit#(50) sfdAb = 0;		
         Bool bigEXP = False;	
         FloatingPoint#(8,23) ab = defaultValue;
         // s.res is invalid -> Not a special case  
         if (s.res matches tagged Invalid) begin
            //if product exponent is greater than max exponent(127 for sp and 1023 for dp), normalization need not be done
            //product exponent and product mantissa remains as it is
	    if (expAB > fromInteger(maxexp(ab))) begin
	       ab.sign = sgnAB;
	       ab.exp = maxBound - 1;
	       ab.sfd = maxBound;
	       sfdAb = {sfdAB,2'b00};
	    end
	    //checking for minimum possible exponent, checks if the exponent cannot be in a valid range after a shift   
	    else if (expAB < (fromInteger(minexp_subnormal(ab))-2)) begin
	       ab.sign = sgnAB;
	       ab.exp = 0;
	       ab.sfd = 0;
	       sfdAb = {sfdAB,2'b00};
	       if (|sfdAB == 1) begin
	          bigEXP = True;
	       end 
	    end
	    else begin
	       //go for normalization
	       let shift = fromInteger(minexp(ab)) - expAB;
	       //shift > 0 indicates expAB is smaller than minimum exponent. If true, set expAB as minimum possible exponent.
	       if (shift > 0) begin
                   //subnormal
                  `ifdef denormal_support
	              Bit#(1) sfdlsb = |(sfdAB << (fromInteger(valueOf(48)) - shift));
                      sfdAB = sfdAB >> shift;
	              sfdAB[0] = sfdAB[0] | sfdlsb;
	              expAB = 'd-126;
                  `else
	              ab.sfd = 0;
	              sfdAB = 0;
                  `endif
	          ab.exp = 0;
	       end
	       //simply add bias in product exponent 
	       else begin
	          ab.exp = cExtend(expAB + fromInteger(bias(ab)));
	       end
	       ab.sign = sgnAB;
               if(|sfdAB == 0)
	          expAB = 'd-127;
	       //do normalization
               let y <- normalize1_sp(tuple2(ab, sfdAB));
	       //normalization1_sp function gives exception, normalized result. 
               ab = tpl_1(y);
               s.exc = s.exc | tpl_2(y);
	       sfdAb = tpl_3(y);
	       expAB = isSubNormal(ab) ? fromInteger(minexp(ab)) : signExtend(unpack(unbias(ab)));
            end
         end

         rg_sgnAB <= sgnAB;
         rg_sfdAb <= sfdAb;
         rg_expAB <= expAB;
         rg_opC <= opC;
         rg_opA <= opA;
         rg_opB <= opB;
         rg_s <= CommonState{res:s.res, exc:s.exc, rmode:s.rmode};
         rg_ab <= ab;
         rg_acc <= acc;
         rg_zeroC <= zeroC;
         rg_bigEXP <= bigEXP;
      endrule //stage2

      /* rules rl_fpu_fma_sp_stage3 & rl_fpu_fma_sp_stage4 together perform the addition */
      rule rl_fpu_fma_sp_stage3;

         //get values from stage2
         Bool sgnAB = rg_sgnAB;
         Bit#(50) sfdAb = rg_sfdAb;
         Int#(10) expAB = rg_expAB;
         FloatingPoint#(8, 23) opC = rg_opC ;
         FloatingPoint#(8, 23) opA = rg_opA ;
         FloatingPoint#(8, 23) opB = rg_opB ;
         CommonState#(8,23) s = rg_s; 
         FloatingPoint#(8, 23) ab = rg_ab ;
         Bool acc = rg_acc;
         Bool zeroC = rg_zeroC;
         Bool bigEXP = rg_bigEXP;

         //compute expC similar to expA,expB
         Int#(10) expC = isSubNormal(opC) ? fromInteger(minexp(opC)) : signExtend(unpack(unbias(opC)));
         `ifdef denormal_support
            opC.sfd = opC.sfd;
         `else
            if (isSubNormal(opC))
               opC.sfd = 0;
         `endif
  
         //compute the significand of opC
         Bit#(50) sfdC = {1'b0,getHiddenBit(opC), opC.sfd, 25'b0}; 
         Bool sub = opC.sign != ab.sign; //opC should be added or subtracted
         Int#(10) exp = ?;
         Int#(10) shift = ?;
         Bit#(50) x  = ?;
         Bit#(50) y  = ?;	
         Bool sgn = ?;
         FloatingPoint#(8,23) out = defaultValue;

         //Smaller operand among opAB and opC will be y and larger will be x
         //later x+y and x-y is performed for final result 
         if ((!acc) || (expAB > expC) || ((expAB == expC) && (sfdAb > sfdC))) begin
            exp = expAB;
	    shift = expAB - expC;
	    x = sfdAb;
	    y = sfdC;
	    sgn = ab.sign;
         end
         else begin
	    exp = expC;
	    shift = expC - expAB;
	    x = sfdC;
	    y = sfdAb;
	    sgn = opC.sign;
         end		
      
         //y is shifted to make exponent of opAB and opC equal
         if (s.res matches tagged Invalid) begin
            if (shift < fromInteger(50)) begin
	       Bit#(50) guard = 0;
	       guard = y << ((50) - shift);
	       y = y >> shift;
	       y[0] = y[0] | (|guard);
	    end
	    else if (|y == 1) begin
	       y = 1;
	    end
         end

         let sum = x + y;
         let diff = x - y;
         //Checking for overflow while addition - extra carry generated implies overflow, inexact and overflow flag are set
         if((x[49] == 1 || y[49] == 1) && sum[49] == 0 && !sub) begin
            s.exc.overflow = True;
	    s.exc.inexact = True;
         end
		
         Bit#(3) guard = 0;
         if (s.res matches tagged Invalid) begin
            Bit#(50) sfd;
	    //final significand is either sum or difference, it is decided by value of sub 
	    sfd = sub ? diff : sum;
	    out.sign = sgn;
	    //bias added in exponent
	    out.exp = cExtend(exp + fromInteger(bias(out)));
            //if opC is zero, final result is product of opA and opB
	    if(zeroC) begin
	       out = ab;
	    end
            // normalization after add/sub phase 
	    let y <- normalize2_sp(tuple4(exp,out, sfd,s.rmode));
            //this normalzation function gives result, exception flags and guard bit. guard bits are used for rounding. 		
	    out = tpl_1(y);
	    guard = tpl_2(y);
	    s.exc = s.exc | tpl_3(y);
	    //prevents by setting underflow flag during product phase. 
	    if(bigEXP)
               s.exc.underflow = False;
         end

         rg_s2 <= CommonState{res:s.res, exc:s.exc, rmode:s.rmode};
         rg_out <= out;
         rg_guard <= guard;
         rg_ab1 <= ab;
         rg_acc2 <= acc;
         rg_opA2 <= opA;
         rg_opB2 <= opB;
         rg_opC2 <= opC;
         rg_sub <= sub; 
         rg_expAB2 <= expAB;
      endrule //stage3

      rule rl_fpu_fma_sp_stage4;

         //get values from stage3
         CommonState#(8,23) s = rg_s2; 
         FloatingPoint#(8, 23) out = rg_out;
         Bit#(3) guard = rg_guard;
         FloatingPoint#(8, 23) ab = rg_ab1 ;
         Bool acc = rg_acc2;
         FloatingPoint#(8, 23) opA = rg_opA2;
         FloatingPoint#(8, 23) opB = rg_opB2;
         FloatingPoint#(8, 23) opC = rg_opC2;
         Bool sub = rg_sub;
         Int#(10) expAB = rg_expAB2;

         if (s.res matches tagged Valid .x) begin
            out = x;
         end
         else begin
            let y1 = out;
	    //rounding done based on rounding mode and guard bit
	    let y = round2(s.rmode, out, guard);
	    out = tpl_1(y);
	    s.exc = s.exc | tpl_2(y);
            if (s.exc.overflow == True) begin  // since we do not send the previously set flags to round2()
              s.exc.underflow = False;
            end

	    //set underflow for zero exponent
	    if(out.exp == 0 && s.exc.inexact && !s.exc.overflow)
	       s.exc.underflow = True;
	    // adjust sign for exact zero result
	    if (acc && isZero(out) && !s.exc.inexact && sub) begin
	       out.sign = (s.rmode == Rnd_Minus_Inf);
	    end
	    //in round_nearest_even and maxMax rounding mode and overflow, set final result to +inf.
	    if(s.exc.overflow == True && (s.rmode == Rnd_Nearest_Even ||s.rmode == Rnd_Nearest_Away_Zero)) begin
	       s.exc.underflow = False;
	       out.exp = '1;
	       out.sfd = '0;
	    end	
	    //in round_plus_inf and overflow, set the result to infinity in case of positive result and largest negative number in case of negative result
	    else if(s.exc.overflow == True && s.rmode == Rnd_Plus_Inf)begin
	       if(ab.sign)	begin
	          out.exp = 'b11111110;
	          out.sfd = '1;	
	       end
	       else begin
	          out.exp = '1;
	          out.sfd = '0;	
	       end
	    end
	    //in round_minus_inf and overflow, set the result to minus infinity in case of negative result and largest positive number in case of positive result
	    else if(s.exc.overflow == True && s.rmode == Rnd_Minus_Inf) begin
	       if(ab.sign) begin
	          out.exp = '1;
                  out.sfd = '0;	
	       end
	       else begin
	          out.exp = 'b11111110;
	          out.sfd = '1;	
	       end
            end
	    //in round_zero rounding mode and overflow, set result to the maximum normal number
	    else if(s.exc.overflow == True && s.rmode == Rnd_Zero) begin
	       out.exp = 'b11111110;
	       out.sfd = '1;	
	    end
	    //if opA, opB and opC are zero, set result to zero 
	    else if( (isZero(opA)||isZero(opB)) && isZero(opC)) begin
               out.exp = 0;
	       out.sfd = 0;
	    end
	    //If exponent less than -126, we cannot represent it, set it to zero. But round up rounding mode, set it to least number in case of positive result
	    else if(isZero(opC) && expAB < -126 && !ab.sign  && s.rmode == Rnd_Plus_Inf) begin
	       out.exp = 0;
	       out.sfd = 1;
	       s.exc.underflow = True;
	    end
 	    else if(isZero(opC) && expAB < -126 && ab.sign && s.rmode == Rnd_Minus_Inf) begin
	       out.exp = 0;
	       out.sfd = 1;
	       s.exc.underflow = True;
	       s.exc.inexact = True;
	    end
            else if(isZero(opC) && expAB < -126) begin
	       out.exp = 0;
	       out.sfd = 0;
	       s.exc.underflow = True;
	       s.exc.inexact = True;
       	    end
         end
      
         wr_returnvalues <= tuple2(canonicalize(out),s.exc);
      endrule //stage 4
       
      rule rl_receive;
         let x = wr_returnvalues;
         rg_stage_out[0] <= x;
         rg_stage_valid[0] <= 1;
      endrule    

      /*doc:rule:rule for implementing pipelining. */
      rule rl_pipeline;         
         for(Integer i = 1 ; i <= `STAGES_FMA_SP -4 ; i = i+1)begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule
      
      method Action send(Tuple4#(FloatingPoint#(8,23),FloatingPoint#(8,23),Maybe#(FloatingPoint#(8,23)),RoundMode) operands);
         wr_operands <= operands;       
      endmethod
      
      method ReturnType#(8,23) receive();
         return ReturnType{valid:rg_stage_valid[`STAGES_FMA_SP-4],value:tpl_1(rg_stage_out[`STAGES_FMA_SP-4]) ,ex:tpl_2(rg_stage_out[`STAGES_FMA_SP-4])};
      endmethod 
   
   endmodule

   //******************************************* DP *********************************************************
   interface Ifc_fpu_fma_dp;
      /*doc:method:send method for receiving inputs in dp. */
      method Action send(Tuple4#(
         FloatingPoint#(11,52),
         FloatingPoint#(11,52),
         Maybe#(FloatingPoint#(11,52)),
         RoundMode) operands);
      /*doc:method:receive method for returning result in dp. */
      method ReturnType#(11,52) receive();
   endinterface

   (*synthesize*)
   /*doc:module:this module implements send and receive method in dp*/
   module mk_fpu_fma_dp(Ifc_fpu_fma_dp);
      Vector#(`STAGES_FMA_DP,Reg#(Tuple2#(FloatingPoint#(11,52),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
      Vector#(`STAGES_FMA_DP,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      Wire#(Tuple4#(FloatingPoint#(11,52),
				FloatingPoint#(11,52),
				Maybe#(FloatingPoint#(11,52)),
				RoundMode)) wr_operands <- mkDWire(tuple4(unpack(0), unpack(0), unpack(0), unpack(0)));
      Wire#(Tuple2#(FloatingPoint#(11,52),Exception)) wr_returnvalues <- mkDWire(tuple2(unpack(0), unpack(0)));

      // for stage 2 -> stage 3
      Reg#(Bool) rg_sgnAB <- mkReg(False);
      Reg#(Bit#(108)) rg_sfdAb <- mkReg(0);
      Reg#(Int#(13)) rg_expAB <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opC <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opA <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opB <- mkReg(0);
      Reg#(CommonState#(11,52)) rg_s <- mkReg(unpack(0));
      Reg#(FloatingPoint#(11,52)) rg_ab <- mkReg(0);
      Reg#(Bool) rg_acc <- mkReg(False);
      Reg#(Bool) rg_zeroC <- mkReg(False);
      Reg#(Bool) rg_bigEXP <- mkReg(False);

      //stage3 -> stage4
      Reg#(Bool) rg_sgnAB32 <- mkReg(False);
      Reg#(Bit#(108)) rg_sfdAb2 <- mkReg(0);
      Reg#(Int#(13)) rg_expAB2 <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opC2 <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opA2 <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opB2 <- mkReg(0);
      Reg#(CommonState#(11,52)) rg_s2 <- mkReg(unpack(0));
      Reg#(FloatingPoint#(11,52)) rg_ab2 <- mkReg(0);
      Reg#(Bool) rg_acc2 <- mkReg(False);
      Reg#(Bool) rg_zeroC2 <- mkReg(False);
      Reg#(Bool) rg_bigEXP2 <- mkReg(False);
      Reg#(Bool) rg_sgn2 <- mkReg(False);
      Reg#(Bool) rg_sub2 <- mkReg(False);
      Reg#(Bit#(108)) rg_sum2 <- mkReg(0);
      Reg#(Bit#(108)) rg_diff2 <- mkReg(0);
      Reg#(Int#(13)) rg_exp2 <- mkReg(0);
      

      // for stage 1 -> stage 2
      Reg#(Bit#(53)) rg_sfdA <- mkReg(0);
      Reg#(Bit#(53)) rg_sfdB <- mkReg(0);
      Reg#(CommonState#(11,52)) rg_s1 <- mkReg(unpack(0));
      Reg#(Int#(13)) rg_expAB1 <- mkReg(0);
      Reg#(Bool) rg_sgnAB1 <- mkReg(False);
      Reg#(FloatingPoint#(11, 52)) rg_opC1 <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opA1 <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opB1 <- mkReg(0);
      Reg#(Bool) rg_acc1 <- mkReg(False);
      Reg#(Bool) rg_zeroC1 <- mkReg(False);
      
      // for stage 4 -> stage 5
      Reg#(CommonState#(11,52)) rg_s3 <- mkReg(unpack(0));
      Reg#(FloatingPoint#(11, 52)) rg_out <- mkReg(0);
      Reg#(Bit#(3)) rg_guard <- mkReg(0);
      Reg#(Bool) rg_acc3 <- mkReg(False);
      Reg#(FloatingPoint#(11 ,52)) rg_ab3 <- mkReg(0);
      Reg#(FloatingPoint#(11, 52 )) rg_opA3 <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opB3 <- mkReg(0);
      Reg#(FloatingPoint#(11, 52)) rg_opC3 <- mkReg(0);
      Reg#(Bool) rg_sub <- mkReg(False);
      Reg#(Int#(13)) rg_expAB3 <- mkReg(0);

      rule rl_fpu_fma_dp_stage1;

         match {.opA, .opB, .mopC, .rmode } = wr_operands;
         CommonState#(11,52) s = CommonState {res: tagged Invalid,exc: defaultValue, rmode: rmode };
         Bool acc = False;
         FloatingPoint#(11,52) opC = 0;
         if (mopC matches tagged Valid .opC_) begin
	    opC = opC_;
	    acc = True;
         end
	 `ifdef verbose `logLevel( tb, 0, $format("opA %h, opB %h, opC %h",opA, opB, opC)) `endif

	 Bool zeroC = (opC.exp == 0 && opC.sfd == 0);
         Int#(13) expA = isSubNormal(opA) ? fromInteger(minexp(opA)) : signExtend(unpack(unbias(opA)));
         Int#(13) expB = isSubNormal(opB) ? fromInteger(minexp(opB)) : signExtend(unpack(unbias(opB)));   
         Bit#(53) sfdA = { getHiddenBit(opA), opA.sfd };
         Bit#(53) sfdB = { getHiddenBit(opB), opB.sfd };
         `ifdef verbose `logLevel( tb, 0, $format("expA %h %d",expA,expA)) `endif
         `ifdef verbose `logLevel( tb, 0, $format("expB %h %d",expB,expB)) `endif
         `ifdef verbose `logLevel( tb, 0, $format("sfdA %h",sfdA)) `endif
         `ifdef verbose `logLevel( tb, 0, $format("sfdB %h",sfdB)) `endif
         Bool sgnAB = opA.sign != opB.sign;
         Bool infAB = isInfinity(opA) || isInfinity(opB);
         Bool zeroAB = isZero(opA) || isZero(opB);
   
         Int#(13) expAB = expA + expB;
         `ifdef verbose `logLevel( tb, 0, $format("expAB %h %d",expAB,expAB)) `endif

	 if(((isZero(opA) && isInfinity(opB)) || (isZero(opB) && isInfinity(opA)) ) && isNaN(opC))
            s.exc.invalid_op = True;
         if (isSNaN(opC)) begin
	    s.res = tagged Valid nanQuiet(opC);
	    s.exc.invalid_op = True;
         end
         else if (isSNaN(opA)) begin
	    s.res = tagged Valid nanQuiet(opA);
	    s.exc.invalid_op = True;
         end
         else if (isSNaN(opB)) begin
	    s.res = tagged Valid nanQuiet(opB);
	    s.exc.invalid_op = True;
         end
         else if (isQNaN(opC)) begin
	    s.res = tagged Valid opC;
         end
         else if (isQNaN(opA)) begin
	    s.res = tagged Valid opA;
         end
         else if (isQNaN(opB)) begin
	    s.res = tagged Valid opB;
         end
         else if ((isInfinity(opA) && isZero(opB)) || (isZero(opA) && isInfinity(opB)) || (isInfinity(opC) && infAB && (opC.sign != sgnAB))) begin
	    // product of zero and infinity or addition of opposite sign infinity
	    s.res = tagged Valid qnan();
	    s.exc.invalid_op = True;
         end
         else if (isInfinity(opC)) begin
	    s.res = tagged Valid opC;
         end
         else if (infAB) begin
	    s.res = tagged Valid infinity(sgnAB);
         end
         else if (isZero(opC) && zeroAB && (opC.sign == sgnAB)) begin
	    s.res = tagged Valid opC;
         end
 
         rg_sfdA <= sfdA;
         rg_sfdB <= sfdB;
         rg_s1 <= CommonState{res:s.res, exc:s.exc, rmode:s.rmode};
         rg_expAB1 <= expAB;
         rg_sgnAB1 <= sgnAB;
         rg_opC1 <= opC;
         rg_opA1 <= opA;
         rg_opB1 <= opB;
         rg_acc1 <= acc;
         rg_zeroC1 <= zeroC;
      endrule //stage1

      rule rl_fpu_fma_dp_stage2;
 
         //get values from stage1
         Bit#(53) sfdA = rg_sfdA ;
         Bit#(53) sfdB = rg_sfdB ;
         CommonState#(11,52) s = rg_s1; 
         Int#(13) expAB = rg_expAB1;
         Bool sgnAB = rg_sgnAB1;
         FloatingPoint#(11, 52) opC = rg_opC1;
         FloatingPoint#(11, 52) opB = rg_opB1;
         FloatingPoint#(11, 52) opA = rg_opA1;
         Bool acc = rg_acc1;
         Bool zeroC = rg_zeroC1;
         Bool bigEXP = False; 
         Bit#(106) sfdAB = primMul(sfdA, sfdB);
         Bit#(108) sfdAb = 0;
         `ifdef verbose `logLevel( tb, 0, $format("sfdAB %h",sfdAB)) `endif
         // normalize multiplication result
         FloatingPoint#(11,52) ab = defaultValue;
	 if (s.res matches tagged Invalid) begin

	    if (expAB > fromInteger(maxexp(ab))) begin
	       ab.sign = sgnAB;
	       ab.exp = maxBound - 1;
	       ab.sfd = maxBound;
               sfdAb = {sfdAB,2'b00};
	    end
	    else if (expAB < (fromInteger(minexp_subnormal(ab))-2)) begin
	       ab.sign = sgnAB;
	       ab.exp = 0;
	       ab.sfd = 0;
	       sfdAb = {sfdAB,2'b00};
	       if (|sfdAB == 1) begin
	          bigEXP = True;
               end
	    end
	    else begin
	       let shift = fromInteger(minexp(ab)) - expAB;
	       if (shift > 0) begin
                   // subnormal
                  `ifdef denormal_support
		     Bit#(1) sfdlsb = |(sfdAB << (fromInteger(valueOf(106)) - shift));
                     sfdAB = sfdAB >> shift;
		     sfdAB[0] = sfdAB[0] | sfdlsb;
		     expAB = 'd-1022;
                  `else
		     ab.sfd = 0;
		     sfdAB = 0;
                  `endif
	          ab.exp = 0;
	       end
	       else begin
	          ab.exp = cExtend(expAB + fromInteger(bias(ab)));
	       end
	       ab.sign = sgnAB;
	       if(|sfdAB == 0)
	          expAB = 'd-1023;
	       let y <- normalize1_dp(tuple2(ab, sfdAB));
	       ab = tpl_1(y);
	       s.exc = s.exc | tpl_2(y);
               sfdAb = tpl_3(y);
	       expAB = isSubNormal(ab) ? fromInteger(minexp(ab)) : signExtend(unpack(unbias(ab)));
	    end
         end
 
         rg_sgnAB <= sgnAB;
         rg_sfdAb <= sfdAb;
         rg_expAB <= expAB;
         rg_opC <= opC;
         rg_opA <= opA;
         rg_opB <= opB;
         rg_s <= CommonState{res:s.res, exc:s.exc, rmode:s.rmode};
         rg_ab <= ab;
         rg_acc <= acc;
         rg_zeroC <= zeroC;
         rg_bigEXP <= bigEXP;
      endrule //stage2

      
      rule rl_fpu_fma_dp_stage3;
 
         //get values from stage2
         Bool sgnAB = rg_sgnAB;
         Bit#(108) sfdAb = rg_sfdAb;
         Int#(13) expAB = rg_expAB;
         FloatingPoint#(11, 52) opC = rg_opC ;
         FloatingPoint#(11, 52) opA = rg_opA ;
         FloatingPoint#(11, 52) opB = rg_opB ;
         CommonState#(11,52) s = rg_s; 
         FloatingPoint#(11, 52) ab = rg_ab ;
         Bool acc = rg_acc;
         Bool zeroC = rg_zeroC;
         Bool bigEXP = rg_bigEXP;
         Int#(13) expC = isSubNormal(opC) ? fromInteger(minexp(opC)) : signExtend(unpack(unbias(opC)));
         
         `ifdef denormal_support
            opC.sfd = opC.sfd;
         `else
            if (isSubNormal(opC))
               opC.sfd = 0;
         `endif	
         Bit#(108) sfdC = {1'b0,getHiddenBit(opC), opC.sfd, 54'b0};
         Bool sub = opC.sign != ab.sign;
	 Int#(13) exp = ?;
 	 Int#(13) shift = ?;
	 Bit#(108) x  = ?;
	 Bit#(108) y  = ?;
	 Bool sgn = ?;
	 if ((!acc) || (expAB > expC) || ((expAB == expC) && (sfdAb > sfdC))) begin
	    exp = expAB;
	    shift = expAB - expC;
	    x = sfdAb;
	    y = sfdC;
	    sgn = ab.sign;
	 end
	 else  begin
	    exp = expC;
	    shift = expC - expAB;
	    x = sfdC;
	    y = sfdAb;
	    sgn = opC.sign;
         end		
	 
         if (s.res matches tagged Invalid) begin
	    if (shift < fromInteger(108)) begin
	       Bit#(108) shifted_value = 0;
               shifted_value = y << ((108) - shift);
	       y = y >> shift;
	       y[0] = y[0] | (|shifted_value);
	    end
	    else if (|y == 1) begin
	       y = 1;
	    end
	 end
	 `ifdef verbose `logLevel( tb, 0, $format("x %h",x)) `endif
	 `ifdef verbose `logLevel( tb, 0, $format("y %h",y)) `endif
	 `ifdef verbose `logLevel( tb, 0, $format("expC %h %d",expC,expC)) `endif
    `ifdef verbose `logLevel( tb, 0, $format("expAB %h %d",expAB,expAB)) `endif
    `ifdef verbose `logLevel( tb, 0, $format("exp %h %d",exp,exp)) `endif
 	 let sum = x + y;
	 let diff = x - y;
	 if((x[107] == 1 || y[107] == 1) && sum[107] == 0 && !sub) begin
	    s.exc.overflow = True;
	    s.exc.inexact = True;
	 end
         rg_s2 <= CommonState{res:s.res, exc:s.exc, rmode:s.rmode};
         rg_ab2 <= ab;
         rg_acc2 <= acc;
         rg_opA2 <= opA;
         rg_opB2 <= opB;
         rg_opC2 <= opC;
         rg_sub2 <= sub; 
         rg_expAB2 <= expAB;
         rg_sum2 <= sum;
         rg_diff2 <= diff;
         rg_exp2 <= exp;
         rg_zeroC2 <= zeroC;
         rg_sgn2 <= sgn;

    endrule

    rule rl_fpu_fma_dp_stage4;
       Bit#(3) guard = 0;
	    FloatingPoint#(11,52) out = defaultValue;
       Int#(13) expAB = rg_expAB2;
       Int#(13) exp = rg_exp2;
       FloatingPoint#(11, 52) opC = rg_opC2 ;
       FloatingPoint#(11, 52) opA = rg_opA2 ;
       FloatingPoint#(11, 52) opB = rg_opB2 ;
       CommonState#(11,52) s = rg_s2; 
       FloatingPoint#(11, 52) ab = rg_ab2 ;
       Bool acc = rg_acc2;
       Bool zeroC = rg_zeroC2;
       Bool sgn = rg_sgn2;
       Bool bigEXP = rg_bigEXP2;
       Bool sub = rg_sub2;
       Bit#(108) sum = rg_sum2;
       Bit#(108) diff = rg_diff2;
	    if (s.res matches tagged Invalid) begin
         Bit#(108) sfd;
	      sfd = sub ? diff : sum;
	      out.sign = sgn;
	      out.exp = cExtend(exp + fromInteger(bias(out)));
	      if(zeroC) begin
	        out = ab;
	      end
         let y <- normalize2_dp(tuple4(exp,out, sfd,s.rmode));	//normalize   2
	      out = tpl_1(y);
	      `ifdef verbose `logLevel( tb, 0, $format("out %h ", out)) `endif
	      guard = tpl_2(y);
	      s.exc = s.exc | tpl_3(y);
	      if(bigEXP)
            s.exc.underflow = False;
      end

      rg_s3 <= CommonState{res:s.res, exc:s.exc, rmode:s.rmode};
      rg_out <= out;
      rg_guard <= guard;
      rg_ab3 <= ab;
      rg_acc3 <= acc;
      rg_opA3 <= opA;
      rg_opB3 <= opB;
      rg_opC3 <= opC;
      rg_sub <= sub; 
      rg_expAB3 <= expAB;
   endrule //stage3
 
      rule rl_fpu_fma_dp_stage5;
     
         //get values from stage3
         CommonState#(11,52) s = rg_s3; 
         FloatingPoint#(11, 52) out = rg_out;
         Bit#(3) guard = rg_guard;
         FloatingPoint#(11, 52) ab = rg_ab3 ;
         Bool acc = rg_acc3;
         FloatingPoint#(11, 52) opA = rg_opA3;
         FloatingPoint#(11, 52) opB = rg_opB3;
         FloatingPoint#(11, 52) opC = rg_opC3;
         Bool sub = rg_sub;
         Int#(13) expAB = rg_expAB3;

	 if (s.res matches tagged Valid .x) begin
	    out = x;
	 end
	 else  begin
	    let y1 = out;
	    let y = round2(s.rmode, out, guard);
	    out = tpl_1(y);
	    `ifdef verbose `logLevel( tb, 0, $format("out %h ",out)) `endif
	    s.exc = s.exc | tpl_2(y);
            if (s.exc.overflow == True) begin  // since we do not send the previously set flags to round2()
              s.exc.underflow = False;
            end

	    if(out.exp == 0 && s.exc.inexact && !s.exc.overflow)
               s.exc.underflow = True;
	    // adjust sign for exact zero result
	    if (acc && isZero(out) && !s.exc.inexact && sub) begin
	       out.sign = (s.rmode == Rnd_Minus_Inf);
	    end
	    if(s.exc.overflow == True && (s.rmode == Rnd_Nearest_Even ||s.rmode == Rnd_Nearest_Away_Zero)) begin
	       s.exc.underflow = False;
	       out.exp = '1;
	       out.sfd = '0;
	    end
	    else if(s.exc.overflow == True && s.rmode == Rnd_Plus_Inf) begin
	       if(ab.sign) begin
	          out.exp = 'b11111111110;
		  out.sfd = '1;	
	       end
	       else begin
	          out.exp = '1;
		  out.sfd = '0;	
	       end
	    end
	    else if(s.exc.overflow == True && s.rmode == Rnd_Minus_Inf) begin
	       if(ab.sign) begin
	          out.exp = '1;
		  out.sfd = '0;	
	       end
	       else begin
		  out.exp = 'b11111111110;
		  out.sfd = '1;	
	       end
	    end
	    else if(s.exc.overflow == True && s.rmode == Rnd_Zero) begin
	       out.exp = 'b11111111110;
	       out.sfd = '1;	
	    end
	    else if( (isZero(opA)||isZero(opB)) && isZero(opC)) begin
	       out.exp = 0;
	       out.sfd = 0;
	    end
	    else if(isZero(opC) && expAB < -1022 && !ab.sign  && s.rmode == Rnd_Plus_Inf) begin
	       out.exp = 0;
	       out.sfd = 1;
	       s.exc.underflow = True;
	    end
	    else if(isZero(opC) && expAB < -1022 && ab.sign && s.rmode == Rnd_Minus_Inf) begin
	       out.exp = 0;
	       out.sfd = 1;
	       s.exc.underflow = True;
	       s.exc.inexact = True;
	    end
	    else if(isZero(opC) && expAB < -1022) begin
	       out.exp = 0;
	       out.sfd = 0;
	       s.exc.underflow = True;
	       s.exc.inexact = True;
	    end
	 end
	 `ifdef verbose `logLevel( tb, 0, $format("out %h ",out)) `endif
         
         wr_returnvalues <= tuple2(canonicalize(out),s.exc);
      endrule  //stage4

      rule rl_receive;
         let x = wr_returnvalues;
         rg_stage_out[0] <= x;
         rg_stage_valid[0] <= 1;
      endrule    
       	
      /*doc:rule:rule for implementing pipelining. */
      rule rl_pipeline;
         for(Integer i = 1 ; i <= `STAGES_FMA_DP - 5 ; i = i+1) begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule

      method Action send(Tuple4#(FloatingPoint#(11,52),FloatingPoint#(11,52), Maybe#(FloatingPoint#(11,52)), RoundMode) operands);
         wr_operands <= operands;		
      endmethod

      method ReturnType#(11,52) receive();
         return ReturnType{valid:rg_stage_valid[`STAGES_FMA_DP-5],value:tpl_1(rg_stage_out[`STAGES_FMA_DP-5]) ,ex:tpl_2(rg_stage_out[`STAGES_FMA_DP-5])};
      endmethod 
   endmodule

endpackage
  
