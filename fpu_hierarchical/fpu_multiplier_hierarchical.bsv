////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
--------------------------------------------------------------------------------------------------

Author: Neel Gala, Shalender Kumar, Sujay Pandit, Lokhesh Kumar
Email id: neelgala@gmail.com, cs18m050@smail.iitm.ac.in, contact.sujaypandit@gmail.com, lokhesh.kumar@gmail.com
--------------------------------------------------------------------------------------------------
*/
////////////////////////////////////////////////////////////////////////////////
//  Filename      : Fpu_Multiplier.bsv
////////////////////////////////////////////////////////////////////////////////
package fpu_multiplier_hierarchical;
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import DReg ::*;
`include "fpu_parameters.bsv"

`ifdef fpu_hierarchical

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// Floating Point Multiplier
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
function Tuple2#(FloatingPoint#(e,m),Exception) fn_fpu_multiplier ( FloatingPoint#(e,m) in1, FloatingPoint#(e,m) in2, RoundMode rmode )
provisos(
	Add#(a__, TLog#(TAdd#(1, TAdd#(TAdd#(m, 1), TAdd#(m, 1)))), TAdd#(e, 1))
);

   function Tuple5#(CommonState#(e,m),
		    Bit#(TAdd#(m,1)),
		    Bit#(TAdd#(m,1)),
		    Int#(TAdd#(e,2)),
		    Bool) s1_stage(Tuple3#(FloatingPoint#(e,m),
					   FloatingPoint#(e,m),
					   RoundMode) op);

      match { .opA, .opB, .rmode } = op;

      CommonState#(e,m) s = CommonState {
	 res: tagged Invalid,
	 exc: defaultValue,
	 rmode: rmode
	 };

      Int#(TAdd#(e,2)) expA = isSubNormal(opA) ? fromInteger(minexp(opA)) : signExtend(unpack(unbias(opA)));
      Int#(TAdd#(e,2)) expB = isSubNormal(opB) ? fromInteger(minexp(opB)) : signExtend(unpack(unbias(opB)));
      Int#(TAdd#(e,2)) newexp = expA + expB;

      Bool sign = (opA.sign != opB.sign);

      Bit#(TAdd#(m,1)) opAsfd = { getHiddenBit(opA), opA.sfd };
      Bit#(TAdd#(m,1)) opBsfd = { getHiddenBit(opB), opB.sfd };

      if (isSNaN(opA)) begin
	 s.res = tagged Valid nanQuiet(opA);
	 s.exc.invalid_op = True;
      end
      else if (isSNaN(opB)) begin
	 s.res = tagged Valid nanQuiet(opB);
	 s.exc.invalid_op = True;
      end
      else if (isQNaN(opA)) begin
	 s.res = tagged Valid opA;
      end
      else if (isQNaN(opB)) begin
	 s.res = tagged Valid opB;
      end
      else if ((isInfinity(opA) && isZero(opB)) || (isZero(opA) && isInfinity(opB))) begin
	 s.res = tagged Valid qnan();
	 s.exc.invalid_op = True;
      end
      else if (isInfinity(opA) || isInfinity(opB)) begin
	 s.res = tagged Valid infinity(opA.sign != opB.sign);
      end
      else if (isZero(opA) || isZero(opB)) begin
	 s.res = tagged Valid zero(opA.sign != opB.sign);
      end
      else if (newexp > fromInteger(maxexp(opA))) begin
	 FloatingPoint#(e,m) out;
	 out.sign = (opA.sign != opB.sign);
	 out.exp = maxBound - 1;
	 out.sfd = maxBound;

	 s.exc.overflow = True;
	 s.exc.inexact = True;

	 let y = round(rmode, out, '1);
	 s.res = tagged Valid tpl_1(y);
	 s.exc = s.exc | tpl_2(y);
      end
      else if (newexp < (fromInteger(minexp_subnormal(opA))-2)) begin
	 FloatingPoint#(e,m) out;
	 out.sign = (opA.sign != opB.sign);
	 out.exp = 0;
	 out.sfd = 0;

	 s.exc.underflow = True;
	 s.exc.inexact = True;

	 let y = round(rmode, out, 'b01);
	 s.res = tagged Valid tpl_1(y);
	 s.exc = s.exc | tpl_2(y);
      end

      return tuple5(s,
		    opAsfd,
		    opBsfd,
		    newexp,
		    sign);
   endfunction

   function Tuple4#(CommonState#(e,m),
		    Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
		    Int#(TAdd#(e,2)),
		    Bool) s2_stage(Tuple5#(CommonState#(e,m),
					   Bit#(TAdd#(m,1)),
					   Bit#(TAdd#(m,1)),
					   Int#(TAdd#(e,2)),
					   Bool) op);

      match {.s, .opAsfd, .opBsfd, .exp, .sign} = op;

      Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))) sfdres = primMul(opAsfd, opBsfd);

      return tuple4(s,
		    sfdres,
		    exp,
		    sign);
   endfunction

   function Tuple4#(CommonState#(e,m),
		    Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
		    Int#(TAdd#(e,2)),
		    Bool) s3_stage(Tuple4#(CommonState#(e,m),
					   Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
					   Int#(TAdd#(e,2)),
					   Bool) op);
      return op;
   endfunction

   function Tuple3#(CommonState#(e,m),
		    FloatingPoint#(e,m),
		    Bit#(2)) s4_stage(Tuple4#(CommonState#(e,m),
					      Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
					      Int#(TAdd#(e,2)),
					      Bool) op);

      match {.s, .sfdres, .exp, .sign} = op;

      FloatingPoint#(e,m) result = defaultValue;
      Bit#(2) guard = ?;

      if (s.res matches tagged Invalid) begin
	 //$display("sfdres = 'h%x", sfdres);

	 let shift = fromInteger(minexp(result)) - exp;
	 if (shift > 0) begin
	    // subnormal
	    Bit#(1) sfdlsb = |(sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift));

	    //$display("sfdlsb = |'h%x = 'b%b", (sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift)), sfdlsb);

            sfdres = sfdres >> shift;
            sfdres[0] = sfdres[0] | sfdlsb;

	    result.exp = 0;
	 end
	 else begin
	    result.exp = cExtend(exp + fromInteger(bias(result)));
	 end

	 // $display("shift = %d", shift);
	 // $display("sfdres = 'h%x", sfdres);
	 // $display("result = ", fshow(result));
	 // $display("exc = 'b%b", pack(exc));
	 // $display("zeros = %d", countZerosMSB(sfdres));

	 result.sign = sign;
	 let y = normalize(result, sfdres);
	 result = tpl_1(y);
	 guard = tpl_2(y);
	 s.exc = s.exc | tpl_3(y);

	 // $display("result = ", fshow(result));
	 // $display("exc = 'b%b", pack(exc));
      end

      return tuple3(s,
		    result,
		    guard);
   endfunction

   function Tuple2#(FloatingPoint#(e,m),
		    Exception) s5_stage(Tuple3#(CommonState#(e,m),
						FloatingPoint#(e,m),
						Bit#(2)) op);

      match {.s, .rnd, .guard} = op;

      FloatingPoint#(e,m) out = rnd;

      if (s.res matches tagged Valid .x)
	 out = x;
      else begin
	 let y = round(s.rmode, out, guard);
	 out = tpl_1(y);
	 s.exc = s.exc | tpl_2(y);
      end
//*********************************************************************************
	if(out.exp == 1 && out.sfd == 0 && guard == 'h3)
		s.exc.underflow = False;
//************************************************************************************
      return tuple2(canonicalize(out),s.exc);
   endfunction

   return s5_stage( s4_stage( s3_stage( s2_stage( s1_stage(tuple3(in1,in2,rmode)) ) ) ) );
endfunction

	interface Ifc_fpu_multiplier_sp;
		method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) operands);					 
		method ReturnType#(8,23) receive();
	endinterface
	//
	interface Ifc_fpu_multiplier_dp;
		method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) operands);			
		method ReturnType#(11,52) receive();
	endinterface

	(*synthesize*)
	module mk_fpu_multiplier_sp(Ifc_fpu_multiplier_sp);
		Vector#(`STAGES_FMUL_SP,Reg#(Tuple2#(FloatingPoint#(8,23),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
		Vector#(`STAGES_FMUL_SP,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
		rule rl_pipeline;
			for(Integer i = 1 ; i <= `STAGES_FMUL_SP -1 ; i = i+1)
			begin
					rg_stage_out[i] <= rg_stage_out[i-1];
					rg_stage_valid[i] <= rg_stage_valid[i-1];
			end
		endrule
		method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) operands);					 
						rg_stage_out[0] <= fn_fpu_multiplier(tpl_1(operands),tpl_2(operands),tpl_3(operands));
						rg_stage_valid[0] <= 1;
		endmethod
		method ReturnType#(8,23) receive();
			return ReturnType{valid:rg_stage_valid[`STAGES_FMUL_SP-1],value:tpl_1(rg_stage_out[`STAGES_FMUL_SP-1]),ex:tpl_2(rg_stage_out[`STAGES_FMUL_SP-1])};
		endmethod  
	endmodule
	//
	(*synthesize*)
	module mk_fpu_multiplier_dp(Ifc_fpu_multiplier_dp);
		Vector#(`STAGES_FMUL_DP,Reg#(Tuple2#(FloatingPoint#(11,52),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
		Vector#(`STAGES_FMUL_DP,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
		rule rl_pipeline;
			for(Integer i = 1 ; i <= `STAGES_FMUL_DP -1 ; i = i+1)
			begin
					rg_stage_out[i] <= rg_stage_out[i-1];
					rg_stage_valid[i] <= rg_stage_valid[i-1];
			end
		endrule
		method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) operands);					 
						rg_stage_out[0] <= fn_fpu_multiplier(tpl_1(operands),tpl_2(operands),tpl_3(operands));
						rg_stage_valid[0] <= 1;
		endmethod
		method ReturnType#(11,52) receive();
			return ReturnType{valid:rg_stage_valid[`STAGES_FMUL_DP-1],value:tpl_1(rg_stage_out[`STAGES_FMUL_DP-1]),ex:tpl_2(rg_stage_out[`STAGES_FMUL_DP-1])};
		endmethod  
	endmodule
`endif
//  module mkTb();
//  	Reg#(int) rg_cycle <- mkReg(0);
//  	Ifc_fpu_multiplier#(8,23,4) ifc <- mk_fpu_multiplier();
//  	rule rl_cycle;
//  		rg_cycle <= rg_cycle +1;
//  		if(rg_cycle>10)
//  			$finish(0);
//  	endrule
//  	rule rl_stage1(rg_cycle==1);
//  		FloatingPoint#(8,23) op1 = FloatingPoint {
//  										sign:       False,
//  										exp:        8'b10000000,
//  										sfd:        23'b00000000000000000000000
//  									}; // decimal value = 2
//  		FloatingPoint#(8,23) op2 = FloatingPoint {
//  			               sign:       False,
//  			               exp:        8'b10000000,
//  			               sfd:        23'b00000000000000000000000
//  									}; // decimal value = 2
//  		RoundMode op4 = Rnd_Nearest_Even;
//  		ifc.send(tuple3(op1,op2,op4));
//  	endrule
//  	rule rl_receive;
// // 		match {.valid, .out} = ifc.receive();
//  		let x = ifc.receive();
//  		$display("cycle %d: valid = %b : result = %b : exc = %b",rg_cycle,x.valid,x.value,x.ex);
//  	endrule
//  endmodule

endpackage
