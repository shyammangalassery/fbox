////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Harrish A
Email id: harrish1499@gmail.com
Details: Double Precision Fused Multiply-Add (Pipelined) Testbench

--------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
/// DOUBLE PRECISION Floating point fused multiple accumulate Testbench
////////////////////////////////////////////////////////////////////////////////

package tb_float_fma_dp; 
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  //import fpu_fma :: * ;
  import RegFile :: * ;
  import fpu_common :: * ;
  //*******************************************************************
  import Real              ::*;
  import Vector            ::*;
  import BUtils            ::*;
  import DefaultValue      ::*;
  import FShow             ::*;
  import GetPut            ::*;
  import ClientServer      ::*;
  import FIFO              ::*;
  import Divide            ::*;
  import SquareRoot        ::*;
  import FixedPoint        ::*;
  import fpu_fma_dp_final_11_stages :: *;
  //*******************************************************************
  //`ifdef FLEN64
  //  `define FLEN 64	
  //  `define a1 51
  //  `define a2 52
  //  `define a3 62
  //  `define a4 63
  //  `define MOD mk_fpu_fma_dp_instance

  //`else
  //  `define FLEN 32
  //  `define a1 22
  //  `define a2 23
  //  `define a3 30
  //  `define a4 31
  //  `define MOD mk_fpu_fma_sp_instance
  //`endif

  `define stim_size 6133248 //6133239 6133248
  `define index_size 53
  `define entry_size 270
  `define FLEN 64	
  `define a1 51
  `define a2 52
  `define a3 62
  `define a4 63
  `define MOD mk_fpu_fma_dp
  `define STAGES 11
  `define No_Testcases 6000000			// TESTCASES TO TEST MAX : stim_size

  (*synthesize*)
  module mktb_float(Empty);

    RegFile#(Bit#(`index_size) , Bit#(`entry_size)) stimulus <- mkRegFileLoad("Testcases/64_rnear_maxMag_Testcases.txt", 0, `stim_size-1);
    let fadd <- (`MOD);

    //FIFOF#(Tuple2#(Bit#(`FLEN), Bit#(5))) ff_golden_output <- mkSizedFIFOF(2);
    Reg#(Bit#(`index_size)) read_index <- mkReg(0);
    Reg#(Bit#(`index_size)) golden_index <- mkReg(0);
    Reg#(int) rg_cycle <- mkReg(0);
    Reg#(Bit#(8)) misflag <- mkReg(0);
    Reg#(Bool) _ready <- mkReg(False);

    Reg#(Bit#(`FLEN)) e_output_1 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_1 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_1 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_1 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_1 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_2 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_2 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_2 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_2 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_2 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_3 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_3 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_3 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_3 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_3 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_4 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_4 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_4 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_4 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_4 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_5 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_5 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_5 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_5 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_5 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_6 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_6 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_6 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_6 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_6 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_7 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_7 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_7 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_7 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_7 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_8 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_8 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_8 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_8 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_8 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_9 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_9 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_9 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_9 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_9 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_10 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_10 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_10 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_10 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_10 <- mkReg(0);

    Reg#(Bit#(`FLEN)) e_output_11 <- mkReg(0);
    Reg#(Bit#(8)) e_flags_11 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opA_11 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opB_11 <- mkReg(0);
    Reg#(FloatingPoint#(11, 52)) rg_opC_11 <- mkReg(0);

    rule cycle;
      if(rg_cycle==11) begin
        _ready<=True;
        rg_cycle <= 0;
      end

      else
       rg_cycle<=rg_cycle+1;
    endrule

    rule rl_pick_stimulus_entry;
      
      let _e = stimulus.sub(read_index); 
      Bit#(8) _flags = truncate(_e);

      _e = _e >> 8;
      Bit#(`FLEN) _output = truncate(_e);
      _e = _e >> `FLEN;
      Bit#(`FLEN) _inp3 = truncate(_e);
      _e = _e >> `FLEN;
      Bit#(`FLEN) _inp2 = truncate(_e);
      _e = _e >> `FLEN;
      Bit#(`FLEN) _inp1 = truncate(_e);

      let op1 = FloatingPoint{sign : unpack(_inp1[`a4]), exp: _inp1[`a3:`a2], sfd: _inp1[`a1:0]};
      let op2 = FloatingPoint{sign : unpack(_inp2[`a4]), exp: _inp2[`a3:`a2], sfd: _inp2[`a1:0]};
      let op3 = FloatingPoint{sign : unpack(_inp3[`a4]), exp: _inp3[`a3:`a2], sfd: _inp3[`a1:0]};

      /* Rnd_Nearest_Even	//rnear_even 	Round to nearest/even. 
         Rnd_Zero		//rminMag 	Round to minimum magnitude (toward zero).  64_rminMag_Testcases
         Rnd_Minus_Inf		//rmin 		Round to minimum (down).
         Rnd_Plus_Inf		//rmax 		Round to maximum (up).
         Rnd_Nearest_Away_Zero	//rnear_maxMag 	Round to nearest/maximum magnitude (nearest/away).*/

      RoundMode rounding_mode = Rnd_Nearest_Away_Zero;
      //RoundMode rounding_mode = Rnd_Plus_Inf;	
      //RoundMode rounding_mode = Rnd_Zero;	
      //RoundMode rounding_mode =  Rnd_Nearest_Even;
      //RoundMode rounding_mode =  Rnd_Minus_Inf;	
      fadd.send(tuple4(op1, op2, tagged Valid op3, rounding_mode));
      read_index <= read_index + 1;

      e_output_1 <= _output;
      e_flags_1  <=  truncate(_flags);  
      rg_opA_1 <= op1;
      rg_opB_1 <= op2;
      rg_opC_1 <= op3;   
      if(read_index == `No_Testcases +`STAGES)
        $finish(0);
    endrule

    rule r1;
      e_output_2 <= e_output_1;
      e_flags_2 <= e_flags_1;
      rg_opA_2 <= rg_opA_1;
      rg_opB_2 <= rg_opB_1;
      rg_opC_2 <= rg_opC_1; 
    endrule

    rule r2;
      e_output_3 <= e_output_2;
      e_flags_3 <= e_flags_2;
      rg_opA_3 <= rg_opA_2;
      rg_opB_3 <= rg_opB_2;
      rg_opC_3 <= rg_opC_2; 
    endrule

    rule r3;
      e_output_4 <= e_output_3;
      e_flags_4 <= e_flags_3;
      rg_opA_4 <= rg_opA_3;
      rg_opB_4 <= rg_opB_3;
      rg_opC_4 <= rg_opC_3; 
    endrule

    rule r4;
      e_output_5 <= e_output_4;
      e_flags_5 <= e_flags_4;
      rg_opA_5 <= rg_opA_4;
      rg_opB_5 <= rg_opB_4;
      rg_opC_5 <= rg_opC_4; 
    endrule

    rule r5;
      e_output_6 <= e_output_5;
      e_flags_6 <= e_flags_5;
      rg_opA_6 <= rg_opA_5;
      rg_opB_6 <= rg_opB_5;
      rg_opC_6 <= rg_opC_5;  
    endrule

    rule r6;
      e_output_7 <= e_output_6;
      e_flags_7 <= e_flags_6;
      rg_opA_7 <= rg_opA_6;
      rg_opB_7 <= rg_opB_6;
      rg_opC_7 <= rg_opC_6; 
    endrule

    rule r7;
      e_output_8 <= e_output_7;
      e_flags_8 <= e_flags_7;
      rg_opA_8 <= rg_opA_7;
      rg_opB_8 <= rg_opB_7;
      rg_opC_8 <= rg_opC_7; 
    endrule

    rule r8;
      e_output_9 <= e_output_8;
      e_flags_9 <= e_flags_8;
      rg_opA_9 <= rg_opA_8;
      rg_opB_9 <= rg_opB_8;
      rg_opC_9 <= rg_opC_8; 
    endrule

    rule r9;
      e_output_10 <= e_output_9;
      e_flags_10 <= e_flags_9;
      rg_opA_10 <= rg_opA_9;
      rg_opB_10 <= rg_opB_9;
      rg_opC_10 <= rg_opC_9; 
    endrule

    rule r10;
      e_output_11 <= e_output_10;
      e_flags_11 <= e_flags_10;
      rg_opA_11 <= rg_opA_10;
      rg_opB_11 <= rg_opB_10;
      rg_opC_11 <= rg_opC_10; 
    endrule

    rule rl_check_output(_ready==True);
      let x = fadd.receive();
      let valid = x.valid;
      let out = x.value;
      let flags = x.ex;
      let index = read_index -11;
      let op1 = rg_opA_11;
      let op2 = rg_opB_11;
      let op3 = rg_opC_11;
      
      if(valid==1) begin
        Bit#(`FLEN) _out = {pack(out.sign), out.exp, out.sfd}; 
        if( _out != pack(e_output_11)) begin
          `logLevel( tb, 0, $format("TB: Sending inputs [%d]: op1:%h op2:%h, op3:%h, output %h",index, op1, op2,op3,pack(e_output_11))) 
          `logLevel( tb, 0, $format("TB: Outputsmismatch[%d], G:%h R:%h,flag G: %h, flag R: %h, VALID: %b",index,pack(e_output_11), _out,pack(e_flags_11), flags, valid))//
        end
        else begin
          `logLevel( tb, 0, $format("TB: Sending inputs [%d]: op1:%h op2:%h, op3:%h, output %h",index, op1, op2,op3,pack(e_output_11))) 
          `logLevel( tb, 0, $format("TB: Outputs match  [%d], G:%h R:%h,flag G: %h, flag R: %h, VALID: %b",index,pack(e_output_11), _out,pack(e_flags_11), flags, valid))	
        end

        if( flags != unpack(truncate(pack(e_flags_11)))) begin
          Bit#(8) value = e_flags_11;
          `logLevel( tb, 0, $format("TB: Sending inputs [%d]: op1:%h op2:%h, op3:%h, output %h",index, op1, op2,op3,pack(e_output_11))) 
          `logLevel( tb, 0, $format("TB: Flags mismatch [%d]: G:%h R:%h B:%h output G: %h , R: %h ",index,pack(e_flags_11), flags, value,pack(e_output_11), _out))
        end
      end
      else begin
        `logLevel( tb, 0, $format("TB: INVALID_HIT[%d]",index))
      end
    endrule

endmodule
endpackage
