//////////////////////////////
// see LICENSE.iitm
//////////////////////////////
package Tb_fp_conv_sp_int; 
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import RegFile :: * ;

  import fpu_sp_to_int_pipelined ::*;
  import fpu_common::*;
//  `include "Logger.bsv"


  (*synthesize*)
  module mktb_conv_sp_int64(Empty);
    RegFile#(Bit#(23) , Bit#(104)) stimulus <- mkRegFileLoad(rdn_11_1k.txt", 0,998);
    let fconv <- mk_fpu_sp_to_int();

    Reg#(Bit#(23)) read_index <- mkReg(0);
    Reg#(Bit#(64)) ex_out <- mkReg(0);
    Reg#(Bit#(8)) ex_flags <- mkReg(0);
     
    Reg#(Bit#(8)) exp_flags0 <- mkReg(0);
    Reg#(Bit#(64)) exp_output0 <- mkReg(0);
    Reg#(Bit#(8)) exp_flags1 <- mkReg(0);
    Reg#(Bit#(64)) exp_output1 <- mkReg(0);
    Reg#(int) count <- mkReg(0);
    Reg#(Bool) _ready <- mkReg(False);


    rule counter;
      if(count==1)
        _ready<=True;
      else
        count<=count+1;
    endrule

    rule rl_pick_stimulus_entry;
      let _e = stimulus.sub(read_index);
      Bit#(8) e_flags = truncate(_e);

      _e = _e >> 8;
      Bit#(64) e_out = truncate(_e);

      _e = _e >> 64;
      Bit#(32) _inp = truncate(_e);
      
      let op1 = FloatingPoint{sign : unpack(_inp[31]), exp: _inp[30:23], sfd: _inp[22:0]};
      
      fconv.start (tuple4(op1, Rnd_Minus_Inf, 1'b1, 1'b1));
      exp_output0 <= e_out;
      exp_flags0 <= e_flags; 
 
  //   `logLevel( tb, 0, $format("TB: Sending inputs[%d]: input %h # output %h flags %h", read_index, _inp, e_out, e_flags))
   $display("TB: Sending inputs[%d]: input %h # output %h flags %h", read_index, _inp, e_out, e_flags);

      read_index <= read_index + 1;
      if(read_index == 998)
        $finish(0);
    endrule

    rule rl_stage1;
      exp_output1 <= exp_output0;
      exp_flags1 <= exp_flags0;
    endrule
  
   
    rule rl_check_output(_ready==True);
      let x =  fconv.receive();  
      let valid = x.valid;
      let out = x.value;
      let flags = x.ex;

      if(valid==1) begin
        Bit#(64) _out = out;     
        if ( _out != pack(exp_output1)) begin
        $display("TB: Outputs mismatch[%d]. E:%h R:%h,flag %h",read_index, pack(exp_output1), _out,flags);
        end
        //else begin
        //  $display("TB: Outputs match[%d]. E:%h R:%h,flag %h",read_index, pack(exp_output1), _out,flags);
        //end
     
        if (flags != unpack(truncate(pack(exp_flags1)))) begin
          $display("TB: Flags mismatch[%d]. E:%h R:%h output G: %h , R: %h ",read_index, pack(exp_flags1), flags,pack(exp_output1),_out);
        end
      end
    endrule

  endmodule

(*synthesize*)
  module mktb_conv_sp_int32(Empty);
    RegFile#(Bit#(23) , Bit#(72)) stimulus <- mkRegFileLoad("rdn_10_1k.txt", 0,999);
    let fconv <- mk_fpu_sp_to_int();

    Reg#(Bit#(23)) read_index <- mkReg(0);
    Reg#(Bit#(32)) ex_out <- mkReg(0);
    Reg#(Bit#(8)) ex_flags <- mkReg(0);
     
    Reg#(Bit#(8)) exp_flags0 <- mkReg(0);
    Reg#(Bit#(32)) exp_output0 <- mkReg(0);
    Reg#(Bit#(8)) exp_flags1 <- mkReg(0);
    Reg#(Bit#(32)) exp_output1 <- mkReg(0);
    Reg#(int) count <- mkReg(0);
    Reg#(Bool) _ready <- mkReg(False);


    rule counter;
      if(count==1)
        _ready<=True;
      else
        count<=count+1;
    endrule

    rule rl_pick_stimulus_entry;
      let _e = stimulus.sub(read_index);
      Bit#(8) e_flags = truncate(_e);

      _e = _e >> 8;
      Bit#(32) e_out = truncate(_e);

      _e = _e >> 32;
      Bit#(32) _inp = truncate(_e);
      
      let op1 = FloatingPoint{sign : unpack(_inp[31]), exp: _inp[30:23], sfd: _inp[22:0]};
      
      fconv.start (tuple4(op1, Rnd_Minus_Inf, 1'b1, 1'b0));
      exp_output0 <= e_out;
      exp_flags0 <= e_flags; 
 
  //   `logLevel( tb, 0, $format("TB: Sending inputs[%d]: input %h # output %h flags %h", read_index, _inp, e_out, e_flags))
   $display("TB: Sending inputs[%d]: input %h # output %h flags %h", read_index, _inp, e_out, e_flags);

      read_index <= read_index + 1;
      if(read_index == 999)
        $finish(0);
    endrule

    rule rl_stage1;
      exp_output1 <= exp_output0;
      exp_flags1 <= exp_flags0;
    endrule
  
   
    rule rl_check_output(_ready==True);
      let x =  fconv.receive();  
      let valid = x.valid;
      let out = truncate(x.value);
      let flags = x.ex;

      if(valid==1) begin
        Bit#(32) _out = out;     
        if ( _out != pack(exp_output1)) begin
          $display("TB: Outputs mismatch[%d]. E:%h R:%h,flag %h",read_index, pack(exp_output1), _out,flags);
        end
        //else begin
        //  $display("TB: Outputs match[%d]. E:%h R:%h,flag %h",read_index, pack(exp_output1), _out,flags);
        //end
     
        if (flags != unpack(truncate(pack(exp_flags1)))) begin
          $display("TB: Flags mismatch[%d]. E:%h R:%h output G: %h , R: %h ",read_index, pack(exp_flags1), flags,pack(exp_output1),_out);
        end
      end
    endrule

  endmodule

  endpackage

