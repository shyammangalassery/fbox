//////////////////////////////
// see LICENSE.iitm
//////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Nagakaushik Moturi
Email id: ee17b111@smail.iitm.ac.in
Details: Simple testbench for a few direct inputs
--------------------------------------------------------------------------------------------------
*/
package tb_fconv_simple;

  import fpu_convert_pipelined1 :: * ;
  import fpu_common :: *;
  import Randomizable ::*;
  `include "../modules_fp/bsv_testbench/Logger.bsv"

  (*synthesize*)
  module mktb_fconv_simple();
    let fconv <- mk_fpu_convert_dp_to_sp();
    Reg#(Bit#(8)) rg_cycle <- mkReg(0);
    Reg#(Bit#(32)) feed <- mkReg(0);
    Reg#(Bit#(64)) rand1 <- mkReg(0);
 
    Randomize#(Bit#(64)) rand_in1 <- mkConstrainedRandomizer(64'd0,64'hffffffffffffffff);
    
    rule init(feed == 0);            //initializing the 2 random values
      rand_in1.cntrl.init();
      feed <= 1;
    endrule
    
    rule rl_stage1(feed==1);              
      let a <- rand_in1.next();
      rand1 <= a;
      
      FloatingPoint#(11, 52) f,g;
      RoundMode r1 = Rnd_Nearest_Even;

      f.sign = unpack(rand1[63]);
      f.exp = rand1[62:52];
      f.sfd = rand1[51:0];
      
      fconv.start(tuple2(f,r1));
      $display(rg_cycle, "send","%h", f);
    endrule
   
    rule rl_receive;
      ReturnType#(8, 23) r;

      r = fconv.receive();
      
      $display("cycle:",rg_cycle,"  %h",r.value);
      
    endrule

    rule rl_end;
      rg_cycle <= rg_cycle + 1;  //incrementing the clock
      if (rg_cycle > 20) begin
        $finish(0);
      end
    endrule

  endmodule
endpackage
