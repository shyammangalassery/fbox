////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
package fpu_sp_to_int_pipelined;

import Vector ::*;
import DefaultValue ::*;
import DReg ::*;
import fpu_common ::*;

/****************************************************************************************/
interface Ifc_fpu_sp_to_int;
        method Action start(Tuple4#(FloatingPoint#(8,23), RoundMode ,Bit#(1), Bit#(1))data_in);
        method ReturnTypeInt#(64) receive();
endinterface
/**********************************************************************************************/

(*synthesize*)
 module mk_fpu_sp_to_int(Ifc_fpu_sp_to_int);

 Wire#(FloatingPoint#(8,23)) wr_inp_fp <-mkDWire(unpack(0));
 Wire#(RoundMode) wr_rounding_mode <-mkDWire(defaultValue);
 Wire#(Bit#(1)) wr_conv_unsign <-mkDWire(0);
 Wire#(Bit#(1)) wr_conv_long  <-mkDWire(0);
 Wire#(Bit#(1)) wr_valid <- mkDWire(0);
 Wire#(ReturnTypeInt#(64)) wr_stage2_result <- mkDWire(unpack(0));

 Reg#(Bit#(1)) rg_stage0_valid <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_sign <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_conv_unsign <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_conv_long <-mkReg(0);
 Reg#(RoundMode) rg_stage0_rounding_mode <- mkReg(defaultValue);
 Reg#(Bit#(64)) rg_stage0_final_result  <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_inf  <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_invalid <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_zero <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_inexact <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_man_zero <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_overflow <-mkReg(0);
 Reg#(Bit#(1)) rg_stage0_denormal <-mkReg(0);
 Reg#(Int#(8)) rg_stage0_original_exponent <-mkReg(0);
 Reg#(Bool) rg_stage0_to_round <-mkReg(False);
 Reg#(Bit#(TAdd#(23, 64))) rg_stage0_final_man <-mkReg(0);
 Reg#(FloatingPoint#(8,23)) rg_stage0_inp_fp <-mkReg(unpack(0));

 Reg#(Bit#(1)) rg_stop_conv <-mkReg(0);
 Reg#(Bit#(1)) rg_stage1_valid <-mkReg(0);
 Reg#(Bit#(1)) rg_stage1_round_up <-mkReg(0);
 Reg#(Bit#(1)) rg_stage1_conv_unsign <-mkReg(0);
 Reg#(Bit#(1)) rg_stage1_conv_long <-mkReg(0);
 Reg#(Bit#(1)) rg_stage1_inf<-mkReg(0);
 Reg#(Bit#(23)) rg_stage1_sfd1 <-mkReg(0);
 Reg#(Bit#(1)) rg_stage1_overflow<-mkReg(0);
 Reg#(Bit#(1))rg_stage1_inexact1<-mkReg(0);
 Reg#(Bit#(1))rg_stage1_inexact2<-mkReg(0);
 Reg#(Bit#(1))rg_stage1_invalid<-mkReg(0);
 Reg#(Int#(8)) rg_stage1_original_exponent <-mkReg(0);
 Reg#(Bit#(64)) rg_stage1_final_result  <-mkReg(0);
 Reg#(Bit#(1)) rg_stage1_sign <-mkReg(0);
 Reg#(RoundMode) rg_stage1_rounding_mode <-mkReg(defaultValue);
 Reg#(Bool) rg_stage1_to_round <-mkReg(False);
 Reg#(Bit#(1)) rg_stage1_add_one <-mkReg(0);

/*****************************************************************************************************/
rule rl_stage0;                             /*** checking the special conditions, original_exp***/

  let lv_sign = pack(wr_inp_fp.sign);
  let lv_exponent  = wr_inp_fp.exp;
  let lv_sfd  = wr_inp_fp.sfd;
  let rnd =wr_rounding_mode;

Exception exc = ex_flag(wr_inp_fp);         // calculating the exceptions from input operand

 Bit#(5) lv_exc = pack(exc);
 let conv_unsign = wr_conv_unsign;
 let conv_long = wr_conv_long;
  
Bool rne = (rnd == Rnd_Nearest_Even);
Bool rtz = (rnd == Rnd_Zero);
Bool rdn = (rnd == Rnd_Minus_Inf);
Bool rup = (rnd == Rnd_Plus_Inf);
Bool rmm = (rnd == Rnd_Nearest_Away_Zero);

Bit#(1) lv_denormal =lv_exc[4];             // assigning the flags      
Bit#(1) lv_zero     = lv_exc[3];                  
Bit#(1) lv_inf      = lv_exc[1];                  

Bit#(1) lv_overflow = 0;
Bit#(1) lv_inexact = 0;
Bit#(1) lv_invalid =(lv_exc[0]|lv_exc[2]);
Bit#(1) lv_manzero = |lv_sfd;

Bit#(9) lv_exp = {1'b0,lv_exponent};
Int#(8) lv_original_exponent = unpack(truncate(lv_exp - 127));      //unbiasing the exponent and finding the original value

Bool to_round=False;
Bit#(64) final_result = 0;
Bit#(TAdd#(23, 64)) final_man = {'0,1'b1,lv_sfd};
Bit#(1) stop_conv=0;

if(lv_zero == 1)                            // special condition checking            
  final_result = 0;                         // checking the zero, -ve exponent values and assigning invalid, inexact flags  
else if(lv_denormal == 1 || (lv_original_exponent <= -1 && (lv_inf|lv_invalid) == 0))  begin
  stop_conv=1;                              // stop_conv flag =1 for bypassing the next stages
    if(lv_sign==1 && conv_unsign==1 && ((lv_original_exponent==-1 && (rmm||(rne && lv_manzero==1))) || (lv_original_exponent<=-1 &&rdn)))
      lv_invalid = 1; 
    else 
      lv_inexact = 1;
    if(lv_sign == 0 && rup)     
         final_result = 1;
    else if(rdn && lv_sign == 1 && conv_unsign == 0)
         final_result = '1;
    else if(lv_original_exponent == -1 && (rmm||(rne && lv_manzero == 1))) begin
       if(lv_sign == 0)
         final_result = 1;   
       else if(conv_unsign == 0)
          final_result = '1;
        else 
           final_result = 0;
     end   
     else 
       final_result = 0;
  end 
  
rg_stage0_valid <= wr_valid;
rg_stage0_sign <=lv_sign;
rg_stage0_conv_unsign<=conv_unsign;
rg_stage0_conv_long<=conv_long;
rg_stage0_rounding_mode<=wr_rounding_mode;
rg_stage0_final_result <= final_result;
rg_stage0_inf <=lv_inf;
rg_stage0_invalid<=lv_invalid;
rg_stage0_zero<=lv_zero;
rg_stage0_inexact<=lv_inexact;
rg_stage0_man_zero<=lv_manzero;
rg_stage0_original_exponent<=lv_original_exponent;
rg_stage0_final_man<=final_man;
rg_stage0_overflow<=lv_overflow;
rg_stage0_inp_fp<=wr_inp_fp;
rg_stage0_denormal<=lv_denormal;
rg_stop_conv <=stop_conv;
rg_stage0_to_round<=to_round;
endrule
/**********************************************************************************************************/
rule rl_stage1;                             /***sp to signed,unsigned word & long conversion & final rounding***/

Bit#(1) lv_sign1=rg_stage0_sign;
Bool to_round1 =False;
Bit#(64) final_result1 = 0;
Bit#(TAdd#(23, 64)) final_man1 = 0;
Bit#(23) lv_sfd1 = 0;
Bit#(1) lv_invalid1 = 0;
Bit#(1) lv_inexact1=0;

if (rg_stage0_zero==1)   begin              // Zero condition from stage0
    rg_stage1_final_result<=0;
    to_round1  =rg_stage0_to_round; 
    lv_invalid1=0;       
end
                                            //negative exponent condition from stage0
else if (((rg_stage0_invalid==1)&& (rg_stop_conv==1))||((rg_stage0_inexact==1) && (rg_stop_conv==1)))  begin
      rg_stage1_final_result <=rg_stage0_final_result;
      to_round1= rg_stage0_to_round;
      lv_invalid1=rg_stage0_invalid;
end
    
else begin      

 final_man1 = rg_stage0_final_man;
 lv_sfd1 = rg_stage0_inp_fp.sfd;
 lv_invalid1 = rg_stage0_invalid;

if (rg_stage0_conv_long == 0) begin         //sp to signed word conversion
  if(rg_stage0_conv_unsign == 0) begin
     Bit#(31) all_ones = '1;
     if(rg_stage0_inf == 1 || lv_invalid1 == 1) begin  
        final_result1 = (lv_sign1 ==1) ?(lv_invalid1==1? zeroExtend(all_ones) : signExtend(32'h80000000)) : zeroExtend(all_ones); 
      end
      else if(rg_stage0_original_exponent < 'd31)   begin
        final_man1 =  final_man1 << rg_stage0_original_exponent;
        Bit#(32) y = final_man1[54:23];     
        final_result1 = signExtend(y);
        lv_sfd1 = final_man1[22:0];         
        to_round1 = True;
       end
       else if(rg_stage0_original_exponent >= 'd31) begin
          lv_invalid1 = 1;
          if(lv_sign1 == 0)
             final_result1 = zeroExtend(all_ones);
           else  begin
              if(rg_stage0_original_exponent == 'd31 && rg_stage0_man_zero == 0)
                lv_invalid1 = 0 ;        //Since we are exactly representing the number? 
                final_result1 = signExtend(32'h80000000);
           end
       end
    end
    else begin                              //sp to unsigned word conversion  
       Bit#(32) all_ones = '1;
       if(rg_stage0_inf == 1 || lv_invalid1 == 1)
          final_result1 = (lv_sign1==1) ? (lv_invalid1==1? signExtend(all_ones) : '0) : signExtend(all_ones); 
       else if(rg_stage0_original_exponent < 'd32)  begin
         final_man1 = final_man1 << rg_stage0_original_exponent;
         Bit#(32) y = final_man1[54:23];      
         final_result1 = signExtend(y);
         lv_sfd1 = final_man1[22:0];                     
         to_round1 = True;
       end
       else if(rg_stage0_original_exponent >= 'd32)  begin
          lv_invalid1 = 1;
          if(lv_sign1 == 0)
             final_result1 = signExtend(all_ones);
           else
              final_result1 = '0;
       end
    end
 end
 else begin                                 // sp to signed long conversion
  if(rg_stage0_conv_unsign == 0) begin 
    Bit#(63) all_ones = '1;
     if(rg_stage0_inf == 1 || lv_invalid1 == 1)
       final_result1 = (lv_sign1==1) ?(lv_invalid1==1? zeroExtend(all_ones) : signExtend(64'h8000000000000000)) : zeroExtend(all_ones); 
     else if(rg_stage0_original_exponent < 'd63)  begin
       final_man1 = final_man1 << rg_stage0_original_exponent;
       Bit#(64) y = final_man1[86:23];
       final_result1 = y;
       lv_sfd1 = final_man1[22:0];         
       to_round1 = True;
     end
     else if(rg_stage0_original_exponent >= 'd63)  begin
       lv_invalid1 = 1;
        if(lv_sign1 == 0)
           final_result1 = zeroExtend(all_ones);
        else begin
           if(rg_stage0_original_exponent == 'd63 && rg_stage0_man_zero == 0 )
             lv_invalid1 = 0;  //Since we are exactly representing the input number
             final_result1 = signExtend(64'h8000000000000000);
        end
     end
  end
  else begin                                //sp to unsigned long conversion
    Bit#(64) all_ones = '1;
     if((rg_stage0_inf == 1) || (lv_invalid1 == 1)) begin
       final_result1 = (lv_sign1==1) ? (lv_invalid1==1? signExtend(all_ones) : '0) : signExtend(all_ones);
     end
     else if(rg_stage0_original_exponent < 'd64) begin
       final_man1 = final_man1 << (rg_stage0_original_exponent);
       Bit#(64) y = final_man1[86:23];
       final_result1 = y;
       lv_sfd1 =  final_man1[22:0];   
       to_round1 = True;
     end
     else if(rg_stage0_original_exponent >= 'd64) begin
       lv_invalid1 = 1;
         if(lv_sign1 == 0)
           final_result1 = signExtend(all_ones);
         else
           final_result1 = '0;
     end
  end
end
                                            // final rounding of the result 
  Bit#(1) lv_guard = lv_sfd1[22];	    //MSB of the already shifted mantissa is guard bit
  Bit#(1) lv_round = lv_sfd1[21];	    //next bit is round bit
  Bit#(1) lv_sticky = |(lv_sfd1<<2);	     //remaining bits determine the sticky bit
  Bit#(1) lv_round_up = 0;
  
   lv_inexact1 = lv_guard | lv_round | lv_sticky;

 if(to_round1)  begin
   case(rg_stage0_rounding_mode) 
     Rnd_Nearest_Even     :  lv_round_up = lv_guard & (final_result1[0] | lv_round | lv_sticky);      
     Rnd_Nearest_Away_Zero:  lv_round_up = lv_guard; //& (lv_round | lv_sticky | ~lv_sign1);               
     Rnd_Minus_Inf        :  lv_round_up = lv_inexact1 & (lv_sign1);                
     Rnd_Plus_Inf         : lv_round_up = lv_inexact1 & (~lv_sign1);                
     default              : lv_round_up = 0;
   endcase
            
// lv_inexact2 = lv_inexact1 | rg_stage0_inexact;
   if(lv_round_up == 1) begin                      //Should set the overflow flag here right?
     lv_invalid1 = 1;
   
       if(rg_stage0_conv_long == 0 && rg_stage0_conv_unsign == 0 && rg_stage0_original_exponent == 30 && final_result1[30:0] == '1 && lv_sign1 == 1'b0) 
	     final_result1 ='h7fffffff;								        //Overflow..  Beyond representable number after rounding
            
        else if(rg_stage0_conv_long == 0 && rg_stage0_conv_unsign == 1 && rg_stage0_original_exponent == 31 && final_result1[31:0] == '1 && lv_sign1 == 1'b0)
            final_result1 = '1;
       
        else if(rg_stage0_conv_long == 1 && rg_stage0_conv_unsign == 0 && rg_stage0_original_exponent == 62 && final_result1[62:0] == '1 && lv_sign1 == 1'b0) 
 	     final_result1 = 64'h7fffffffffffffff;						        //Overflow..  Beyond representable number after rounding
          
        else if(rg_stage0_conv_long == 1 && rg_stage0_conv_unsign == 1 && rg_stage0_original_exponent == 63 && final_result1[63:0] == '1 && lv_sign1 == 1'b0)
            final_result1 = 64'hffffffffffffffff;               
        
        else  begin    
           lv_invalid1 = 0;
           final_result1 = final_result1 + 1;
           if(rg_stage0_conv_long == 0 && final_result1[31]==1)
               final_result1 = signExtend(final_result1[31:0]);
        end        
    end                                     // end to lv_round_up=1 
 end                                        //end to to_round1
 rg_stage1_final_result <=final_result1;
end

rg_stage1_to_round<=to_round1;
rg_stage1_invalid<=lv_invalid1;
rg_stage1_inexact1<=rg_stage0_inexact;
rg_stage1_inexact2<=lv_inexact1;
rg_stage1_original_exponent<= rg_stage0_original_exponent;
rg_stage1_conv_unsign<=rg_stage0_conv_unsign;
rg_stage1_conv_long <=rg_stage0_conv_long;
rg_stage1_inf<= rg_stage0_inf;
rg_stage1_overflow<=rg_stage0_overflow;
rg_stage1_rounding_mode<=rg_stage0_rounding_mode;
rg_stage1_valid <= rg_stage0_valid;
rg_stage1_sign<=lv_sign1;

endrule
/***************************************************************************************************/
rule rl_stage2;                             /*** flag set for integer result***/

Bit#(64) final_result2 =rg_stage1_final_result ;
Bit#(1) lv_invalid2 = rg_stage1_invalid;
Bit#(1) lv_overflow2=rg_stage1_overflow;
Bit#(1) lv_inexact2=0;
Bit#(1) lv_sign1=rg_stage1_sign;

 if((rg_stage1_to_round)) begin
    if(rg_stage1_conv_unsign == 0 && lv_sign1 == 1'b1)begin		//Negating the output if floating point number is negative and converted to signed word/long
       final_result2 = ~final_result2 + 1;
      
         if(rg_stage1_conv_long == 0 && final_result2[31] == 1)
            final_result2 = signExtend(final_result2[31:0]);
    end
    else if(rg_stage1_conv_unsign == 1 && lv_sign1 == 1'b1) begin
        final_result2 = 0;
        lv_invalid2 = 1;
     end
 end 

   lv_inexact2 = rg_stage1_inexact1 | rg_stage1_inexact2;       // inexact flag set for long integers
	  
    if((lv_invalid2|rg_stage1_inf) == 1) begin        //What about Quiet NaN?? What does the Spec Say?
        lv_overflow2 = 0;
        lv_inexact2 = 0;
    end

 Bit#(5) fflags={lv_invalid2|rg_stage1_inf,1'b0,lv_overflow2,1'b0,lv_inexact2};
 Exception flags = unpack(fflags);  

wr_stage2_result <= ReturnTypeInt{valid:rg_stage1_valid,value:final_result2,ex:flags};

endrule
/*********************************************************************************************************/

method Action start(Tuple4#(FloatingPoint#(8,23) , RoundMode ,Bit#(1) , Bit#(1))data_in);
    wr_inp_fp    <= tpl_1(data_in);
wr_rounding_mode <= tpl_2(data_in);
wr_conv_unsign   <= tpl_3(data_in);
wr_conv_long     <= tpl_4(data_in);
wr_valid         <=1'b1;
 endmethod

method ReturnTypeInt#(64) receive();
let stage2_result = wr_stage2_result;
return stage2_result;
endmethod
endmodule
/********************************************************************************************************/

(*synthesize*)

module mktb_fpu_sp_to_int();

Ifc_fpu_sp_to_int fconv <-mk_fpu_sp_to_int();


Reg#(int) cycle <- mkReg(0);
Reg#(Bit#(4)) rg_state<-mkReg(0);

rule count_cycle;
cycle <= cycle+1;
if(cycle >3)
begin
$finish(0);
end
endrule

rule rl_start(cycle==0);
		FloatingPoint#(8,23) op1 = FloatingPoint {
				sign	: True,
				exp	: 8'b10011110,
				sfd	: 23'b00000000000000000000001 
			};
  
	      RoundMode op2 = Rnd_Nearest_Even;
               Bit#(1)  op3 = 1'b0;
               Bit#(1)  op4 = 1'b0;
      
	fconv.start(tuple4(op1,op2,op3,op4));
	
$display("ip_operands: %h  %b %b %b" , op1,op2,op3,op4);
endrule


rule receive;
  ReturnTypeInt#(64)  lv_out = fconv.receive();
  Bit#(64) lv_int_out = (lv_out.value);
  let lv_flags  = lv_out.ex;
     
 $display(" %d, Valid: %b, Value:%h , Flags:%b",cycle,lv_out.valid, lv_int_out,lv_flags);
endrule
 endmodule

endpackage



























