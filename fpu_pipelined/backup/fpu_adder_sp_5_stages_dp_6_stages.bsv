////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Joyanta Mondal, Moturi Nagakaushik
Email id: ee19m058@smail.iitm.ac.in, ee17b111@smail.iitm.ac.in
Details: SP Adder : 5 stages, DP Adder : 6 stages
--------------------------------------------------------------------------------------------------
*/

package fpu_adder;

import DReg ::*;
import Vector ::*;
import DefaultValue ::*;
import fpu_common ::*;


//Functions

// single precision function for addition and normalise
function Tuple3#(FloatingPoint#(8,23),Exception,Bit#(3)) fn_add_norm_sp(Bit#(1) sgn,Bit#(1) sub,Bit#(8) expn,Bit#(28) sfdX,Bit#(28) sfdY1);

//23 Bit CLA Adder

   Bit#(1) cin = (sub == 0)?0:1;                        //subtraction part
   Bit#(28) sfdY = (sub==0)?sfdY1:~(sfdY1);        
  
   Bit#(28) gen_carry = sfdX&sfdY;              //carry generation
   Bit#(28) prop_carry = sfdX|sfdY;             //carry propagation
   
   Vector#(28, Bit#(1)) gen_c = unpack(gen_carry);
   Vector#(28, Bit#(1)) prop_c = unpack(prop_carry);
   
   
   Vector#(8, Bit#(1)) act_c1 ;
   Vector#(8, Bit#(1)) act_c2_1 ;
   Vector#(8, Bit#(1)) act_c3_1 ;
   Vector#(8, Bit#(1)) act_c4_1 ;
   
   
   Vector#(8, Bit#(1)) act_c2_0 ;
   Vector#(8, Bit#(1)) act_c3_0 ;
   Vector#(8, Bit#(1)) act_c4_0 ;
   
   
   act_c1[0] = cin;                               // carry generation,1st segment
   // carry vector generation
   for (Integer k=1;k<8;k=k+1) begin
   act_c1[k] = gen_c[k-1] | (prop_c[k-1]&act_c1[k-1]);
   end
   Bit#(8) carry1 = pack(act_c1);
  
  
   act_c2_1[0] = 1'b1;                               // carry generation for c_in=1,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_1[k] = gen_c[k+6] | (prop_c[k+6]&act_c2_1[k-1]);
   end
   Bit#(8) carry2_1 = pack(act_c2_1);
  
   act_c2_0[0] = 1'b0;                               // carry generation for c_in=0,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_0[k] = gen_c[k+6] | (prop_c[k+6]&act_c2_0[k-1]);
   end
   Bit#(8) carry2_0 = pack(act_c2_0);
  
  
   act_c3_0[0] = 1'b0;                               // carry generation for c_in=0,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_0[k] = gen_c[k+13] | (prop_c[k+13]&act_c3_0[k-1]);
   end
   Bit#(8) carry3_0 = pack(act_c3_0);
  
   act_c3_1[0] = 1'b1;                               // carry generation for c_in=1,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_1[k] = gen_c[k+13] | (prop_c[k+13]&act_c3_1[k-1]);
   end
   Bit#(8) carry3_1 = pack(act_c3_1);
  
  
   act_c4_1[0] = 1'b1;                               // carry generation for c_in=1,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_1[k] = gen_c[k+20] | (prop_c[k+20]&act_c4_1[k-1]);
   end
   Bit#(8) carry4_1 = pack(act_c4_1);
  
   act_c4_0[0] = 1'b0;                               // carry generation for c_in=0,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_0[k] = gen_c[k+20] | (prop_c[k+20]&act_c4_0[k-1]);
   end
   Bit#(8) carry4_0 = pack(act_c4_0);
   
   
   
   
   //choosing the carry segments according to actual c_in
   Bit#(8) carry2 = (carry1[7]==1'b1)?carry2_1:carry2_0;
   Bit#(8) carry3 = (carry2[7]==1'b1)?carry3_1:carry3_0;
   Bit#(8) carry4 = (carry3[7]==1'b1)?carry4_1:carry4_0;
   
   
   //generating actual carry vector
   Bit#(28) carry = {carry4[6:0],carry3[6:0],carry2[6:0],carry1[6:0]}; 
    
   //Actual sum generation 
   Bit#(28) res = sfdX^sfdY^carry;         //final result of CLA Adder for mantissa bits


   

   
   
   Int#(9) expo = (|expn==0)?unpack(9'b110000010):unpack(zeroExtend(expn)-9'd127);
   Bit#(3) guard = 0;
   Bit#(5) ex = 0;
   Bit#(23) sfdres=0;
   
   FloatingPoint#(8,23) out = defaultValue;
   
   
   
   
   Vector#(7, Bit#(1)) v1 = unpack(res[27:21]);                        //counting MSB 0's of CLA result,1st segment
   Integer result1=0;                                  
   Bool done1 = False;
   for( Integer p1 = 6; p1 >=0; p1 = p1 - 1) 
     begin
       if ((v1[p1] == 0)&&(!done1))  result1 = result1+1 ;
       else
         done1 = True;
     end
   Integer i0 = (result1);
   
   Vector#(7, Bit#(1)) v2 = unpack(res[20:14]);                        //counting MSB 0's of CLA result,2nd segment
   Integer result2=0;                                   
   Bool done2 = False;
   for( Integer p2 = 6; p2 >=0; p2 = p2 - 1) 
     begin
       if ((v2[p2] == 0)&&(!done2))  result2 = result2+1 ;
       else
         done2 = True;
     end
   Integer i1 = result2;
   
   Vector#(7, Bit#(1)) v3 = unpack(res[13:7]);                        //counting MSB 0's of CLA result,3rd segment
   Integer result3=0;                                   
   Bool done3 = False;
   for( Integer p3 = 6; p3 >=0; p3 = p3 - 1) 
     begin
       if ((v3[p3] == 0)&&(!done3))  result3 = result3+1 ;
       else
         done3 = True;
     end
   Integer i2 = result3;
   
   Vector#(7, Bit#(1)) v4 = unpack(res[6:0]);                        //counting MSB 0's of CLA result,4th segment
   Integer result4=0;                                   
   Bool done4 = False;
   for( Integer p4 = 6; p4 >=0; p4 = p4 - 1) 
     begin
       if ((v4[p4] == 0)&&(!done4))  result4 = result4+1 ;
       else
         done4 = True;
     end
   Integer i3 = result4;
   
   
   // final number of MSB zero from the 4 segments of CLA result
   Integer zeros = (|res[27:21]==1)?i0:((|res[20:14]==1)?(i1+7):((|res[13:7]==1)?(i2+14):((|res[6:0]==1)?(i3+21):28)));
   
   
   
   if ((zeros == 0) && (expo == fromInteger(254))) begin       //overflow case
      expn = 8'hFE;
      sfdres = '1;
      guard = '1;
      ex[2] = 1'b1;         
      ex[0] = 1'b1;
   end
   else begin
      if (zeros == 0) begin          // carry, no sfd adjust necessary
	 if (expn == 0)
	    expn = 2;
	 else if (expn==8'hFE)
	     begin  
             ex[2] = 1'b1;         
             ex[0] = 1'b1;
	     end	     
	 else
	    expn = expn + 1;    // carry bit
	 res = res << 1;
      end
      else if (zeros == 1) begin     // already normalized
	 if (expn == 0)
	    expn = 1;       	 // carry, hidden bits
	 res = res << 2;
      end
      else if (zeros == fromInteger(28)) begin   // exactly zero
	 expn = 0;
      end
      else begin     // try to normalize
	 Int#(9) shift = fromInteger(zeros - 1);
	 Int#(9) maxshift = expo - 9'b110000010;
         if (shift > maxshift) begin            // result will be subnormal
	    res = res << maxshift;
	    expn = 0;
	 end
	 else begin                            // result will be normal
	    res = res << shift;
	    expn = expn - truncate(pack(shift));
	 end                    
	 res = res << 2;         // carry, hidden bits
      end

      sfdres = res[27:5];               // final mantissa after normalise
      guard[2] = res[4];                //guard bit
      guard[1] = res[3];                // round bit
      guard[0] = |res[2:0];             // sticky bit
   end
   if ((expn == 0) && (guard != 0)) begin           //underflow case
      ex[1] = 1'b1;
      ex[0] = 1'b1; end                  
   
   out = FloatingPoint{sign:unpack(sgn),exp:expn,sfd:sfdres};
   
   
   return tuple3(out,unpack(ex),guard);
endfunction


//single precision rounding function
function Tuple2#(FloatingPoint#(8,23),Exception) fn_round_sp( RoundMode rmode, FloatingPoint#(8,23) din, Bit#(3) guards ,Bit#(1) spl_flag,FloatingPoint#(8,23) spl_res,Bit#(5) exc,Bit#(1) sub);

   FloatingPoint#(8,23) out = defaultValue;
   Exception ex = defaultValue;
   let mant_out = din.sfd;
   let local_expo = din.exp;
   let guard = guards[2];
   let round = guards[1];
   let sticky = guards[0];
   let inexact = guard|round|sticky;
   Bit#(1) lv_roundup = 0;
   Bit#(25) lv_man = {2'b0,mant_out};
   case(rmode) 
     Rnd_Nearest_Even: begin lv_roundup = guard & (mant_out[0] | round | sticky); end        //rnear_even
     Rnd_Nearest_Away_Zero: lv_roundup = guard; //& (round | sticky | ~sign);                //rnear_away_zero
     Rnd_Plus_Inf: lv_roundup = (guard | round | sticky) & (~pack(din.sign));                 //rmax
     Rnd_Minus_Inf: lv_roundup = (guard | round | sticky) & pack(din.sign);                  //rmin
     default: lv_roundup = 0;
   endcase
   if(lv_roundup == 1)
     lv_man = lv_man + 1;
   let out_exp1 = ((lv_man[23] == 1)&&(local_expo<8'hFE))?(local_expo + 1):local_expo;
   let out_exp = ((local_expo==8'hFE)&&((lv_man[23]|lv_man[24])==1))?8'hFF:out_exp1;
   let out_sfd = ((local_expo==8'hFE)&&((lv_man[23]|lv_man[24])==1))?23'd0:lv_man[22:0];
   
   Bit#(8) final_exp;
   Bit#(23) final_sfd;
   Bit#(1) out_inf=pack((&out_exp==1'b1)&&(|out_sfd==1'b0));
   Bit#(1) out_z=pack((|out_exp==1'b0)&&(|out_sfd==1'b0));
   Bit#(1) out_snan=pack((&out_exp==1'b1)&&(out_sfd[22]==1'b0)&&(|out_sfd[21:0]==1'b1));
   Bit#(1) out_qnan=pack((&out_exp==1'b1)&&(out_sfd[22]==1'b1));
   
   
  
   Bool final_sign = ((out_z==1) && (inexact==0) && (sub==1))?(rmode == Rnd_Minus_Inf):din.sign;
   final_exp = (out_z==1)?8'd0:((out_inf==1)?8'hFF:((out_qnan==1)?8'hFF:((out_snan==1)?8'hFF:out_exp)));
   final_sfd = (out_z==1)?23'd0:((out_inf==1)?23'd0:((out_qnan==1)?{3'b100,20'd0}:((out_snan==1)?{3'b100,20'd0}:out_sfd)));
   let exc1 = ((local_expo==8'hFE)&&((lv_man[23]|lv_man[24])==1))?(exc|5'b00101):exc;
   out = (spl_flag == 0) ? FloatingPoint{sign:final_sign , exp : final_exp , sfd : final_sfd}:FloatingPoint{sign:spl_res.sign , exp : spl_res.exp , sfd : spl_res.sfd};
   
   ex = ((out.exp =='1))?((final_sfd!=0)?unpack(exc1&{1'b1,4'd0}):((spl_flag!=0)?unpack(exc1&{1'b1,4'd0}):unpack(exc1))):unpack(exc1|{4'd0,inexact});
   
   Bit#(1) sp_max = pack(((rmode==Rnd_Plus_Inf)&&(final_sign==True))||((rmode==Rnd_Minus_Inf)&&(final_sign==False)));
   
   
   FloatingPoint#(8,23) rounded = ((out.exp ==8'hFF)&&(out.sfd!=0))?FloatingPoint{sign:False , exp:8'hFF , sfd: truncate(24'h400000)}:(((exc[2]&exc[0])==1'b1)?(((rmode!=Rnd_Zero))?((sp_max==1)?FloatingPoint{sign:final_sign , exp:8'hFE , sfd: '1}:FloatingPoint{sign:final_sign , exp:8'hFF , sfd: '0}):FloatingPoint{sign:final_sign , exp:8'hFE , sfd: '1}):out);
   
   
   return tuple2(rounded,ex);
endfunction





//Single Precision Carry-LookAhead Adder Function
function Bit#(28) fn_CLA_sp(Bit#(1) sub,Bit#(28) sfdX,Bit#(28) sfdY1);
  
   Bit#(1) cin = (sub == 0)?0:1;                        //subtraction part
   Bit#(28) sfdY = (sub==0)?sfdY1:~(sfdY1);        
  
   Bit#(28) gen_carry = sfdX&sfdY;              //carry generation
   Bit#(28) prop_carry = sfdX|sfdY;             //carry propagation
   
   Vector#(28, Bit#(1)) gen_c = unpack(gen_carry);
   Vector#(28, Bit#(1)) prop_c = unpack(prop_carry);
   
   
   Vector#(8, Bit#(1)) act_c1 ;
   Vector#(8, Bit#(1)) act_c2_1 ;
   Vector#(8, Bit#(1)) act_c3_1 ;
   Vector#(8, Bit#(1)) act_c4_1 ;
   
   
   Vector#(8, Bit#(1)) act_c2_0 ;
   Vector#(8, Bit#(1)) act_c3_0 ;
   Vector#(8, Bit#(1)) act_c4_0 ;
   
   
   act_c1[0] = cin;                               // carry generation,1st segment
   // carry vector generation
   for (Integer k=1;k<8;k=k+1) begin
   act_c1[k] = gen_c[k-1] | (prop_c[k-1]&act_c1[k-1]);
   end
   Bit#(8) carry1 = pack(act_c1);
  
  
   act_c2_1[0] = 1'b1;                               // carry generation for c_in=1,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_1[k] = gen_c[k+6] | (prop_c[k+6]&act_c2_1[k-1]);
   end
   Bit#(8) carry2_1 = pack(act_c2_1);
  
   act_c2_0[0] = 1'b0;                               // carry generation for c_in=0,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_0[k] = gen_c[k+6] | (prop_c[k+6]&act_c2_0[k-1]);
   end
   Bit#(8) carry2_0 = pack(act_c2_0);
  
  
   act_c3_0[0] = 1'b0;                               // carry generation for c_in=0,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_0[k] = gen_c[k+13] | (prop_c[k+13]&act_c3_0[k-1]);
   end
   Bit#(8) carry3_0 = pack(act_c3_0);
  
   act_c3_1[0] = 1'b1;                               // carry generation for c_in=1,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_1[k] = gen_c[k+13] | (prop_c[k+13]&act_c3_1[k-1]);
   end
   Bit#(8) carry3_1 = pack(act_c3_1);
  
  
   act_c4_1[0] = 1'b1;                               // carry generation for c_in=1,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_1[k] = gen_c[k+20] | (prop_c[k+20]&act_c4_1[k-1]);
   end
   Bit#(8) carry4_1 = pack(act_c4_1);
  
   act_c4_0[0] = 1'b0;                               // carry generation for c_in=0,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_0[k] = gen_c[k+20] | (prop_c[k+20]&act_c4_0[k-1]);
   end
   Bit#(8) carry4_0 = pack(act_c4_0);
   
   
   
   
   //choosing the carry segments according to actual c_in
   Bit#(8) carry2 = (carry1[7]==1'b1)?carry2_1:carry2_0;
   Bit#(8) carry3 = (carry2[7]==1'b1)?carry3_1:carry3_0;
   Bit#(8) carry4 = (carry3[7]==1'b1)?carry4_1:carry4_0;
   
   
   //generating actual carry vector
   Bit#(28) carry = {carry4[6:0],carry3[6:0],carry2[6:0],carry1[6:0]}; 
    
   //Actual sum generation
   Bit#(28) sum = sfdX^sfdY^carry;
   
   return sum;
endfunction







//Double Precision Carry-LookAhead Adder Function
function Bit#(57) fn_CLA_dp(Bit#(1) sub,Bit#(57) sfdX,Bit#(57) sfdY1);
  
   Bit#(1) cin = (sub == 0)?0:1;                        //subtraction part
   Bit#(57) sfdY = (sub==0)?sfdY1:~(sfdY1);        
  
   Bit#(57) gen_carry = sfdX&sfdY;              //carry generation
   Bit#(57) prop_carry = sfdX|sfdY;             //carry propagation
   
   Vector#(57, Bit#(1)) gen_c = unpack(gen_carry);
   Vector#(57, Bit#(1)) prop_c = unpack(prop_carry);
   
   
   Vector#(9, Bit#(1)) act_c1 ;
   Vector#(8, Bit#(1)) act_c2_1 ;
   Vector#(8, Bit#(1)) act_c3_1 ;
   Vector#(8, Bit#(1)) act_c4_1 ;
   Vector#(8, Bit#(1)) act_c5_1 ;
   Vector#(8, Bit#(1)) act_c6_1 ;
   Vector#(8, Bit#(1)) act_c7_1 ;
   Vector#(8, Bit#(1)) act_c8_1 ;
   
   Vector#(8, Bit#(1)) act_c2_0 ;
   Vector#(8, Bit#(1)) act_c3_0 ;
   Vector#(8, Bit#(1)) act_c4_0 ;
   Vector#(8, Bit#(1)) act_c5_0 ;
   Vector#(8, Bit#(1)) act_c6_0 ;
   Vector#(8, Bit#(1)) act_c7_0 ;
   Vector#(8, Bit#(1)) act_c8_0 ;
   
   act_c1[0] = cin;                               // carry generation,1st segment
   // carry vector generation
   for (Integer k=1;k<9;k=k+1) begin
   act_c1[k] = gen_c[k-1] | (prop_c[k-1]&act_c1[k-1]);
   end
   Bit#(9) carry1 = pack(act_c1);
  
  
   act_c2_1[0] = 1'b1;                               // carry generation for c_in=1,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_1[k] = gen_c[k+7] | (prop_c[k+7]&act_c2_1[k-1]);
   end
   Bit#(8) carry2_1 = pack(act_c2_1);
  
   act_c2_0[0] = 1'b0;                               // carry generation for c_in=0,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_0[k] = gen_c[k+7] | (prop_c[k+7]&act_c2_0[k-1]);
   end
   Bit#(8) carry2_0 = pack(act_c2_0);
  
  
   act_c3_0[0] = 1'b0;                               // carry generation for c_in=0,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_0[k] = gen_c[k+14] | (prop_c[k+14]&act_c3_0[k-1]);
   end
   Bit#(8) carry3_0 = pack(act_c3_0);
  
   act_c3_1[0] = 1'b1;                               // carry generation for c_in=1,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_1[k] = gen_c[k+14] | (prop_c[k+14]&act_c3_1[k-1]);
   end
   Bit#(8) carry3_1 = pack(act_c3_1);
  
  
   act_c4_1[0] = 1'b1;                               // carry generation for c_in=1,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_1[k] = gen_c[k+21] | (prop_c[k+21]&act_c4_1[k-1]);
   end
   Bit#(8) carry4_1 = pack(act_c4_1);
  
   act_c4_0[0] = 1'b0;                               // carry generation for c_in=0,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_0[k] = gen_c[k+21] | (prop_c[k+21]&act_c4_0[k-1]);
   end
   Bit#(8) carry4_0 = pack(act_c4_0);
   
   
   act_c5_1[0] = 1'b1;                               // carry generation for c_in=1,5th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c5_1[k] = gen_c[k+28] | (prop_c[k+28]&act_c5_1[k-1]);
   end
   Bit#(8) carry5_1 = pack(act_c5_1);
  
   act_c5_0[0] = 1'b0;                               // carry generation for c_in=0,5th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c5_0[k] = gen_c[k+28] | (prop_c[k+28]&act_c5_0[k-1]);
   end
   Bit#(8) carry5_0 = pack(act_c5_0);
   
   
   act_c6_1[0] = 1'b1;                               // carry generation for c_in=1,6th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c6_1[k] = gen_c[k+35] | (prop_c[k+35]&act_c6_1[k-1]);
   end
   Bit#(8) carry6_1 = pack(act_c6_1);
  
   act_c6_0[0] = 1'b0;                               // carry generation for c_in=0,6th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c6_0[k] = gen_c[k+35] | (prop_c[k+35]&act_c6_0[k-1]);
   end
   Bit#(8) carry6_0 = pack(act_c6_0);
   
   act_c7_1[0] = 1'b1;                               // carry generation for c_in=1,7th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c7_1[k] = gen_c[k+42] | (prop_c[k+42]&act_c7_1[k-1]);
   end
   Bit#(8) carry7_1 = pack(act_c7_1);
  
   act_c7_0[0] = 1'b0;                               // carry generation for c_in=0,7th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c7_0[k] = gen_c[k+42] | (prop_c[k+42]&act_c7_0[k-1]);
   end
   Bit#(8) carry7_0 = pack(act_c7_0);
   
   act_c8_1[0] = 1'b1;                               // carry generation for c_in=1,8th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c8_1[k] = gen_c[k+49] | (prop_c[k+49]&act_c8_1[k-1]);
   end
   Bit#(8) carry8_1 = pack(act_c8_1);
  
   act_c8_0[0] = 1'b0;                               // carry generation for c_in=0,8th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c8_0[k] = gen_c[k+49] | (prop_c[k+49]&act_c8_0[k-1]);
   end
   Bit#(8) carry8_0 = pack(act_c8_0);
   
   //choosing the carry segments according to actual c_in
   Bit#(8) carry2 = (carry1[8]==1'b1)?carry2_1:carry2_0;
   Bit#(8) carry3 = (carry2[7]==1'b1)?carry3_1:carry3_0;
   Bit#(8) carry4 = (carry3[7]==1'b1)?carry4_1:carry4_0;
   Bit#(8) carry5 = (carry4[7]==1'b1)?carry5_1:carry5_0;
   Bit#(8) carry6 = (carry5[7]==1'b1)?carry6_1:carry6_0;
   Bit#(8) carry7 = (carry6[7]==1'b1)?carry7_1:carry7_0;
   Bit#(8) carry8 = (carry7[7]==1'b1)?carry8_1:carry8_0;
   
   //generating actual carry vector
   Bit#(57) carry = {carry8[6:0],carry7[6:0],carry6[6:0],carry5[6:0],carry4[6:0],carry3[6:0],carry2[6:0],carry1[7:0]}; 
    
   //Actual sum generation
   Bit#(57) sum = sfdX^sfdY^carry;
   
   return sum;
endfunction







/*

//double-precesion addition-normalise

function Tuple3#(FloatingPoint#(11,52),Exception,Bit#(3)) fn_add_norm_dp(Bit#(1) sgn,Bit#(11) expn,Bit#(1) sub,Bit#(57) sfdX,Bit#(57) sfdY1);

   Bit#(1) cin = (sub == 0)?0:1;                        //subtraction part
   Bit#(57) sfdY = (sub==0)?sfdY1:~(sfdY1);        
  
   Bit#(57) gen_carry = sfdX&sfdY;              //carry generation
   Bit#(57) prop_carry = sfdX|sfdY;             //carry propagation
   
   Vector#(57, Bit#(1)) gen_c = unpack(gen_carry);
   Vector#(57, Bit#(1)) prop_c = unpack(prop_carry);
   
   
   Vector#(9, Bit#(1)) act_c1 ;
   Vector#(8, Bit#(1)) act_c2_1 ;
   Vector#(8, Bit#(1)) act_c3_1 ;
   Vector#(8, Bit#(1)) act_c4_1 ;
   Vector#(8, Bit#(1)) act_c5_1 ;
   Vector#(8, Bit#(1)) act_c6_1 ;
   Vector#(8, Bit#(1)) act_c7_1 ;
   Vector#(8, Bit#(1)) act_c8_1 ;
   
   Vector#(8, Bit#(1)) act_c2_0 ;
   Vector#(8, Bit#(1)) act_c3_0 ;
   Vector#(8, Bit#(1)) act_c4_0 ;
   Vector#(8, Bit#(1)) act_c5_0 ;
   Vector#(8, Bit#(1)) act_c6_0 ;
   Vector#(8, Bit#(1)) act_c7_0 ;
   Vector#(8, Bit#(1)) act_c8_0 ;
   
   act_c1[0] = cin;                               // carry generation,1st segment
   // carry vector generation
   for (Integer k=1;k<9;k=k+1) begin
   act_c1[k] = gen_c[k-1] | (prop_c[k-1]&act_c1[k-1]);
   end
   Bit#(9) carry1 = pack(act_c1);
  
  
   act_c2_1[0] = 1'b1;                               // carry generation for c_in=1,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_1[k] = gen_c[k+7] | (prop_c[k+7]&act_c2_1[k-1]);
   end
   Bit#(8) carry2_1 = pack(act_c2_1);
  
   act_c2_0[0] = 1'b0;                               // carry generation for c_in=0,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_0[k] = gen_c[k+7] | (prop_c[k+7]&act_c2_0[k-1]);
   end
   Bit#(8) carry2_0 = pack(act_c2_0);
  
  
   act_c3_0[0] = 1'b0;                               // carry generation for c_in=0,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_0[k] = gen_c[k+14] | (prop_c[k+14]&act_c3_0[k-1]);
   end
   Bit#(8) carry3_0 = pack(act_c3_0);
  
   act_c3_1[0] = 1'b1;                               // carry generation for c_in=1,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_1[k] = gen_c[k+14] | (prop_c[k+14]&act_c3_1[k-1]);
   end
   Bit#(8) carry3_1 = pack(act_c3_1);
  
  
   act_c4_1[0] = 1'b1;                               // carry generation for c_in=1,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_1[k] = gen_c[k+21] | (prop_c[k+21]&act_c4_1[k-1]);
   end
   Bit#(8) carry4_1 = pack(act_c4_1);
  
   act_c4_0[0] = 1'b0;                               // carry generation for c_in=0,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_0[k] = gen_c[k+21] | (prop_c[k+21]&act_c4_0[k-1]);
   end
   Bit#(8) carry4_0 = pack(act_c4_0);
   
   
   act_c5_1[0] = 1'b1;                               // carry generation for c_in=1,5th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c5_1[k] = gen_c[k+28] | (prop_c[k+28]&act_c5_1[k-1]);
   end
   Bit#(8) carry5_1 = pack(act_c5_1);
  
   act_c5_0[0] = 1'b0;                               // carry generation for c_in=0,5th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c5_0[k] = gen_c[k+28] | (prop_c[k+28]&act_c5_0[k-1]);
   end
   Bit#(8) carry5_0 = pack(act_c5_0);
   
   
   act_c6_1[0] = 1'b1;                               // carry generation for c_in=1,6th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c6_1[k] = gen_c[k+35] | (prop_c[k+35]&act_c6_1[k-1]);
   end
   Bit#(8) carry6_1 = pack(act_c6_1);
  
   act_c6_0[0] = 1'b0;                               // carry generation for c_in=0,6th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c6_0[k] = gen_c[k+35] | (prop_c[k+35]&act_c6_0[k-1]);
   end
   Bit#(8) carry6_0 = pack(act_c6_0);
   
   act_c7_1[0] = 1'b1;                               // carry generation for c_in=1,7th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c7_1[k] = gen_c[k+42] | (prop_c[k+42]&act_c7_1[k-1]);
   end
   Bit#(8) carry7_1 = pack(act_c7_1);
  
   act_c7_0[0] = 1'b0;                               // carry generation for c_in=0,7th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c7_0[k] = gen_c[k+42] | (prop_c[k+42]&act_c7_0[k-1]);
   end
   Bit#(8) carry7_0 = pack(act_c7_0);
   
   act_c8_1[0] = 1'b1;                               // carry generation for c_in=1,8th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c8_1[k] = gen_c[k+49] | (prop_c[k+49]&act_c8_1[k-1]);
   end
   Bit#(8) carry8_1 = pack(act_c8_1);
  
   act_c8_0[0] = 1'b0;                               // carry generation for c_in=0,8th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c8_0[k] = gen_c[k+49] | (prop_c[k+49]&act_c8_0[k-1]);
   end
   Bit#(8) carry8_0 = pack(act_c8_0);
   
   //choosing the carry segments according to actual c_in
   Bit#(8) carry2 = (carry1[8]==1'b1)?carry2_1:carry2_0;
   Bit#(8) carry3 = (carry2[7]==1'b1)?carry3_1:carry3_0;
   Bit#(8) carry4 = (carry3[7]==1'b1)?carry4_1:carry4_0;
   Bit#(8) carry5 = (carry4[7]==1'b1)?carry5_1:carry5_0;
   Bit#(8) carry6 = (carry5[7]==1'b1)?carry6_1:carry6_0;
   Bit#(8) carry7 = (carry6[7]==1'b1)?carry7_1:carry7_0;
   Bit#(8) carry8 = (carry7[7]==1'b1)?carry8_1:carry8_0;
   
   //generating actual carry vector
   Bit#(57) carry = {carry8[6:0],carry7[6:0],carry6[6:0],carry5[6:0],carry4[6:0],carry3[6:0],carry2[6:0],carry1[7:0]}; 
    
   //Actual sum generation
   Bit#(57) res = sfdX^sfdY^carry;

    Int#(12) expo = (|expn==0)?unpack(12'b110000000010):unpack(zeroExtend(expn)-12'd1023);    //exponent evaluation
   Bit#(3) guard = 0;
   Bit#(5) ex = 0;
   Bit#(52) sfdres=0;
   
   FloatingPoint#(11,52) out = defaultValue;
   
   
   
   
   Vector#(15, Bit#(1)) v1 = unpack(res[56:42]);                        //counting MSB 0's of CLA result,1st segment
   Integer result1=0;                                  
   Bool done1 = False;
   for( Integer p1 = 14; p1 >=0; p1 = p1 - 1) 
     begin
       if ((v1[p1] == 0)&&(!done1))  result1 = result1+1 ;
       else
         done1 = True;
     end
   Integer i0 = (result1);
   
   Vector#(14, Bit#(1)) v2 = unpack(res[41:28]);                        //counting MSB 0's of CLA result,2nd segment
   Integer result2=0;                                   
   Bool done2 = False;
   for( Integer p2 = 13; p2 >=0; p2 = p2 - 1) 
     begin
       if ((v2[p2] == 0)&&(!done2))  result2 = result2+1 ;
       else
         done2 = True;
     end
   Integer i1 = result2;
   
   Vector#(14, Bit#(1)) v3 = unpack(res[27:14]);                        //counting MSB 0's of CLA result,3rd segment
   Integer result3=0;                                   
   Bool done3 = False;
   for( Integer p3 = 13; p3 >=0; p3 = p3 - 1) 
     begin
       if ((v3[p3] == 0)&&(!done3))  result3 = result3+1 ;
       else
         done3 = True;
     end
   Integer i2 = result3;
   
   Vector#(14, Bit#(1)) v4 = unpack(res[13:0]);                        //counting MSB 0's of CLA result,4th segment
   Integer result4=0;                                   
   Bool done4 = False;
   for( Integer p4 = 13; p4 >=0; p4 = p4 - 1) 
     begin
       if ((v4[p4] == 0)&&(!done4))  result4 = result4+1 ;
       else
         done4 = True;
     end
   Integer i3 = result4;
   
   // Obtaining actual MSB zero count from 4 segments
   Integer zeros = (|res[56:42]==1)?i0:((|res[41:28]==1)?(i1+15):((|res[27:14]==1)?(i2+29):((|res[13:0]==1)?(i3+43):57)));
   
   
   
   if ((zeros == 0) && (expo == fromInteger(2046))) begin        //overflow case
      expn = {3'b111,8'hFE};
      sfdres = '1;
      guard = '1;
      ex[2] = 1'b1;         
      ex[0] = 1'b1;
   end
   else begin
      if (zeros == 0) begin          // carry, no sfd adjust necessary
	 if (expn == 0)
	    expn = 2;
	 else if (expn==11'd2046)
	     begin  
             ex[2] = 1'b1;         
             ex[0] = 1'b1;
	     end	     
	 else
	    expn = expn + 1;    // carry bit
	 res = res << 1;
      end
      else if (zeros == 1) begin     // already normalized
	 if (expn == 0)
	    expn = 1;       	 // carry, hidden bits
	 res = res << 2;
      end
      else if (zeros == fromInteger(57)) begin   // exactly zero
	 expn = 0;
      end
      else begin     // try to normalize
	 Int#(12) shift = fromInteger(zeros - 1);
	 Int#(12) maxshift = expo - 12'b110000000010;
         if (shift > maxshift) begin            // result will be subnormal
	    res = res << maxshift;
	    expn = 0;
	 end
	 else begin                            // result will be normal
	    res = res << shift;
	    expn = expn - truncate(pack(shift));
	 end                    
	 res = res << 2;         // carry, hidden bits
      end

      sfdres = res[56:5];
      guard[2] = res[4];
      guard[1] = res[3];
      guard[0] = |res[2:0];
   end
   if ((expn == 0) && (guard != 0)) begin     //underflow case
      ex[1] = 1'b1;
      ex[0] = 1'b1; end
   
   out = FloatingPoint{sign:unpack(sgn),exp:expn,sfd:sfdres};
   
   return tuple3(out,unpack(ex),guard);
endfunction

*/




//Double Precision Normalise Function
function Tuple3#(FloatingPoint#(11,52),Exception,Bit#(3)) fn_norm_dp(Bit#(1) sgn,Bit#(11) expn,Bit#(57) res);

   
//   Bit#(28) res = (sub == 0)?(sfdX+sfdY):(sfdX-sfdY);
   
   Int#(12) expo = (|expn==0)?unpack(12'b110000000010):unpack(zeroExtend(expn)-12'd1023);    //exponent evaluation
   Bit#(3) guard = 0;
   Bit#(5) ex = 0;
   Bit#(52) sfdres=0;
   
   FloatingPoint#(11,52) out = defaultValue;
   
   
   
   
   Vector#(15, Bit#(1)) v1 = unpack(res[56:42]);                        //counting MSB 0's of CLA result,1st segment
   Integer result1=0;                                  
   Bool done1 = False;
   for( Integer p1 = 14; p1 >=0; p1 = p1 - 1) 
     begin
       if ((v1[p1] == 0)&&(!done1))  result1 = result1+1 ;
       else
         done1 = True;
     end
   Integer i0 = (result1);
   
   Vector#(14, Bit#(1)) v2 = unpack(res[41:28]);                        //counting MSB 0's of CLA result,2nd segment
   Integer result2=0;                                   
   Bool done2 = False;
   for( Integer p2 = 13; p2 >=0; p2 = p2 - 1) 
     begin
       if ((v2[p2] == 0)&&(!done2))  result2 = result2+1 ;
       else
         done2 = True;
     end
   Integer i1 = result2;
   
   Vector#(14, Bit#(1)) v3 = unpack(res[27:14]);                        //counting MSB 0's of CLA result,3rd segment
   Integer result3=0;                                   
   Bool done3 = False;
   for( Integer p3 = 13; p3 >=0; p3 = p3 - 1) 
     begin
       if ((v3[p3] == 0)&&(!done3))  result3 = result3+1 ;
       else
         done3 = True;
     end
   Integer i2 = result3;
   
   Vector#(14, Bit#(1)) v4 = unpack(res[13:0]);                        //counting MSB 0's of CLA result,4th segment
   Integer result4=0;                                   
   Bool done4 = False;
   for( Integer p4 = 13; p4 >=0; p4 = p4 - 1) 
     begin
       if ((v4[p4] == 0)&&(!done4))  result4 = result4+1 ;
       else
         done4 = True;
     end
   Integer i3 = result4;
   
   // Obtaining actual MSB zero count from 4 segments
   Integer zeros = (|res[56:42]==1)?i0:((|res[41:28]==1)?(i1+15):((|res[27:14]==1)?(i2+29):((|res[13:0]==1)?(i3+43):57)));
   
   
   
   if ((zeros == 0) && (expo == fromInteger(2046))) begin        //overflow case
      expn = {3'b111,8'hFE};
      sfdres = '1;
      guard = '1;
      ex[2] = 1'b1;         
      ex[0] = 1'b1;
   end
   else begin
      if (zeros == 0) begin          // carry, no sfd adjust necessary
	 if (expn == 0)
	    expn = 2;
	 else if (expn==11'd2046)
	     begin  
             ex[2] = 1'b1;         
             ex[0] = 1'b1;
	     end	     
	 else
	    expn = expn + 1;    // carry bit
	 res = res << 1;
      end
      else if (zeros == 1) begin     // already normalized
	 if (expn == 0)
	    expn = 1;       	 // carry, hidden bits
	 res = res << 2;
      end
      else if (zeros == fromInteger(57)) begin   // exactly zero
	 expn = 0;
      end
      else begin     // try to normalize
	 Int#(12) shift = fromInteger(zeros - 1);
	 Int#(12) maxshift = expo - 12'b110000000010;
         if (shift > maxshift) begin            // result will be subnormal
	    res = res << maxshift;
	    expn = 0;
	 end
	 else begin                            // result will be normal
	    res = res << shift;
	    expn = expn - truncate(pack(shift));
	 end                    
	 res = res << 2;         // carry, hidden bits
      end

      sfdres = res[56:5];
      guard[2] = res[4];
      guard[1] = res[3];
      guard[0] = |res[2:0];
   end
   if ((expn == 0) && (guard != 0)) begin     //underflow case
      ex[1] = 1'b1;
      ex[0] = 1'b1; end
   
   out = FloatingPoint{sign:unpack(sgn),exp:expn,sfd:sfdres};
   
   
   return tuple3(out,unpack(ex),guard);
endfunction











//Double precision rounding function
function Tuple2#(FloatingPoint#(11,52),Exception) fn_round_dp( RoundMode rmode, FloatingPoint#(11,52) din, Bit#(3) guards ,Bit#(1) spl_flag,FloatingPoint#(11,52) spl_res,Bit#(5) exc,Bit#(1) sub);
// spl_flag = {b_snan,a_snan,b_qnan,a_qnan,b_inf,a_inf,b_z,a_z}
   FloatingPoint#(11,52) out = defaultValue;
   Exception ex = defaultValue;
   let mant_out = din.sfd;
   let local_expo = din.exp;
   let guard = guards[2];
   let round = guards[1];
   let sticky = guards[0];
   let inexact = guard|round|sticky;
   Bit#(1) lv_roundup = 0;
   Bit#(54) lv_man = {2'b0,mant_out};
   case(rmode) 
     Rnd_Nearest_Even: begin lv_roundup = guard & (mant_out[0] | round | sticky); end        //rnear_even
     Rnd_Nearest_Away_Zero: lv_roundup = guard; //& (round | sticky | ~sign);                //rnear_away_zero
     Rnd_Plus_Inf: lv_roundup = (guard | round | sticky) & (~pack(din.sign));                 //rmax
     Rnd_Minus_Inf: lv_roundup = (guard | round | sticky) & pack(din.sign);                  //rmin
     default: lv_roundup = 0;
   endcase
   if(lv_roundup == 1)
     lv_man = lv_man + 1;
   let out_exp1 = ((lv_man[52] == 1)&&(local_expo<11'd2046))?(local_expo + 1):local_expo;
   let out_exp = ((local_expo==11'd2046)&&((lv_man[52]|lv_man[53])==1))?11'd2047:out_exp1;
   let out_sfd = ((local_expo==11'd2046)&&((lv_man[52]|lv_man[53])==1))?52'd0:lv_man[51:0];
   
   Bit#(11) final_exp;
   Bit#(52) final_sfd;
   Bit#(1) out_inf=pack((&out_exp==1'b1)&&(|out_sfd==1'b0));
   Bit#(1) out_z=pack((|out_exp==1'b0)&&(|out_sfd==1'b0));
   Bit#(1) out_snan=pack((&out_exp==1'b1)&&(out_sfd[51]==1'b0)&&(|out_sfd[50:0]==1'b1));
   Bit#(1) out_qnan=pack((&out_exp==1'b1)&&(out_sfd[51]==1'b1));
   
   
  
   Bool final_sign = ((out_z==1) && (inexact==0) && (sub==1))?(rmode == Rnd_Minus_Inf):din.sign;
   final_exp = (out_z==1)?11'd0:((out_inf==1)?11'd2047:((out_qnan==1)?{3'b111,8'hFF}:((out_snan==1)?{3'b111,8'hFF}:out_exp)));
   final_sfd = (out_z==1)?52'd0:((out_inf==1)?52'd0:((out_qnan==1)?{3'b100,49'd0}:((out_snan==1)?{3'b100,49'd0}:out_sfd)));
   let exc1 = ((local_expo==11'd2046)&&((lv_man[52]|lv_man[53])==1))?(exc|5'b00101):exc;
   out = (spl_flag == 0) ? FloatingPoint{sign:final_sign , exp : final_exp , sfd : final_sfd}:FloatingPoint{sign:spl_res.sign , exp : spl_res.exp , sfd : spl_res.sfd};
   
   ex = ((out.exp =='1))?((final_sfd!=0)?unpack(exc1&{1'b1,4'd0}):((spl_flag!=0)?unpack(exc1&{1'b1,4'd0}):unpack(exc1))):unpack(exc1|{4'd0,inexact});
   
   Bit#(1) sp_max = pack(((rmode==Rnd_Plus_Inf)&&(final_sign==True))||((rmode==Rnd_Minus_Inf)&&(final_sign==False)));
   
   
   FloatingPoint#(11,52) rounded = ((out.exp ==11'd2047)&&(out.sfd!=0))?FloatingPoint{sign:False , exp:{3'b111,8'hFF} , sfd: truncate(52'h8000000000000)}:(((exc[2]&exc[0])==1'b1)?(((rmode!=Rnd_Zero))?((sp_max==1)?FloatingPoint{sign:final_sign , exp:{3'b111,8'hFE} , sfd: '1}:FloatingPoint{sign:final_sign , exp:{3'b111,8'hFF} , sfd: '0}):FloatingPoint{sign:final_sign , exp:{3'b111,8'hFE} , sfd: '1}):out);
   
   
   return tuple2(rounded,ex);
endfunction











//Interfaces and Modules

interface Ifc_fpu_adder_sp;                                                                    // single precision Adder Interface
 method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) data_in);
 method ReturnType#(8,23) receive();
endinterface



interface Ifc_fpu_adder_dp;                                                                    // double precision Adder Interface
 method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) data_in);
 method ReturnType#(11,52) receive();
endinterface







(*synthesize*)
module mk_fpu_adder_sp(Ifc_fpu_adder_sp);                                         // single precision Adder Module

Vector#(5,Reg#(Bit#(1))) rg_sp_valid <- replicateM(mkDReg(0));
Reg#(Bit#(28)) add_sfdX <- mkReg(0);
Reg#(Bit#(28)) add_sfdY <- mkReg(0);
Reg#(Bit#(8)) exp_stage1 <- mkReg(0);
Reg#(Bool) sign_stage1 <- mkReg(False);
Reg#(Bit#(8)) exp_stage2 <- mkReg(0);
Reg#(Bool) sign_stage2 <- mkReg(False);
Reg#(Bit#(8)) exp_stage3 <- mkReg(0);
Reg#(Bool) sign_stage3 <- mkReg(False);

Reg#(Bit#(28)) add_sfdX1 <- mkReg(0);
Reg#(Bit#(28)) add_sfdY1 <- mkReg(0);


Reg#(Bit#(1)) rg_sub0 <- mkReg(0);
Reg#(Bit#(1)) rg_sub1 <- mkReg(0);
Reg#(Bit#(1)) rg_sub2 <- mkReg(0);
Reg#(Bit#(1)) rg_sub3 <- mkReg(0);
Reg#(Bit#(1)) rg_sub4 <- mkReg(0);

Reg#(Bit#(1)) rg_spl_flag0 <- mkReg(0);
Reg#(Bit#(1)) rg_spl_flag1 <- mkReg(0);
Reg#(Bit#(1)) rg_spl_flag2 <- mkReg(0);
Reg#(Bit#(1)) rg_spl_flag3 <- mkReg(0);
Reg#(Bit#(1)) rg_spl_flag4 <- mkReg(0);

Reg#(Bit#(28)) res <- mkReg(0);
Reg#(Bit#(5)) rg_statusA <- mkReg(0);
Reg#(Bit#(5)) rg_statusB <- mkReg(0);

Reg#(Bit#(5)) rg_exc0 <- mkReg(0);
Reg#(Bit#(5)) rg_exc1 <- mkReg(0);
Reg#(Bit#(5)) rg_exc2 <- mkReg(0);
Reg#(Bit#(5)) rg_exc3 <- mkReg(0);
Reg#(Bit#(5)) rg_exc4 <- mkReg(0);
Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out_prim <- mkReg(tuple2(defaultValue,defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),Exception,Bit#(3))) rg_out_prim1 <- mkReg(tuple3(defaultValue,defaultValue,0));
Reg#(Tuple3#(FloatingPoint#(8,23),Exception,Bit#(3))) rg_out_prim2 <- mkReg(tuple3(defaultValue,defaultValue,0));
Reg#(Tuple3#(FloatingPoint#(8,23),Exception,Bit#(3))) rg_out_prim3 <- mkReg(tuple3(defaultValue,defaultValue,0));
Reg#(Tuple3#(FloatingPoint#(8,23),Exception,Bit#(3))) rg_out_prim4 <- mkReg(tuple3(defaultValue,defaultValue,0));
Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out <- mkReg(tuple2(defaultValue,defaultValue));


Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operand1 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operand2 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));


Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out1 <- mkReg(tuple2(defaultValue,defaultValue));

Reg#(FloatingPoint#(8,23)) rg_spl_res <- mkReg(defaultValue);
Reg#(FloatingPoint#(8,23)) rg_spl_res1 <- mkReg(defaultValue);
Reg#(FloatingPoint#(8,23)) rg_spl_res2 <- mkReg(defaultValue);
Reg#(FloatingPoint#(8,23)) rg_spl_res3 <- mkReg(defaultValue);
Reg#(FloatingPoint#(8,23)) rg_spl_res4 <- mkReg(defaultValue);

Reg#(RoundMode) rg_rnd0 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd1 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd2 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd3 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd4 <- mkReg(defaultValue);

Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_data_in <- mkReg(tuple3(defaultValue,defaultValue,defaultValue));
Reg#(Bit#(1)) rg_hd_bit_A <- mkReg(0);
Reg#(Bit#(1)) rg_hd_bit_B <- mkReg(0);
Reg#(Bit#(28)) rg_sfdA <- mkReg(0);
Reg#(Bit#(28)) rg_sfdB <- mkReg(0);
Reg#(Bit#(1)) rg_exp_comp <- mkReg(0);
Reg#(Bit#(1)) rg_sub_norm <- mkReg(0);

Reg#(Bit#(8)) rg_out_exp <- mkReg(0);
Reg#(Bit#(23)) rg_out_sfd <- mkReg(0);
Reg#(Bit#(25)) rg_lv_man <- mkReg(0);

Reg#(Bit#(28)) rg_res <- mkReg(0);
Reg#(Bit#(9)) rg_zeros <- mkReg(0);

rule rl_valid_check;                                //rule for valid bits
for(Integer i = 1 ; i < 5 ; i = i+1)
  begin
   rg_sp_valid[i] <= rg_sp_valid[i-1];
  end
endrule


rule rl_dummy1;                                     // dummy rule,stage 2

  match {.opA, .opB, .rmode} = rg_data_in;
  Bit#(1) hidden_bit_A = rg_hd_bit_A;
  Bit#(1) hidden_bit_B = rg_hd_bit_B;
  Bit#(28) sfdA = rg_sfdA;
  Bit#(28) sfdB = rg_sfdB;    
  Bit#(1) exp_comp = rg_exp_comp;

  Bit#(5) ex =0;
  rg_rnd0 <= rmode;                                          // rounding mode,stage 1

  Bit#(1) sub_norm = rg_sub_norm;
  Bit#(8) exp_diff_prim = (exp_comp ==1)?truncate(pack(opB.exp - opA.exp)):truncate(pack(opA.exp - opB.exp));
  Bit#(8) exp_diff1 = (((opA.exp==0)&&(opB.exp==1))||((opA.exp==1)&&(opB.exp==0)))?0:exp_diff_prim; 
  // Bit#(8) exp_diff =  (((opA.exp==0)&&(opB.exp==2))||((opA.exp==2)&&(opB.exp==0)))?1:exp_diff1;

  Bit#(8) exp_diff =  (sub_norm==1)?exp_diff_prim-1:exp_diff_prim;      // determine final exponent difference



  Bit#(28) sfdX = (exp_comp ==1)?sfdB:sfdA;
  Bit#(28) sfdY = (exp_comp ==1)?sfdA:sfdB;
  
  rg_sub0 <= (exp_comp ==1)?pack(opB.sign != opA.sign):pack(opA.sign != opB.sign);      // determination of Addition or Subtraction

  //determination of special exception bits 
  Bit#(1) a_inf=pack((&opA.exp==1'b1)&&(|opA.sfd==1'b0));
  Bit#(1) b_inf=pack((&opB.exp==1'b1)&&(|opB.sfd==1'b0));
  Bit#(1) a_z=pack((|opA.exp==1'b0)&&(|opA.sfd==1'b0));
  Bit#(1) b_z=pack((|opB.exp==1'b0)&&(|opB.sfd==1'b0));
  Bit#(1) a_snan=pack((&opA.exp==1'b1)&&(opA.sfd[22]==1'b0)&&(|opA.sfd[21:0]==1'b1));
  Bit#(1) b_snan=pack((&opB.exp==1'b1)&&(opB.sfd[22]==1'b0)&&(|opB.sfd[21:0]==1'b1));
  Bit#(1) a_qnan=pack((&opA.exp==1'b1)&&(opA.sfd[22]==1'b1));
  Bit#(1) b_qnan=pack((&opB.exp==1'b1)&&(opB.sfd[22]==1'b1));


  Bit#(8) flag = {b_snan,a_snan,b_qnan,a_qnan,b_inf,a_inf,b_z,a_z};
  //special Exception flag
  rg_spl_flag0 <= |flag;                                 
  ex[4] = ((a_snan|b_snan|(a_inf&b_inf&pack(opA.sign!=opB.sign)))==1'b1)?1'b1:1'b0;
  rg_exc0 <= ex;          //exception generation                               

  Bit#(1) a_nan = a_snan|a_qnan;
  Bit#(1) b_nan = b_snan|b_qnan;
  Bit#(1) nan = pack(((a_nan&b_inf)==1'b1)||((b_nan&a_inf)==1'b1));

  //$display("%b",{a_nan,b_nan,a_inf,b_inf,nan});
  Bool z_sign = ((a_z&b_z)==1)&&(opA.sign==opB.sign)?opA.sign:((rmode==Rnd_Minus_Inf)?True:False);

  // evaluation of special results

  if ((a_z&b_z)==1) rg_spl_res <= FloatingPoint{sign:z_sign , exp:8'd0 , sfd: 23'd0};
  else if(a_z==1) rg_spl_res <= FloatingPoint{sign:opB.sign , exp:opB.exp , sfd: opB.sfd};                  
  else if (b_z==1) rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: opA.sfd};
  else if ((a_inf&b_inf) ==1) begin
    if (opA.sign == opB.sign) 
        rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: opA.sfd};
    else
        rg_spl_res <= FloatingPoint{sign:False , exp:8'hFF , sfd: truncate(24'h400000)};     
  end 
  else if (nan==1) rg_spl_res <= FloatingPoint{sign:False , exp:8'hFF , sfd: truncate(24'h400000)};
  else if (a_inf==1) rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: opA.sfd};
  else if (b_inf==1) rg_spl_res <= FloatingPoint{sign:opB.sign , exp:opB.exp , sfd: opB.sfd};
  else if (a_qnan == 1) rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: opA.sfd};
  else if (b_qnan==1) rg_spl_res <= FloatingPoint{sign:opB.sign , exp:opB.exp , sfd: opB.sfd};
  else if (a_snan == 1) rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: (opA.sfd|truncate(24'h400000))};
  else if (b_snan == 1) rg_spl_res <= FloatingPoint{sign:opB.sign , exp:opB.exp , sfd: (opB.sfd|truncate(24'h400000))};
  else if (((a_qnan&b_qnan)==1)||((a_snan&b_snan)==1)) rg_spl_res <= FloatingPoint{sign:False , exp:8'hFF , sfd: truncate(24'h400000)};  
  else  rg_spl_res <= FloatingPoint{sign:False , exp:8'd0 , sfd: 23'd0};


  // shifting mantissa based on exponent difference   
  if (exp_diff < fromInteger(28)) begin
      Bit#(28) guard = sfdY;
      guard = sfdY << (fromInteger(28) - exp_diff);
      sfdY = sfdY >> exp_diff;
      sfdY[0] = sfdY[0] | (|guard);
  end
  else if (|sfdY == 1) begin
      sfdY = 1;
  end

  //final mantissa
  add_sfdX <= sfdX;                                                 
  add_sfdY <= sfdY;                    
  exp_stage2 <= exp_stage1;
  sign_stage2 <= sign_stage1;
endrule


rule rl_add_norm;                                  // rule for addition and normalise,stage 3
/*Tuple3#(FloatingPoint#(8,23),Exception,Bit#(3)) x=fn_add_norm_sp(pack(sign_stage2),pack(rg_sub0),pack(exp_stage2),pack(add_sfdX),pack(add_sfdY));
rg_out_prim2<= x;
rg_exc2 <= rg_exc0;          //exception generation
rg_rnd2 <= rg_rnd0;
rg_sub2 <= rg_sub0;
rg_spl_res2 <= rg_spl_res;
rg_spl_flag2 <= rg_spl_flag0;*/

Bit#(1) sub = pack(rg_sub0);
   Bit#(28) sfdX = pack(add_sfdX);
   Bit#(28) sfdY1 = pack(add_sfdY);
   
   Bit#(1) cin = (sub == 0)?0:1;                        //subtraction part
   Bit#(28) sfdY = (sub==0)?sfdY1:~(sfdY1);        
  
   Bit#(28) gen_carry = sfdX&sfdY;              //carry generation
   Bit#(28) prop_carry = sfdX|sfdY;             //carry propagation
   
   Vector#(28, Bit#(1)) gen_c = unpack(gen_carry);
   Vector#(28, Bit#(1)) prop_c = unpack(prop_carry);
   
   
   Vector#(8, Bit#(1)) act_c1 ;
   Vector#(8, Bit#(1)) act_c2_1 ;
   Vector#(8, Bit#(1)) act_c3_1 ;
   Vector#(8, Bit#(1)) act_c4_1 ;
   
   
   Vector#(8, Bit#(1)) act_c2_0 ;
   Vector#(8, Bit#(1)) act_c3_0 ;
   Vector#(8, Bit#(1)) act_c4_0 ;
   
   
   act_c1[0] = cin;                               // carry generation,1st segment
   // carry vector generation
   for (Integer k=1;k<8;k=k+1) begin
   act_c1[k] = gen_c[k-1] | (prop_c[k-1]&act_c1[k-1]);
   end
   Bit#(8) carry1 = pack(act_c1);
  
  
   act_c2_1[0] = 1'b1;                               // carry generation for c_in=1,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_1[k] = gen_c[k+6] | (prop_c[k+6]&act_c2_1[k-1]);
   end
   Bit#(8) carry2_1 = pack(act_c2_1);
  
   act_c2_0[0] = 1'b0;                               // carry generation for c_in=0,2nd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c2_0[k] = gen_c[k+6] | (prop_c[k+6]&act_c2_0[k-1]);
   end
   Bit#(8) carry2_0 = pack(act_c2_0);
  
  
   act_c3_0[0] = 1'b0;                               // carry generation for c_in=0,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_0[k] = gen_c[k+13] | (prop_c[k+13]&act_c3_0[k-1]);
   end
   Bit#(8) carry3_0 = pack(act_c3_0);
  
   act_c3_1[0] = 1'b1;                               // carry generation for c_in=1,3rd segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c3_1[k] = gen_c[k+13] | (prop_c[k+13]&act_c3_1[k-1]);
   end
   Bit#(8) carry3_1 = pack(act_c3_1);
  
  
   act_c4_1[0] = 1'b1;                               // carry generation for c_in=1,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_1[k] = gen_c[k+20] | (prop_c[k+20]&act_c4_1[k-1]);
   end
   Bit#(8) carry4_1 = pack(act_c4_1);
  
   act_c4_0[0] = 1'b0;                               // carry generation for c_in=0,4th segment
   for (Integer k=1;k<8;k=k+1) begin
   act_c4_0[k] = gen_c[k+20] | (prop_c[k+20]&act_c4_0[k-1]);
   end
   Bit#(8) carry4_0 = pack(act_c4_0);
   
   
   
   
   //choosing the carry segments according to actual c_in
   Bit#(8) carry2 = (carry1[7]==1'b1)?carry2_1:carry2_0;
   Bit#(8) carry3 = (carry2[7]==1'b1)?carry3_1:carry3_0;
   Bit#(8) carry4 = (carry3[7]==1'b1)?carry4_1:carry4_0;
   
   
   //generating actual carry vector
   Bit#(28) carry = {carry4[6:0],carry3[6:0],carry2[6:0],carry1[6:0]}; 
    
   //Actual sum generation 
   Bit#(28) res = sfdX^sfdY^carry;         //final result of CLA Adder for mantissa bits
   
   
   Vector#(7, Bit#(1)) v1 = unpack(res[27:21]);                        //counting MSB 0's of CLA result,1st segment
   Integer result1=0;                                  
   Bool done1 = False;
   for( Integer p1 = 6; p1 >=0; p1 = p1 - 1) 
     begin
       if ((v1[p1] == 0)&&(!done1))  result1 = result1+1 ;
       else
         done1 = True;
     end
   Integer i0 = (result1);
   
   Vector#(7, Bit#(1)) v2 = unpack(res[20:14]);                        //counting MSB 0's of CLA result,2nd segment
   Integer result2=0;                                   
   Bool done2 = False;
   for( Integer p2 = 6; p2 >=0; p2 = p2 - 1) 
     begin
       if ((v2[p2] == 0)&&(!done2))  result2 = result2+1 ;
       else
         done2 = True;
     end
   Integer i1 = result2;
   
   Vector#(7, Bit#(1)) v3 = unpack(res[13:7]);                        //counting MSB 0's of CLA result,3rd segment
   Integer result3=0;                                   
   Bool done3 = False;
   for( Integer p3 = 6; p3 >=0; p3 = p3 - 1) 
     begin
       if ((v3[p3] == 0)&&(!done3))  result3 = result3+1 ;
       else
         done3 = True;
     end
   Integer i2 = result3;
   
   Vector#(7, Bit#(1)) v4 = unpack(res[6:0]);                        //counting MSB 0's of CLA result,4th segment
   Integer result4=0;                                   
   Bool done4 = False;
   for( Integer p4 = 6; p4 >=0; p4 = p4 - 1) 
     begin
       if ((v4[p4] == 0)&&(!done4))  result4 = result4+1 ;
       else
         done4 = True;
     end
   Integer i3 = result4;
   
   
   // final number of MSB zero from the 4 segments of CLA result
   Integer zeros_int = (|res[27:21]==1)?i0:((|res[20:14]==1)?(i1+7):((|res[13:7]==1)?(i2+14):((|res[6:0]==1)?(i3+21):28)));
   Int#(9) zeros = fromInteger(zeros_int);
   
rg_res <= res;
rg_zeros <= pack(zeros);
rg_exc2 <= rg_exc0;
rg_rnd2 <= rg_rnd0;
rg_sub2 <= rg_sub0;
rg_spl_res2 <= rg_spl_res;
rg_spl_flag2 <= rg_spl_flag0;
sign_stage3 <= sign_stage2;
exp_stage3 <= exp_stage2;
endrule

rule rl_dummy;
  Int#(9) zeros = unpack(rg_zeros);
   Bit#(28) res = rg_res;
   Bit#(1) sgn = pack(sign_stage3);
   Bit#(8) expn = pack(exp_stage3);
   Int#(9) expo = (|expn==0)?unpack(9'b110000010):unpack(zeroExtend(expn)-9'd127);
   Bit#(3) guards = 0;
   Bit#(5) ex = 0;
   Bit#(23) sfdres=0;
   Bit#(1) sub = pack(rg_sub2);
   
   
   FloatingPoint#(8,23) out = defaultValue;
   
   if ((zeros == 0) && (expo == fromInteger(254))) begin       //overflow case
      expn = 8'hFE;
      sfdres = '1;
      guards = '1;
      ex[2] = 1'b1;         
      ex[0] = 1'b1;
   end
   else begin
      if (zeros == 0) begin          // carry, no sfd adjust necessary
	 if (expn == 0)
	    expn = 2;
	 else if (expn==8'hFE)
	     begin  
             ex[2] = 1'b1;         
             ex[0] = 1'b1;
	     end	     
	 else
	    expn = expn + 1;    // carry bit
	 res = res << 1;
      end
      else if (zeros == 1) begin     // already normalized
	 if (expn == 0)
	    expn = 1;       	 // carry, hidden bits
	 res = res << 2;
      end
      else if (zeros == fromInteger(28)) begin   // exactly zero
	 expn = 0;
      end
      else begin     // try to normalize
	 Int#(9) shift = (zeros - 1);
	 Int#(9) maxshift = expo - 9'b110000010;
         if (shift > maxshift) begin            // result will be subnormal
	    res = res << maxshift;
	    expn = 0;
	 end
	 else begin                            // result will be normal
	    res = res << shift;
	    expn = expn - truncate(pack(shift));
	 end                    
	 res = res << 2;         // carry, hidden bits
      end

      sfdres = res[27:5];               // final mantissa after normalise
      guards[2] = res[4];                //guard bit
      guards[1] = res[3];                // round bit
      guards[0] = |res[2:0];             // sticky bit
   end
   if ((expn == 0) && (guards != 0)) begin           //underflow case
      ex[1] = 1'b1;
      ex[0] = 1'b1; end                  
   
   out = FloatingPoint{sign:unpack(sgn),exp:expn,sfd:sfdres};
   
   
  Tuple3#(FloatingPoint#(8,23),Exception,Bit#(3)) x = tuple3(out,unpack(ex),guards);
rg_out_prim3 <= x;
rg_exc3 <= (pack(rg_exc2)|pack(tpl_2(x)));
rg_rnd3 <= rg_rnd2;
rg_sub3 <= rg_sub2;
rg_spl_res3 <= rg_spl_res2;
rg_spl_flag3 <= rg_spl_flag2;
endrule

rule rl_round;                        //rule for rounding operation,stage 4

  RoundMode rmode = unpack(pack(rg_rnd3));
  FloatingPoint#(8,23) din = tpl_1(rg_out_prim3);
  Bit#(3) guards = tpl_3(rg_out_prim3);
  Bit#(1) spl_flag = pack(rg_spl_flag3);
  FloatingPoint#(8,23) spl_res = unpack(pack(rg_spl_res3));
  Bit#(5) exc = pack(rg_exc3);
  Bit#(1) sub = pack(rg_sub3);
  
  FloatingPoint#(8,23) out = defaultValue;
  Exception ex = defaultValue;
  let mant_out = din.sfd;
  let local_expo = din.exp;
  let guard = guards[2];
  let round = guards[1];
  let sticky = guards[0];
  let inexact = guard|round|sticky;
  Bit#(1) lv_roundup = 0;
  Bit#(25) lv_man = {2'b0,mant_out};
  case(rmode) 
   Rnd_Nearest_Even: begin lv_roundup = guard & (mant_out[0] | round | sticky); end        //rnear_even
   Rnd_Nearest_Away_Zero: lv_roundup = guard; //& (round | sticky | ~sign);                //rnear_away_zero
   Rnd_Plus_Inf: lv_roundup = (guard | round | sticky) & (~pack(din.sign));                 //rmax
   Rnd_Minus_Inf: lv_roundup = (guard | round | sticky) & pack(din.sign);                  //rmin
   default: lv_roundup = 0;
  endcase
  if(lv_roundup == 1)
   lv_man = lv_man + 1;
  let out_exp1 = ((lv_man[23] == 1)&&(local_expo<8'hFE))?(local_expo + 1):local_expo;
  let out_exp = ((local_expo==8'hFE)&&((lv_man[23]|lv_man[24])==1))?8'hFF:out_exp1;
  let out_sfd = ((local_expo==8'hFE)&&((lv_man[23]|lv_man[24])==1))?23'd0:lv_man[22:0];

  rg_out_exp <= out_exp;
  rg_out_sfd <= out_sfd;
  rg_lv_man <= lv_man;
  rg_out_prim4 <= rg_out_prim3;
  rg_exc4 <= rg_exc3;
  rg_rnd4 <= rg_rnd3;
  rg_sub4 <= rg_sub3;
  rg_spl_res4 <= rg_spl_res3;
  rg_spl_flag4 <= rg_spl_flag3;
endrule



method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) data_in);              //taking inputs, stage 1

  match {.opA, .opB, .rmode} = data_in;

  Bit#(1) hidden_bit_A = (|opA.exp==0)?0:1;
  Bit#(1) hidden_bit_B = (|opB.exp==0)?0:1;
  Bit#(28) sfdA = {1'b0, hidden_bit_A, opA.sfd, 3'b0};
  Bit#(28) sfdB = {1'b0, hidden_bit_B, opB.sfd, 3'b0};    
  Bit#(1) exp_comp = pack(((opB.exp > opA.exp) || ((opB.exp == opA.exp) && (sfdB > sfdA))));
  Bit#(5) ex =0;
  
  exp_stage1 <= (exp_comp ==1)?opB.exp:opA.exp;             // exponent, stage1
  sign_stage1 <= (exp_comp ==1)?opB.sign:opA.sign;           //sign evaluation,stage1

  Bit#(1) sub_norm = pack(((opA.exp==0)&&(opB.exp!=0))||((opA.exp!=0)&&(opB.exp==0)));    // determination of sub-normal numbers
               
  rg_data_in <= data_in;               
  rg_hd_bit_A <= hidden_bit_A;
  rg_hd_bit_B <= hidden_bit_B;
  rg_sfdA <= sfdA;
  rg_sfdB <= sfdB;
  rg_exp_comp <= exp_comp;
  rg_sub_norm <= sub_norm;
  
  rg_sp_valid[0]<=unpack(1'b1);                                     
endmethod

method ReturnType#(8,23) receive();                                     // output stage
  RoundMode rmode = unpack(pack(rg_rnd4));
  FloatingPoint#(8,23) din = tpl_1(rg_out_prim4);
  Bit#(3) guards = tpl_3(rg_out_prim4);
  Bit#(1) spl_flag = pack(rg_spl_flag4);
  FloatingPoint#(8,23) spl_res = unpack(pack(rg_spl_res4));
  Bit#(5) exc = pack(rg_exc4);
  Bit#(1) sub = pack(rg_sub4);

  FloatingPoint#(8,23) out = defaultValue;
  Exception ex = defaultValue;
  Bit#(8) out_exp = rg_out_exp;
  Bit#(23) out_sfd = rg_out_sfd;
  Bit#(25) lv_man = rg_lv_man;

  let local_expo = din.exp;
  let inexact = |guards;

  Bit#(8) final_exp;
  Bit#(23) final_sfd;
  Bit#(1) out_inf=pack((&out_exp==1'b1)&&(|out_sfd==1'b0));
  Bit#(1) out_z=pack((|out_exp==1'b0)&&(|out_sfd==1'b0));
  Bit#(1) out_snan=pack((&out_exp==1'b1)&&(out_sfd[22]==1'b0)&&(|out_sfd[21:0]==1'b1));
  Bit#(1) out_qnan=pack((&out_exp==1'b1)&&(out_sfd[22]==1'b1));



  Bool final_sign = ((out_z==1) && (inexact==0) && (sub==1))?(rmode == Rnd_Minus_Inf):din.sign;
  final_exp = (out_z==1)?8'd0:((out_inf==1)?8'hFF:((out_qnan==1)?8'hFF:((out_snan==1)?8'hFF:out_exp)));
  final_sfd = (out_z==1)?23'd0:((out_inf==1)?23'd0:((out_qnan==1)?{3'b100,20'd0}:((out_snan==1)?{3'b100,20'd0}:out_sfd)));
  let exc1 = ((local_expo==8'hFE)&&((lv_man[23]|lv_man[24])==1))?(exc|5'b00101):exc;
  out = (spl_flag == 0) ? FloatingPoint{sign:final_sign , exp : final_exp , sfd : final_sfd}:FloatingPoint{sign:spl_res.sign , exp : spl_res.exp , sfd : spl_res.sfd};

  ex = ((out.exp =='1))?((final_sfd!=0)?unpack(exc1&{1'b1,4'd0}):((spl_flag!=0)?unpack(exc1&{1'b1,4'd0}):unpack(exc1))):unpack(exc1|{4'd0,inexact});

  Bit#(1) sp_max = pack(((rmode==Rnd_Plus_Inf)&&(final_sign==True))||((rmode==Rnd_Minus_Inf)&&(final_sign==False)));


  FloatingPoint#(8,23) rounded = ((out.exp ==8'hFF)&&(out.sfd!=0))?FloatingPoint{sign:False , exp:8'hFF , sfd: truncate(24'h400000)}:(((exc[2]&exc[0])==1'b1)?(((rmode!=Rnd_Zero))?((sp_max==1)?FloatingPoint{sign:final_sign , exp:8'hFE , sfd: '1}:FloatingPoint{sign:final_sign , exp:8'hFF , sfd: '0}):FloatingPoint{sign:final_sign , exp:8'hFE , sfd: '1}):out);


  Tuple2#(FloatingPoint#(8,23),Exception) rg_out = tuple2(rounded,ex);
  let x = ReturnType{valid: pack(rg_sp_valid[3]),value:tpl_1(rg_out),ex:tpl_2(rg_out)};
  return x;
endmethod

endmodule






(*synthesize*)
module mk_fpu_adder_dp(Ifc_fpu_adder_dp);                                         // double precision Adder Module

Vector#(6,Reg#(Bit#(1))) rg_dp_valid <- replicateM(mkDReg(0));
Reg#(Bit#(57)) add_sfdX <- mkReg(0);
Reg#(Bit#(57)) add_sfdY <- mkReg(0);

Reg#(Bit#(57)) rg_sum <- mkReg(0);
Reg#(Bit#(57)) rg_sum1 <- mkReg(0);

Reg#(Bit#(11)) exp_stage1 <- mkReg(0);
Reg#(Bool) sign_stage1 <- mkReg(False);

Reg#(Bit#(57)) add_sfdX1 <- mkReg(0);
Reg#(Bit#(57)) add_sfdY1 <- mkReg(0);
Reg#(Bit#(11)) exp_stage2 <- mkReg(0);
Reg#(Bool) sign_stage2 <- mkReg(False);


Reg#(Bit#(11)) exp_stage3 <- mkReg(0);
Reg#(Bool) sign_stage3 <- mkReg(False);


Reg#(Bit#(1)) rg_sub0 <- mkReg(0);
Reg#(Bit#(1)) rg_sub1 <- mkReg(0);
Reg#(Bit#(1)) rg_sub2 <- mkReg(0);
Reg#(Bit#(1)) rg_sub3 <- mkReg(0);
Reg#(Bit#(1)) rg_sub4 <- mkReg(0);


Reg#(Bit#(1)) rg_spl_flag0 <- mkReg(0);
Reg#(Bit#(1)) rg_spl_flag1 <- mkReg(0);
Reg#(Bit#(1)) rg_spl_flag2 <- mkReg(0);
Reg#(Bit#(1)) rg_spl_flag3 <- mkReg(0);

Reg#(Bit#(57)) res <- mkReg(0);


Reg#(Bit#(5)) rg_exc0 <- mkReg(0);
Reg#(Bit#(5)) rg_exc1 <- mkReg(0);
Reg#(Bit#(5)) rg_exc2 <- mkReg(0);
Reg#(Bit#(5)) rg_exc3 <- mkReg(0);

Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out_prim <- mkReg(tuple2(defaultValue,defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),Exception,Bit#(3))) rg_out_prim1 <- mkReg(tuple3(defaultValue,defaultValue,0));
Reg#(Tuple3#(FloatingPoint#(11,52),Exception,Bit#(3))) rg_out_prim2 <- mkReg(tuple3(defaultValue,defaultValue,0));
//Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out <- mkReg(tuple2(defaultValue,defaultValue));

Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out1 <- mkReg(tuple2(defaultValue,defaultValue));
Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out2 <- mkReg(tuple2(defaultValue,defaultValue));

Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operand1 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operand2 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));

Reg#(FloatingPoint#(11,52)) rg_spl_res <- mkReg(defaultValue);
Reg#(FloatingPoint#(11,52)) rg_spl_res1 <- mkReg(defaultValue);
Reg#(FloatingPoint#(11,52)) rg_spl_res2 <- mkReg(defaultValue);
Reg#(FloatingPoint#(11,52)) rg_spl_res3 <- mkReg(defaultValue);

Reg#(RoundMode) rg_rnd0 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd1 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd2 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd3 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd4 <- mkReg(defaultValue);

Reg#(FloatingPoint#(11,52)) rg_spl_res00 <- mkReg(defaultValue);
Reg#(Bit#(1)) rg_spl_flag00 <- mkReg(0);
Reg#(FloatingPoint#(11,52)) rg_spl_res01 <- mkReg(defaultValue);
Reg#(Bit#(1)) rg_spl_flag01 <- mkReg(0);
Reg#(Bit#(5)) rg_exc00 <- mkReg(0);
Reg#(Bit#(5)) rg_exc01 <- mkReg(0);
Reg#(Bit#(57)) rg_sfdX <- mkReg(0);
Reg#(Bit#(57)) rg_sfdY <- mkReg(0);
Reg#(Bit#(1)) rg_sub00 <- mkReg(0);
Reg#(RoundMode) rg_rnd00 <- mkReg(defaultValue);
Reg#(Bit#(11)) exp_stage01 <- mkReg(0);
Reg#(Bool) sign_stage01 <- mkReg(False);
Reg#(Bit#(11)) rg_exp_diff_prim <- mkReg(0);
Reg#(Bit#(1)) rg_sub_norm <- mkReg(0);
Reg#(Bit#(1)) rg_exp_comp <- mkReg(0);
Reg#(Bit#(11)) rg_out_exp <- mkReg(0);
Reg#(Bit#(52)) rg_out_sfd <- mkReg(0);
Reg#(Bit#(54)) rg_lv_man <- mkReg(0);

rule rl_valid_check;
for(Integer i = 1 ; i < 6 ; i = i+1)
  begin
   rg_dp_valid[i] <= rg_dp_valid[i-1];
  end
endrule


rule rl_compute_stage1;                                                 //computation of sign,exponent and mantissa,stage 1
  
  match {.opA, .opB, .rmode} = rg_operand1;
  
  Bit#(57) sfdA = rg_sfdX;
  Bit#(57) sfdB = rg_sfdY;
  
  Bit#(1) exp_comp = rg_exp_comp;
  
  Bit#(57) sfdX = (exp_comp ==1)?sfdB:sfdA;
  Bit#(57) sfdY = (exp_comp ==1)?sfdA:sfdB;
  
  rg_rnd0 <= rmode;                                          //rounding mode
  exp_stage1 <= (exp_comp ==1)?opB.exp:opA.exp;             // exponent, stage 2

  Bit#(1) sub_norm = pack(((opA.exp==0)&&(opB.exp!=0))||((opA.exp!=0)&&(opB.exp==0)));    // determination of sub-normal numbers

  sign_stage1 <= (exp_comp ==1)?opB.sign:opA.sign;          //sign evaluation,stage1
  rg_sub0 <= (exp_comp ==1)?pack(opB.sign != opA.sign):pack(opA.sign != opB.sign);      // determination of Addition or Subtraction
  
  Bit#(11) exp_diff_prim = (exp_comp ==1)?truncate(pack(opB.exp - opA.exp)):truncate(pack(opA.exp - opB.exp));
  Bit#(11) exp_diff =  (sub_norm==1)?exp_diff_prim-1:exp_diff_prim;
  
  if (exp_diff < fromInteger(57)) begin
      Bit#(57) guard = sfdY;
      guard = sfdY << (fromInteger(57) - exp_diff);
      sfdY = sfdY >> exp_diff;
      sfdY[0] = sfdY[0] | (|guard);
  end
  else if (|sfdY == 1) begin
      sfdY = 1;
  end
  add_sfdX <= sfdX;                                                 
  add_sfdY <= sfdY;                                                 
  rg_exc00 <= rg_exc0;
  rg_spl_res00 <= rg_spl_res;
  rg_spl_flag00 <= rg_spl_flag0;

endrule 

rule rl_cla;                                                                    //addition operation, stage 2
rg_sum <= fn_CLA_dp(pack(rg_sub0),pack(add_sfdX),pack(add_sfdY));       //57 bit CLA Adder     
sign_stage2 <= sign_stage1;
exp_stage2 <= exp_stage1;
rg_exc01 <= rg_exc00;
rg_rnd1 <= rg_rnd0;
rg_sub1 <= rg_sub0;
rg_spl_res01 <= rg_spl_res00;
rg_spl_flag01 <= rg_spl_flag00;
endrule

rule rl_dummy_stage3;
rg_sum1 <= rg_sum;
sign_stage3 <= sign_stage2;
exp_stage3 <= exp_stage2;
rg_exc1 <= rg_exc01;
rg_rnd2 <= rg_rnd1;
rg_sub2 <= rg_sub1;
rg_spl_res1 <= rg_spl_res01;
rg_spl_flag1 <= rg_spl_flag01;
endrule

rule rl_add_norm;                                                       // rule for normalising the result from addition, stage 4                      
Tuple3#(FloatingPoint#(11,52),Exception,Bit#(3)) x=fn_norm_dp(pack(sign_stage3),pack(exp_stage3),pack(rg_sum1));
rg_out_prim1<= x;
rg_exc2 <= (pack(rg_exc1)|pack(tpl_2(x)));
rg_rnd3 <= rg_rnd2;
rg_sub3 <= rg_sub2;
rg_spl_res2 <= rg_spl_res1;
rg_spl_flag2 <= rg_spl_flag1;
endrule


rule rl_round;                                     //rule for rounding operation,stage 5

  RoundMode rmode = unpack(pack(rg_rnd3));
  FloatingPoint#(11,52) din = tpl_1(rg_out_prim1);
  Bit#(3) guards = tpl_3(rg_out_prim1);
  Bit#(1) spl_flag = pack(rg_spl_flag2);
  FloatingPoint#(11,52) spl_res = unpack(pack(rg_spl_res2));
  Bit#(5) exc = pack(rg_exc2);
  Bit#(1) sub = pack(rg_sub3);
  
  let mant_out = din.sfd;
  let local_expo = din.exp;
  let guard = guards[2];
  let round = guards[1];
  let sticky = guards[0];
  Bit#(1) lv_roundup = 0;
  Bit#(54) lv_man = {2'b0,mant_out};
  case(rmode) 
   Rnd_Nearest_Even: begin lv_roundup = guard & (mant_out[0] | round | sticky); end        //rnear_even
   Rnd_Nearest_Away_Zero: lv_roundup = guard; //& (round | sticky | ~sign);                //rnear_away_zero
   Rnd_Plus_Inf: lv_roundup = (guard | round | sticky) & (~pack(din.sign));                 //rmax
   Rnd_Minus_Inf: lv_roundup = (guard | round | sticky) & pack(din.sign);                  //rmin
   default: lv_roundup = 0;
  endcase
  if(lv_roundup == 1)
   lv_man = lv_man + 1;
  let out_exp1 = ((lv_man[52] == 1)&&(local_expo<11'd2046))?(local_expo + 1):local_expo;
  let out_exp = ((local_expo==11'd2046)&&((lv_man[52]|lv_man[53])==1))?11'd2047:out_exp1;
  let out_sfd = ((local_expo==11'd2046)&&((lv_man[52]|lv_man[53])==1))?52'd0:lv_man[51:0];
  
  rg_out_exp <= out_exp;
  rg_out_sfd <= out_sfd;
  rg_lv_man <= lv_man;

  rg_out_prim2 <= rg_out_prim1;
  rg_exc3 <= rg_exc2;
  rg_rnd4 <= rg_rnd3;
  rg_sub4 <= rg_sub3;
  rg_spl_res3 <= rg_spl_res2;
  rg_spl_flag3 <= rg_spl_flag2;

  
endrule

method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) data_in);  //taking inputs and doing some pre-computation of finding special exceptions
  rg_operand1 <= data_in;
  rg_operand2 <= data_in;
  match {.opA, .opB, .rmode} = data_in;
  Bit#(1) hidden_bit_A = (|opA.exp==0)?0:1;
  Bit#(1) hidden_bit_B = (|opB.exp==0)?0:1;
  Bit#(57) sfdA = {1'b0, hidden_bit_A, opA.sfd, 3'b0};
  Bit#(57) sfdB = {1'b0, hidden_bit_B, opB.sfd, 3'b0};    
  
  Bit#(1) exp_comp = pack(((opB.exp > opA.exp) || ((opB.exp == opA.exp) && (sfdB > sfdA))));
  
  rg_sfdX <= sfdA;
  rg_sfdY <= sfdB;
  //rg_exp_diff_prim <= exp_diff_prim;
  //rg_sub_norm <= sub_norm;
  rg_exp_comp <= exp_comp;
  
  Bit#(5) ex = 0;
  
  //determination of special exception bits 
  Bit#(1) a_inf=pack((&opA.exp==1'b1)&&(|opA.sfd==1'b0));
  Bit#(1) b_inf=pack((&opB.exp==1'b1)&&(|opB.sfd==1'b0));
  Bit#(1) a_z=pack((|opA.exp==1'b0)&&(|opA.sfd==1'b0));
  Bit#(1) b_z=pack((|opB.exp==1'b0)&&(|opB.sfd==1'b0));
  Bit#(1) a_snan=pack((&opA.exp==1'b1)&&(opA.sfd[51]==1'b0)&&(|opA.sfd[50:0]==1'b1));
  Bit#(1) b_snan=pack((&opB.exp==1'b1)&&(opB.sfd[51]==1'b0)&&(|opB.sfd[50:0]==1'b1));
  Bit#(1) a_qnan=pack((&opA.exp==1'b1)&&(opA.sfd[51]==1'b1));
  Bit#(1) b_qnan=pack((&opB.exp==1'b1)&&(opB.sfd[51]==1'b1));

  Bit#(8) flag = {b_snan,a_snan,b_qnan,a_qnan,b_inf,a_inf,b_z,a_z};

  rg_spl_flag0 <= |flag;                                 //special Exception flag
  ex[4] = ((a_snan|b_snan|(a_inf&b_inf&pack(opA.sign!=opB.sign)))==1'b1)?1'b1:1'b0;
  rg_exc0 <= ex;                                         //exception generation   

  Bit#(1) a_nan = a_snan|a_qnan;
  Bit#(1) b_nan = b_snan|b_qnan;
  Bit#(1) nan = pack(((a_nan&b_inf)==1'b1)||((b_nan&a_inf)==1'b1));


  Bool z_sign = ((a_z&b_z)==1)&&(opA.sign==opB.sign)?opA.sign:((rmode==Rnd_Minus_Inf)?True:False);

  if ((a_z&b_z)==1) rg_spl_res <= FloatingPoint{sign:z_sign , exp:11'd0 , sfd: 52'd0};
  else if(a_z==1) rg_spl_res <= FloatingPoint{sign:opB.sign , exp:opB.exp , sfd: opB.sfd};
  else if (b_z==1) rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: opA.sfd};
  else if ((a_inf&b_inf) ==1) begin
    if (opA.sign == opB.sign) 
        rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: opA.sfd};
    else
        rg_spl_res <= FloatingPoint{sign:False , exp:{3'b111,8'hFF} , sfd: truncate(52'h8000000000000)};     
  end 
  else if (nan==1) rg_spl_res <= FloatingPoint{sign:False , exp:{3'b111,8'hFF} , sfd: truncate(52'h8000000000000)};
  else if (a_inf==1) rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: opA.sfd};
  else if (b_inf==1) rg_spl_res <= FloatingPoint{sign:opB.sign , exp:opB.exp , sfd: opB.sfd};
  else if (a_qnan == 1) rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: opA.sfd};
  else if (b_qnan==1) rg_spl_res <= FloatingPoint{sign:opB.sign , exp:opB.exp , sfd: opB.sfd};
  else if (a_snan == 1) rg_spl_res <= FloatingPoint{sign:opA.sign , exp:opA.exp , sfd: (opA.sfd|truncate(52'h8000000000000))};
  else if (b_snan == 1) rg_spl_res <= FloatingPoint{sign:opB.sign , exp:opB.exp , sfd: (opB.sfd|truncate(52'h8000000000000))};
  else if (((a_qnan&b_qnan)==1)||((a_snan&b_snan)==1)) rg_spl_res <= FloatingPoint{sign:False , exp:{3'b111,8'hFF} , sfd: truncate(52'h8000000000000)};  
  else  rg_spl_res <= FloatingPoint{sign:False , exp:{3'b000,8'd0} , sfd: 52'd0};
rg_dp_valid[0]<=unpack(1'b1);                                    
endmethod



method ReturnType#(11,52) receive();                                                             // output stage

  RoundMode rmode = unpack(pack(rg_rnd4));
  FloatingPoint#(11,52) din = tpl_1(rg_out_prim2);
  Bit#(3) guards = tpl_3(rg_out_prim2);
  Bit#(1) spl_flag = pack(rg_spl_flag3);
  FloatingPoint#(11,52) spl_res = unpack(pack(rg_spl_res3));
  Bit#(5) exc = pack(rg_exc3);
  Bit#(1) sub = pack(rg_sub4);
  
  FloatingPoint#(11,52) out = defaultValue;
  Exception ex = defaultValue;
  Bit#(11) out_exp = rg_out_exp;
  Bit#(52) out_sfd = rg_out_sfd;
  Bit#(54) lv_man = rg_lv_man;
  
  let local_expo = din.exp;
  let inexact = |guards;

  Bit#(11) final_exp;
  Bit#(52) final_sfd;
  Bit#(1) out_inf=pack((&out_exp==1'b1)&&(|out_sfd==1'b0));
  Bit#(1) out_z=pack((|out_exp==1'b0)&&(|out_sfd==1'b0));
  Bit#(1) out_snan=pack((&out_exp==1'b1)&&(out_sfd[51]==1'b0)&&(|out_sfd[50:0]==1'b1));
  Bit#(1) out_qnan=pack((&out_exp==1'b1)&&(out_sfd[51]==1'b1));



  Bool final_sign = ((out_z==1) && (inexact==0) && (sub==1))?(rmode == Rnd_Minus_Inf):din.sign;
  final_exp = (out_z==1)?11'd0:((out_inf==1)?11'd2047:((out_qnan==1)?{3'b111,8'hFF}:((out_snan==1)?{3'b111,8'hFF}:out_exp)));
  final_sfd = (out_z==1)?52'd0:((out_inf==1)?52'd0:((out_qnan==1)?{3'b100,49'd0}:((out_snan==1)?{3'b100,49'd0}:out_sfd)));
  let exc1 = ((local_expo==11'd2046)&&((lv_man[52]|lv_man[53])==1))?(exc|5'b00101):exc;
  out = (spl_flag == 0) ? FloatingPoint{sign:final_sign , exp : final_exp , sfd : final_sfd}:FloatingPoint{sign:spl_res.sign , exp : spl_res.exp , sfd : spl_res.sfd};

  ex = ((out.exp =='1))?((final_sfd!=0)?unpack(exc1&{1'b1,4'd0}):((spl_flag!=0)?unpack(exc1&{1'b1,4'd0}):unpack(exc1))):unpack(exc1|{4'd0,inexact});

  Bit#(1) sp_max = pack(((rmode==Rnd_Plus_Inf)&&(final_sign==True))||((rmode==Rnd_Minus_Inf)&&(final_sign==False)));


  FloatingPoint#(11,52) rounded = ((out.exp ==11'd2047)&&(out.sfd!=0))?FloatingPoint{sign:False , exp:{3'b111,8'hFF} , sfd: truncate(52'h8000000000000)}:(((exc[2]&exc[0])==1'b1)?(((rmode!=Rnd_Zero))?((sp_max==1)?FloatingPoint{sign:final_sign , exp:{3'b111,8'hFE} , sfd: '1}:FloatingPoint{sign:final_sign , exp:{3'b111,8'hFF} , sfd: '0}):FloatingPoint{sign:final_sign , exp:{3'b111,8'hFE} , sfd: '1}):out);


  Tuple2#(FloatingPoint#(11,52),Exception) rg_out = tuple2(rounded,ex);
  
  let x = ReturnType{valid: pack(rg_dp_valid[5]),value:tpl_1(rg_out),ex:tpl_2(rg_out)};
  return x;
endmethod

endmodule




























endpackage


