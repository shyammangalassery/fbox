////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Joyanta Mondal, Nagakaushik Moturi
Email id: ee19m058@smail.iitm.ac.in, ee17b111@smail.iitm.ac.in
Details: Manually pipelined and optimized SP and DP multipliers (6 and 9 cycles)
--------------------------------------------------------------------------------------------------
*/
package fpu_booth_multiplier;

//imported libraries
import fpu_multiplier_common ::*;
import DReg ::*;
import Vector ::*;
import fpu_common ::*;
import DefaultValue      ::*;


//interface declaration
`ifdef fpu_hierarchical
interface Ifc_fpu_multiplier_sp;                                                // single precision Multiplier Interface
  method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) data_in);
  method ReturnType#(8,23) receive();
endinterface
	
interface Ifc_fpu_multiplier_dp;                                                // double precision Multiplier Interface
  method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) data_in);
  method ReturnType#(11,52) receive();
endinterface
`endif



`ifdef fpu_hierarchical

(*synthesize*)
module mk_fpu_multiplier_sp(Ifc_fpu_multiplier_sp);                                         // single precision Multiplier Module


//register declarations
Reg#(Bit#(3)) rg_sp_ex_1<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex0<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex1<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex2<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex3<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex3_d<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex4<-mkReg(0);
Reg#(Bit#(1)) rg_s0<-mkReg(0);
Reg#(Bit#(1)) rg_s1<-mkReg(0);
Reg#(Bit#(1)) rg_s2<-mkReg(0);
Vector#(6,Reg#(Bit#(1))) rg_sp_valid <- replicateM(mkDReg(0));

Reg#(Tuple4#(Bit#(8),Int#(32), Bit#(5),Bit#(2))) rg_e0<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(8),Int#(32), Bit#(5),Bit#(2))) rg_e1<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(48),Bit#(8),Bit#(5),Bit#(2))) rg_x0<-mkReg(tuple4(0,0,0,0));
Reg#(Bit#(48)) rg_x00 <- mkReg(0);
Reg#(RoundMode) rg_rnd0 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd1 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd2 <- mkReg(defaultValue);
Reg#(Exception) rg_exc0 <- mkReg(defaultValue);
Reg#(Exception) rg_exc1 <- mkReg(defaultValue);
Reg#(Exception) rg_exc2 <- mkReg(defaultValue);
Reg#(Exception) rg_exc3 <- mkReg(defaultValue);
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands1 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands2 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands3 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands4 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands5 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands6 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands7 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands8 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out_1 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out_2 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out <- mkReg(tuple2(unpack(0),unpack(0)));
//Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out_d <- mkReg(tuple2(unpack(0),unpack(0)));
//Reg#(Tuple3#(Bit#(10),Bit#(1),Int#(32))) rg_e1 <- mkReg(tuple3(0,0,unpack(0)));
Reg#(Bit#(48)) rg_partial_product0 <- mkReg(0);
Reg#(Bit#(48)) rg_partial_product1 <- mkReg(0);
Reg#(Bit#(48)) rg_partial_product2 <- mkReg(0);
Reg#(Bit#(48)) rg_partial_product3 <- mkReg(0);




rule rl_exponent_evaluate_stage1;                                         
  match {.opA, .opB, .rmode} = rg_operands1;
  rg_e0 <= fn_find_exp_sp(opA.exp,opB.exp,opA.sfd,opB.sfd);                  //evaluating exponent, 1st stage
  rg_s0<= pack(opA.sign != opB.sign);                                        //evaluating sign, 1st stage
endrule


rule rl_special_exception_check_stage1;
  match {.opA, .opB, .rmode} = rg_operands2;
  rg_rnd0<= rmode;  
  rg_sp_ex0<= fn_Special_EX_sp(opA.exp,opB.exp,opA.sfd,opB.sfd);             //evaluating special exceptions,1st stage
endrule




rule rl_partial_product_1_stage1;                  
  match {.opA, .opB, .rmode} = rg_operands3;                                 // evaluating partial product1 for mantissa, 1st stage
  Bit#(8) expA = 0;
  Bit#(8) expB = 0;
  Bit#(23) sfdA = 0;
  Bit#(23) sfdB = 0;

  sfdA = opA.sfd;
  expA = opA.exp;
  sfdB = opB.sfd;
  expB = opB.exp;

  rg_partial_product0<=fn_gen_pp_sp({(|expA),sfdA},sfdB[5:0]);
endrule                                                       
    
    
    
rule rl_partial_product_2_stage1;                                             // evaluating partial product2 for mantissa, 1st stage 
  match {.opA, .opB, .rmode} = rg_operands4; 
  Bit#(8) expA1 = 0;
  Bit#(8) expB1 = 0;
  Bit#(23) sfdA1 = 0;
  Bit#(23) sfdB1 = 0;

  sfdA1 = opA.sfd;
  expA1 = opA.exp;
  sfdB1 = opB.sfd;
  expB1 = opB.exp;
  
  rg_partial_product1<= {fn_gen_pp_sp({(|expA1),sfdA1},sfdB1[11:6])[41:0],6'd0};
endrule   
          
          
rule rl_partial_product_3_stage1;                                            // evaluating partial product3 for mantissa, 1st stage
  match {.opA, .opB, .rmode} = rg_operands5; 
  Bit#(8) expA1 = 0;
  Bit#(8) expB1 = 0;
  Bit#(23) sfdA1 = 0;
  Bit#(23) sfdB1 = 0;
  
  sfdA1 = opA.sfd;
  expA1 = opA.exp;
  sfdB1 = opB.sfd;
  expB1 = opB.exp;

  rg_partial_product2<={fn_gen_pp_sp({(|expA1),sfdA1},sfdB1[17:12])[35:0],12'd0};
endrule            
         
rule rl_partial_product_4_stage1;                                             // evaluating partial product4 for mantissa, 1st stage
  match {.opA, .opB, .rmode} = rg_operands6; 
  Bit#(8) expA1 = 0;
  Bit#(8) expB1 = 0;
  Bit#(23) sfdA1 = 0;
  Bit#(23) sfdB1 = 0;
  
  sfdA1 = opA.sfd;
  expA1 = opA.exp;
  sfdB1 = opB.sfd;
  expB1 = opB.exp;

  rg_partial_product3<={fn_gen_pp_sp({(|expA1),sfdA1},{(|expB1),sfdB1[22:18]})[29:0],18'd0};
endrule             
         


rule rl_add_stage2;                               // evaluating mantissa products(addition of partial products),exception generation, 2nd stage
  rg_sp_ex1<=rg_sp_ex0;
  rg_s1<=(tpl_3(rg_e0)[4]==1'b1)?1'b0:rg_s0;
  rg_x00<= fn_add_sp(pack(rg_partial_product0),pack(rg_partial_product1),pack(rg_partial_product2),pack(rg_partial_product3));
  rg_rnd1<=rg_rnd0;
  rg_e1<= rg_e0;
endrule

rule rl_normalise_stage3;                          // normalisation, 3rd stage
  rg_sp_ex2<=rg_sp_ex1;
  rg_rnd2 <= rg_rnd1;
  rg_exc3 <= unpack(tpl_3(rg_e1));
  rg_x0 <= fn_norm_sp(pack(rg_x00),tpl_1(rg_e1),tpl_2(rg_e1),tpl_3(rg_e1),tpl_4(rg_e1));
  rg_s2 <= rg_s1;
endrule
 


(*no_implicit_conditions*)                                 //rounding off mantissa and finalising exponents and exceptions,4th stage
rule rl_round_stage4; 
  rg_sp_ex3<=rg_sp_ex2;
  case(rg_rnd2)
    Rnd_Zero              : rg_out <= fn_rnd_Zero_sp(rg_exc3,pack(rg_s2),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    Rnd_Nearest_Even      : rg_out <= fn_rnd_Nearest_Even_sp(rg_exc3,pack(rg_s2),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    Rnd_Nearest_Away_Zero : rg_out <= fn_rnd_Nearest_Away_Zero_sp(rg_exc3,pack(rg_s2),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    Rnd_Plus_Inf          : rg_out <= fn_rnd_Plus_Inf_sp(rg_exc3,pack(rg_s2),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    Rnd_Minus_Inf         : rg_out <= fn_rnd_Minus_Inf_sp(rg_exc3,pack(rg_s2),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    default               : rg_out <= tuple2(defaultValue,defaultValue);  
  endcase 
endrule

rule rl_valid_check;
  for(Integer i = 1 ; i <= 4 ; i = i+1)
    begin
      rg_sp_valid[i] <= rg_sp_valid[i-1];
    end
endrule


// send and receive methods
method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) data_in);
  rg_operands1<=data_in;
  rg_operands2<=data_in;
  rg_operands3<=data_in;
  rg_operands4<=data_in;
  rg_operands5<=data_in;
  rg_operands6<=data_in;
  rg_sp_valid[0]<=unpack(1'b1);
endmethod


method ReturnType#(8,23) receive();                                      
  return case (pack(rg_sp_ex3))
    3'b000:  ReturnType{valid: pack(rg_sp_valid[4]),value:tpl_1(rg_out),ex:tpl_2(rg_out)};
    3'b100:  ReturnType{valid: pack(rg_sp_valid[4]),value:FloatingPoint{sign: unpack(1'b0),exp: 8'hFF,sfd: truncate(24'h400000)},ex:unpack(5'b10000)};
    3'b010:  ReturnType{valid: pack(rg_sp_valid[4]),value:FloatingPoint{sign: tpl_1(rg_out).sign,exp: 8'hFF,sfd: 23'd0},ex:defaultValue};
    3'b001:  ReturnType{valid: pack(rg_sp_valid[4]),value:FloatingPoint{sign: tpl_1(rg_out).sign,exp: 8'd0,sfd: 23'd0},ex:defaultValue};
    3'b111:  ReturnType{valid: pack(rg_sp_valid[4]),value:FloatingPoint{sign: unpack(1'b0),exp: 8'hFF,sfd: truncate(24'h400000)},ex:unpack(5'b00000)};
   endcase;
endmethod

endmodule





	
(*synthesize*)
module mk_fpu_multiplier_dp(Ifc_fpu_multiplier_dp);       // double precision Multiplier Module
                                        
//register declarations
Reg#(Bit#(3)) rg_dp_ex_1<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex0<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex00<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex01<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex11<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex02<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex1<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex2<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex2_d<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex3<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex4<-mkReg(0);
Reg#(Bit#(1)) rg_s0<-mkReg(0);
Reg#(Bit#(1)) rg_s00<-mkReg(0);
Reg#(Bit#(1)) rg_s01<-mkReg(0);
Reg#(Bit#(1)) rg_s11<-mkReg(0);
Reg#(Bit#(1)) rg_s02<-mkReg(0);
Reg#(Bit#(1)) rg_s1<-mkReg(0);

Reg#(Tuple4#(Bit#(11),Int#(32), Bit#(5),Bit#(2))) rg_e0<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(11),Int#(32), Bit#(5),Bit#(2))) rg_e00<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(11),Int#(32), Bit#(5),Bit#(2))) rg_e01<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(11),Int#(32), Bit#(5),Bit#(2))) rg_e011<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(11),Int#(32), Bit#(5),Bit#(2))) rg_e02<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(106),Bit#(11),Bit#(5),Bit#(2))) rg_x0<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple3#(Bit#(13),Bit#(1),Int#(32))) rg_e1 <- mkReg(tuple3(0,0,unpack(0)));

Reg#(RoundMode) rg_rnd0 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd00 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd01 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd11 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd02 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd1 <- mkReg(defaultValue);
Reg#(Exception) rg_exc0 <- mkReg(defaultValue);
Reg#(Exception) rg_exc1 <- mkReg(defaultValue);
Reg#(Exception) rg_exc2 <- mkReg(defaultValue);

Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands1 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands2 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands3 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands3_1 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands4 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands5 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands6 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands7 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands8 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands9 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands10 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands11 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));


Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out_1 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out_2 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out_3 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out_d <- mkReg(tuple2(unpack(0),unpack(0)));

Reg#(Bit#(108)) rg_partial_product0 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product1 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product2 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product3 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product4 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product5 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product6 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product7 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product8 <- mkReg(0);
Reg#(Bit#(216)) rg_partial_product0_1 <- mkReg(0);
Reg#(Bit#(216)) rg_partial_product1_1 <- mkReg(0);
Reg#(Bit#(216)) rg_partial_product2_1 <- mkReg(0);
Reg#(Bit#(216)) rg_partial_product3_1 <- mkReg(0);
Reg#(Bit#(216)) rg_partial_product4_1 <- mkReg(0);
Reg#(Bit#(216)) rg_partial_product5_1 <- mkReg(0);
Reg#(Bit#(216)) rg_partial_product6_1 <- mkReg(0);
Reg#(Bit#(216)) rg_partial_product7_1 <- mkReg(0);
Reg#(Bit#(216)) rg_partial_product8_1 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product0_01 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product1_11 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product2_21 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product3_31 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product4_41 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product5_51 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product6_61 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product7_71 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product8_81 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product5_2 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product6_2 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product7_2 <- mkReg(0);
Reg#(Bit#(108)) rg_partial_product8_2 <- mkReg(0);

Reg#(Bit#(108)) rg_res <- mkReg(0);
Reg#(Bit#(108)) rg_res1 <- mkReg(0);
Reg#(Bit#(108)) rg_res11 <- mkReg(0);

Vector#(8,Reg#(Bit#(1))) rg_dp_valid <- replicateM(mkDReg(0));

Reg#(Bit#(32)) i1 <- mkReg(0);
Integer i0=0;



//evaluation of partial products, 1st stage (all rules conflict free)

rule rl_exponent_evaluate_stage1;                                         
  match {.opA, .opB, .rmode} = rg_operands1;
  rg_e02 <= fn_find_exp_dp(opA.exp,opB.exp,opA.sfd,opB.sfd);                   //evaluating exponent, 1st stage
  rg_s02<= pack(opA.sign != opB.sign);                                         //evaluating sign, 1st stage
endrule


rule rl_special_exception_check_stage1;
  match {.opA, .opB, .rmode} = rg_operands2;
  rg_rnd02<= rmode;  
  rg_dp_ex02<= fn_Special_EX_dp(opA.exp,opB.exp,opA.sfd,opB.sfd);              //evaluating special exceptions,1st stage
endrule




rule rl_partial_product_1_stage1;                  
  match {.opA, .opB, .rmode} = rg_operands3;                                  // evaluating partial products(in 2 halves) for mantissa, 1st stage
  Bit#(11) expA1 = 0;
  Bit#(11) expB1 = 0;
  Bit#(52) sfdA1 = 0;
  Bit#(52) sfdB1 = 0;
  
  sfdA1 = opA.sfd;
  expA1 = opA.exp;
  sfdB1 = opB.sfd;
  expB1 = opB.exp;
  
  rg_partial_product0_1<=fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[5:0]);

  rg_partial_product1_1<= fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[11:6]);
  
  rg_partial_product2_1<=fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[17:12]);
  
  rg_partial_product3_1<=fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[23:18]);
  
  rg_partial_product4_1<=fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[29:24]);
  
  rg_partial_product5_1<=fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[35:30]);
  
  rg_partial_product6_1<=fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[41:36]);
  
  rg_partial_product7_1<=fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[47:42]);

  rg_partial_product8_1<=fn_gen_pp_dp({1'b0,(|expA1),sfdA1},{1'b0,(|expB1),sfdB1[51:48]});

  
  rg_operands3_1 <= rg_operands3;
endrule  

rule rl_partial_product_stage_2;
  match {.opA, .opB, .rmode} = rg_operands3_1;                                  // evaluating partial products(adding the 2 halves) for mantissa, 2nd stage
  Bit#(11) expA1 = 0;
  Bit#(11) expB1 = 0;
  Bit#(52) sfdA1 = 0;
  Bit#(52) sfdB1 = 0;
  
  sfdA1 = opA.sfd;
  expA1 = opA.exp;
  sfdB1 = opB.sfd;
  expB1 = opB.exp;
  
  rg_partial_product0 <= rg_partial_product0_1[107:0]+rg_partial_product0_1[215:108];
  rg_partial_product1 <= {(rg_partial_product1_1[107:0]+rg_partial_product1_1[215:108])[101:0],6'd0};
  rg_partial_product2 <= {(rg_partial_product2_1[107:0]+rg_partial_product2_1[215:108])[95:0],12'd0};
  rg_partial_product3 <= {(rg_partial_product3_1[107:0]+rg_partial_product3_1[215:108])[89:0],18'd0};
  rg_partial_product4 <= {(rg_partial_product4_1[107:0]+rg_partial_product4_1[215:108])[83:0],24'd0};
  rg_partial_product5 <= {(rg_partial_product5_1[107:0]+rg_partial_product5_1[215:108])[77:0],30'd0};
  rg_partial_product6 <= {(rg_partial_product6_1[107:0]+rg_partial_product6_1[215:108])[71:0],36'd0};
  rg_partial_product7 <= {(rg_partial_product7_1[107:0]+rg_partial_product7_1[215:108])[65:0],42'd0};
  rg_partial_product8 <= {(rg_partial_product8_1[107:0]+rg_partial_product8_1[215:108])[59:0],48'd0};
  
  
  rg_dp_ex0<=rg_dp_ex02; 
  rg_s0<=rg_s02;
  rg_rnd0<=rg_rnd02; 
  rg_e0<=rg_e02;
 
 
endrule

rule rl_partial_product_add_stage3;            //3rd stage  
  Bit#(108) v1 =0;
  Bit#(108) v2 =0;
  Bit#(108) v3 =0;
  Bit#(108) v4 =0;
  Bit#(108) v5 =0;

  v1 = pack(rg_partial_product0);
  v2 = pack(rg_partial_product1);
  v3 = pack(rg_partial_product2);
  v4 = pack(rg_partial_product3);
  v5 = pack(rg_partial_product4);


  rg_res<= v1+v2+v3+v4+v5;                     //addition of 5 partial products to get intermediate product
  rg_dp_ex00<=rg_dp_ex0; 
  rg_s00<=rg_s0;
  rg_rnd00<=rg_rnd0; 
  rg_e00<=rg_e0;
  
  rg_partial_product5_2 <= rg_partial_product5;
  rg_partial_product6_2 <= rg_partial_product6;
  rg_partial_product7_2 <= rg_partial_product7;
  rg_partial_product8_2 <= rg_partial_product8;
endrule 

rule rl_product_propagate_stage4;            //4th stage
  Bit#(108) v6 =0;
  Bit#(108) v7 =0;
  Bit#(108) v8 =0;
  Bit#(108) v9 =0;
  Bit#(108) v10 =0;
  v6 = pack(rg_partial_product5_2);
  v7 = pack(rg_partial_product6_2);
  v8 = pack(rg_partial_product7_2);
  v9 = pack(rg_partial_product8_2);
  v10 = pack(rg_res);
  
  rg_res1<=v6+v7+v8+v9+v10;                     //addition of all partial products to get final product
  
  rg_s01<=rg_s00;
  rg_dp_ex01<=rg_dp_ex00;
  rg_rnd01<=rg_rnd00;
  rg_e01<=rg_e00;
endrule
  
  

rule rl_normalise_stage5;                             // Counting zeros for normalization,evaluating mantissa products,exception generation, 5th stage
  Integer i0 =0;
  rg_dp_ex11<=rg_dp_ex01;
  rg_s11<=(tpl_3(rg_e01)[4]==1'b1)?1'b0:rg_s01;
  i0 = fn_count_zeros_dp(pack(rg_res1),tpl_1(rg_e01)); //count zeros 
  i1 <= fromInteger(i0);
  rg_rnd11<=rg_rnd01;    
  rg_res11<=rg_res1; 
  rg_e011 <= rg_e01;                                                      //exception
endrule
 
rule rl_normalise_stage6;                            //Normalization, 6th stage
  rg_dp_ex1<=rg_dp_ex11;
  rg_s1<=(tpl_3(rg_e011)[4]==1'b1)?1'b0:rg_s11;
  rg_x0<= fn_norm_dp(pack(rg_res11),tpl_1(rg_e011),tpl_2(rg_e011),tpl_3(rg_e011),tpl_4(rg_e011),i1); //normalise 
  rg_rnd1<=rg_rnd11;
  rg_exc2<=unpack(tpl_3(rg_e011));                                                            //exception
endrule
 


                                         
rule rl_round_stage7;                              //rounding off mantissa and finalising exponents and exceptions,7th stage
  rg_dp_ex2_d<=rg_dp_ex1;
  case(rg_rnd1)
    Rnd_Zero              : rg_out_d <= fn_rnd_Zero_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    Rnd_Nearest_Even      : rg_out_d <= fn_rnd_Nearest_Even_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    Rnd_Nearest_Away_Zero : rg_out_d <= fn_rnd_Nearest_Away_Zero_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    Rnd_Plus_Inf          : rg_out_d <= fn_rnd_Plus_Inf_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    Rnd_Minus_Inf         : rg_out_d <= fn_rnd_Minus_Inf_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));
    default               : rg_out_d <= tuple2(defaultValue,defaultValue);  
  endcase 
endrule

rule rl_valid_check;
  for(Integer i = 1 ; i <= 7 ; i = i+1)
   begin
     rg_dp_valid[i] <= rg_dp_valid[i-1];
   end
endrule


// send and receive methods
method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) data_in);
 rg_operands1<=data_in;
 rg_operands2<=data_in;
 rg_operands3<=data_in;
 rg_operands4<=data_in;
 rg_operands5<=data_in;
 rg_operands6<=data_in;
 rg_operands7<=data_in;
 rg_operands8<=data_in;
 rg_operands9<=data_in;
 rg_operands10<=data_in;
 rg_operands11<=data_in;
 rg_dp_valid[0]<=unpack(1'b1);
endmethod


method ReturnType#(11,52) receive();                                                  
  return case (pack(rg_dp_ex2_d))
    3'b000:  ReturnType{valid: pack(rg_dp_valid[7]),value:tpl_1(rg_out_d),ex:tpl_2(rg_out_d)};
    3'b100:  ReturnType{valid: pack(rg_dp_valid[7]),value:FloatingPoint{sign: unpack(1'b0),exp: 11'd2047,sfd:  truncate(52'h8000000000000)},ex:unpack(5'b10000)};
    3'b010:  ReturnType{valid: pack(rg_dp_valid[7]),value:FloatingPoint{sign: tpl_1(rg_out_d).sign,exp: 11'd2047,sfd: 52'd0},ex:defaultValue};
    3'b001:  ReturnType{valid: pack(rg_dp_valid[7]),value:FloatingPoint{sign: tpl_1(rg_out_d).sign,exp: 11'd0,sfd: 52'd0},ex:defaultValue};
    3'b111:  ReturnType{valid: pack(rg_dp_valid[7]),value:FloatingPoint{sign: unpack(1'b0),exp: 11'd2047,sfd:  truncate(52'h8000000000000)},ex:unpack(5'b00000)};
  endcase;
endmethod

endmodule

`endif

endpackage



