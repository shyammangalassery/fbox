// Copyright (c) 2016-2019 Bluespec, Inc. All Rights Reserved

package fpu;

// ================================================================
// This package executes the FD instructions, and implements the
// FP core of the CPU

// ================================================================
// BSV Library imports

import FIFO         :: *;
import Assert        :: *;
import ConfigReg     :: *;
import FShow         :: *;
import SSFloating_Point :: *;
import GetPut        :: *;
import ClientServer  :: *;
import DefaultValue  :: *;
import UniqueWrappers::*;


// ----------------
// BSV additional libs

//import Cur_Cycle  :: *;
//import GetPut_Aux :: *;

// ================================================================
// Project imports

import ISA_Decls :: *;
//import FPU       :: *;

// ================================================================
// FBox interface

typedef struct {
   Bit #(FPLEN)   value;            // The result rd
   Bit #(5)    flags;            // FCSR.FFLAGS update value
} FBoxResult deriving (Bits, Eq, FShow);

typedef enum {
   FBOX_RST,                     // FBox is resetting
   FBOX_REQ,                     // FBox is accepting a request
   FBOX_BUSY,                    // FBox waiting for a response
   FBOX_RSP                      // FBox driving response
} FBoxState deriving (Bits, Eq, FShow);

interface Ifc_fpu;

   // FBox request
  // (* always_ready *)
   method Action req (
        Opcode                      opcode
      , Bit #(7)                    f7
      , Bit #(3)                    rm
      , RegName                     rs2
      , Bit #(FPLEN)                   v1
      , Bit #(FPLEN)                   v2
      , Bit #(FPLEN)                   v3
   );

   // FBox response
   //(* always_ready *)
   method ActionValue#(Tuple2 #(Bit #(FPLEN), Bit #(5))) word;
endinterface

// ================================================================
// Some helper function
// Definitions of Q-NaNs for single and double precision
Bit #(32) canonicalNaN32 = 32'h7fc00000;
Bit #(64) canonicalNaN64 = 64'h7ff8000000000000;

// Convert the rounding mode into the format understood by the FPU/PNU
function SSFloating_Point::RoundMode fv_getRoundMode (Bit #(3) rm);
   case (rm)
      3'h0: return (Rnd_Nearest_Even);
      3'h1: return (Rnd_Zero);
      3'h2: return (Rnd_Minus_Inf);
      3'h3: return (Rnd_Plus_Inf);
      3'h4: return (Rnd_Nearest_Away_Zero); // XXX Is this RMM?
      default: return (Rnd_Nearest_Even);
   endcase
endfunction

// Converts the exception coming from the FPU/PNU to the format for the FCSR
function Bit #(5) exception_to_fcsr( SSFloating_Point::Exception x );
   let nv  = x.invalid_op ? 1'b1 : 1'b0 ;
   let dz  = x.divide_0   ? 1'b1 : 1'b0 ;
   let of  = x.overflow   ? 1'b1 : 1'b0 ;
   let uf  = x.underflow  ? 1'b1 : 1'b0 ;
   let nx  = x.inexact    ? 1'b1 : 1'b0 ;
   return pack ({nv, dz, of, uf, nx});
endfunction

// Take a single precision value and nanboxes it to be able to write it to a
// 64-bit FPR register file. This is necessary if single precision operands
// used with a register file capable of holding double precision values

// Take a 64-bit value and check if it is properly nanboxed if operating in a DP
// capable environment. If not properly nanboxed, return canonicalNaN32
function FloatingPoint#(8,23) fv_unbox (Bit #(FPLEN) x);
`ifdef ISA_D
   if (x [63:32] == 32'hffffffff)
      return (unpack (x [31:0]));
   else
      return (unpack (canonicalNaN32));
`else  
   return (unpack (x [31:0]));
`endif
endfunction

// Check if FloatingPoint#(8,23) is a +0
function Bool fv_FDataIsPositiveZero ( FloatingPoint#(8,23) x );
   return ( isZero (x) && !(x.sign) );
endfunction

/*function Tuple2#( FloatingPoint#(8,23), SSFloating_Point::Exception ) fpu_ops(Tuple5#( FloatingPoint#(8,23), FloatingPoint#(8,23), FloatingPoint#(8,23), RoundMode, FpuOp) x);
  
    //Tuple2#( FloatingPoint#(8,23), SSFloating_Point::Exception ) res;
   FloatingPoint#(8,23) fp = defaultValue;
   SSFloating_Point::Exception ex = defaultValue;

   let op1  = tpl_1(x);
   let op2  = tpl_2(x);
   let op3  = tpl_3(x);
   let rmd  = tpl_4(x);
   let iop  = tpl_5(x);
   
   Bool negateResult = False;

      if      (iop == FPNMAdd)    negateResult = True;
      else if (iop == FPNMSub)    negateResult = True;
      else                        negateResult = False;
      
      case ( iop )
         FPAdd:   {fp, ex} = fmaFP(  tuple4(Valid(op1), op2,         one(False), rmd) );
         FPSub:   {fp, ex} = fmaFP(  tuple4(Valid(op1), negate(op2), one(False), rmd) );
         FPMul:   {fp, ex} = fmaFP(  tuple4(Invalid,     op1,         op2,       rmd) );
         FPDiv:   {fp, ex} = dividerFP( tuple3(op1, op2, rmd ));
         //FPDiv:   {fp, ex} = divFP( op1, op2, rmd );
         FPSqrt:  {fp, ex} = squareRootFP( tuple2(op1, rmd) );
         FPMAdd:  {fp, ex} = fmaFP(  tuple4(Valid(op3),         op1, op2, rmd) );
         FPMSub:  {fp, ex} = fmaFP(  tuple4(Valid(negate(op3)), op1, op2, rmd) );
         FPNMAdd: {fp, ex} = fmaFP(  tuple4(Valid(op3),         op1, op2, rmd) );
         FPNMSub: {fp, ex} = fmaFP(  tuple4(Valid(negate(op3)), op1, op2, rmd) );
      endcase

         if (negateResult) begin
            let neg = negate( fp );
            return tuple2(neg, ex);
         end
         else return tuple2(fp, ex);
   endfunction*/

// ================================================================

(* synthesize *)
module mkfpu (Ifc_fpu);

   

  Reg   #(Maybe #(Tuple7 #(
        Opcode
      , Bit #(7)
      , RegName 
      , Bit #(3)
      , Bit #(FPLEN)
      , Bit #(FPLEN)
      , Bit #(FPLEN))))       requestR             <- mkReg(tagged Invalid);



   Reg   #(Maybe #(Tuple2 #(
        Bit #(FPLEN)
      , Bit #(5))))        resultR              <- mkReg(tagged Invalid);
      
    
//Wrapper#(Tuple5#( FloatingPoint#(8,23), FloatingPoint#(8,23), FloatingPoint#(8,23), RoundMode, FpuOp), Tuple2#( FloatingPoint#(8,23), SSFloating_Point::Exception )) wr_fpu_ops <- mkUniqueWrapper(fpu_ops);
//Wrapper3#(Bit#(32), Bit#(5), Bit#(2), Tuple3#(Bit#(1),Int#(9), Bit#(28))) decodePosit32_op2 <- mkUniqueWrapper3(decodePosit);
//Wrapper3#(Bit#(32), Bit#(5), Bit#(2), Tuple3#(Bit#(1),Int#(9), Bit#(28))) decodePosit32_op3 <- mkUniqueWrapper3(decodePosit);

		 
		 Wrapper#(Tuple4#(Maybe#(FloatingPoint#(8,23)), FloatingPoint#(8,23), FloatingPoint#(8,23), SSFloating_Point::RoundMode), Tuple2#(FloatingPoint#(8,23),SSFloating_Point::Exception) ) wr_fmaFP <- mkUniqueWrapper(fmaFP);
   
   //FPU_IFC                 fpu                  <- mkFPU;

   // =============================================================
   // Drive response to the pipeline


   // =============================================================
   // Decode sub-opcodes (a direct lift from the spec)
  rule start(isValid(requestR) && !isValid(resultR));
   match {.opc, .f7, .rs2, .rm, .v1, .v2, .v3} = requestR.Valid;
   //requestR.deq;
   Bit #(2) f2 = f7[1:0];
  
   
   

   let isFMADD_S     = (opc == op_FMADD)  && (f2 == 0);
   let isFMSUB_S     = (opc == op_FMSUB)  && (f2 == 0);
   let isFNMADD_S    = (opc == op_FNMADD) && (f2 == 0);
   let isFNMSUB_S    = (opc == op_FNMSUB) && (f2 == 0);
   let isFADD_S      = (opc == op_FP) && (f7 == f7_FADD_S); 
   let isFSUB_S      = (opc == op_FP) && (f7 == f7_FSUB_S);
   let isFMUL_S      = (opc == op_FP) && (f7 == f7_FMUL_S);

   let isFDIV_S      = (opc == op_FP) && (f7 == f7_FDIV_S);
   let isFSQRT_S     = (opc == op_FP) && (f7 == f7_FSQRT_S);

   let isFSGNJ_S     = (opc == op_FP) && (f7 == f7_FSGNJ_S) && (rm == 0);
   let isFSGNJN_S    = (opc == op_FP) && (f7 == f7_FSGNJ_S) && (rm == 1);
   let isFSGNJX_S    = (opc == op_FP) && (f7 == f7_FSGNJ_S) && (rm == 2);
   let isFCVT_W_S    = (opc == op_FP) && (f7 == f7_FCVT_W_S)  && (rs2 == 0);
   let isFCVT_WU_S   = (opc == op_FP) && (f7 == f7_FCVT_WU_S) && (rs2 == 1);

   let isFCVT_S_W    = (opc == op_FP) && (f7 == f7_FCVT_S_W)  && (rs2 == 0);
   let isFCVT_S_WU   = (opc == op_FP) && (f7 == f7_FCVT_S_WU) && (rs2 == 1);

   let isFMIN_S      = (opc == op_FP) && (f7 == f7_FMIN_S) && (rm == 0);
   let isFMAX_S      = (opc == op_FP) && (f7 == f7_FMAX_S) && (rm == 1);
   let isFLE_S       = (opc == op_FP) && (f7 == f7_FCMP_S) && (rm == 0);
   let isFLT_S       = (opc == op_FP) && (f7 == f7_FCMP_S) && (rm == 1);
   let isFEQ_S       = (opc == op_FP) && (f7 == f7_FCMP_S) && (rm == 2);
   let isFMV_X_W     = (opc == op_FP) && (f7 == f7_FMV_X_W) && (rm == 0);
   let isFMV_W_X     = (opc == op_FP) && (f7 == f7_FMV_W_X) && (rm == 0);
   let isFCLASS_S    = (opc == op_FP) && (f7 == f7_FCLASS_S) && (rm == 1);

   // =============================================================
   // Prepare the operands. The operands come in as raw 64 bits. They need to be
   // type cast as FloatingPoint#(8,23) of FloatingPoint#(8,23). This is also where the nanbox check needs
   // to be done. If we are executing in a DP capable environment, all SP 64-bit
   // rs values should be properly nanboxed. Otherwise, they will be treated as
   // as canonicalNaN32
   FloatingPoint#(8,23) sV1, sV2, sV3;
   
   sV1 = unpack(v1);
   sV2 = unpack(v2);
   sV3 = unpack(v3);

   let rmd = fv_getRoundMode (rm);

   // =============================================================
   // BEHAVIOR

   // These rules execute the operations, either dispatch to the FPU/PNU or
   // locally here in the F-Box
   Bool validReq = isValid (requestR)  ;

   // Single precision operations
   let cmpres_s = compareFP ( sV1, sV2 );
   if( validReq && isFADD_S ) begin
      //let x <- wr_fpu_ops.func(tuple5 (sV1, sV2, ?, rmd, FPAdd));
      let x <- wr_fmaFP.func(  tuple4(Valid(sV1), sV2,         one(False), rmd) );
      let fcsr = exception_to_fcsr(tpl_2(x));
      resultR     <= tagged Valid (tuple2 (pack(tpl_1(x)), fcsr));
      //resultR.enq(tuple2 (pack(tpl_1(x)), fcsr));
   end
   
   else if(validReq && isFSUB_S ) begin
      //let x <- wr_fpu_ops.func(tuple5 (sV1, sV2, ?, rmd, FPSub));
      let x <- wr_fmaFP.func(  tuple4(Valid(sV1), negate(sV2), one(False), rmd) );
      let fcsr = exception_to_fcsr(tpl_2(x));
      resultR     <= tagged Valid (tuple2 (pack(tpl_1(x)), fcsr));
      //resultR.enq(tuple2 (pack(tpl_1(x)), fcsr));
   end
   
   else if( validReq &&isFMUL_S ) begin
      //let x <- wr_fpu_ops.func(tuple5 (sV1, sV2, ?, rmd, FPMul));
      let x <- wr_fmaFP.func(  tuple4(Invalid,     sV1,         sV2,       rmd) );
      let fcsr = exception_to_fcsr(tpl_2(x));
      resultR     <= tagged Valid (tuple2 (pack(tpl_1(x)), fcsr));
      //resultR.enq(tuple2 (pack(tpl_1(x)), fcsr));
   end
   
   else if( validReq && isFMADD_S ) begin
      //let x <- wr_fpu_ops.func(tuple5( sV1, sV2, sV3, rmd, FPMAdd ));
      let  x <- wr_fmaFP.func(  tuple4(Valid(sV3),         sV1, sV2, rmd) );
      let fcsr = exception_to_fcsr(tpl_2(x));
      resultR     <= tagged Valid (tuple2 (pack(tpl_1(x)), fcsr));
      //resultR.enq(tuple2 (pack(tpl_1(x)), fcsr));
   end
   
   else if( validReq &&isFMSUB_S ) begin
      //let x <- wr_fpu_ops.func( tuple5( sV1, sV2, sV3, rmd, FPMSub ));
      let x <- wr_fmaFP.func(  tuple4(Valid(negate(sV3)), sV1, sV2, rmd) );
      let fcsr = exception_to_fcsr(tpl_2(x));
      resultR     <= tagged Valid (tuple2 (pack(tpl_1(x)), fcsr));
      //resultR.enq(tuple2 (pack(tpl_1(x)), fcsr));
   end
   
   else if( validReq && isFNMADD_S ) begin
      //let x <- wr_fpu_ops.func( tuple5( sV1, sV2, sV3, rmd, FPNMAdd ));
      let {fp,ex} <- wr_fmaFP.func(  tuple4(Valid(sV3),        sV1, sV2, rmd) );
      fp = negate(fp);
      let fcsr = exception_to_fcsr(ex);
      resultR     <= tagged Valid (tuple2 (pack(fp), fcsr));
      //resultR.enq(tuple2 (pack(fp), fcsr));
   end
   
   else if( validReq && isFNMSUB_S ) begin
      //let x <- wr_fpu_ops.func( tuple5( sV1, sV2, sV3, rmd, FPNMSub ));
      let {fp,ex} <- wr_fmaFP.func(  tuple4(Valid(negate(sV3)), sV1, sV2, rmd) );
      fp = negate(fp);
      let fcsr = exception_to_fcsr(ex);
      resultR     <= tagged Valid (tuple2 (pack(fp), fcsr));
      //resultR.enq(tuple2 (pack(fp), fcsr));
   end
   
   else if( validReq && isFDIV_S )begin
      //let x <- wr_fpu_ops.func(tuple5( sV1, sV2, ?, rmd, FPDiv));
      let x = dividerFP( tuple3(sV1, sV2, rmd ));
      let fcsr = exception_to_fcsr(tpl_2(x));
      resultR     <= tagged Valid (tuple2 (pack(tpl_1(x)), fcsr));
      //resultR.enq(tuple2 (pack(tpl_1(x)), fcsr));
   end
   
   else if( validReq && isFSQRT_S )begin
      //let x <- wr_fpu_ops.func(tuple5( sV1, ?, ?, rmd, FPSqrt));
      let x = squareRootFP( tuple2(sV1, rmd) );
      let fcsr = exception_to_fcsr(tpl_2(x));
      resultR     <= tagged Valid (tuple2 (pack(tpl_1(x)), fcsr));
      //resultR.enq(tuple2 (pack(tpl_1(x)), fcsr));
   end
   
   else if( validReq && isFSGNJ_S ) begin
      let r1 = FloatingPoint {  sign: sV2.sign
                        , exp:  sV1.exp
                        , sfd:  sV1.sfd};
      Bit #(FPLEN) res = pack (r1);
    
      resultR     <= tagged Valid (tuple2 (res, 0));
      //resultR.enq(tuple2 (res, 0));
   end
   else if( validReq && isFSGNJN_S )begin
      FloatingPoint#(8,23) r1 = FloatingPoint {sign: !sV2.sign,
                            exp:   sV1.exp,
                            sfd:   sV1.sfd};

      Bit #(FPLEN) res = pack (r1);
      resultR     <= tagged Valid (tuple2 (res, 0));
      //resultR.enq(tuple2 (res, 0));
      end
   else if(validReq && isFSGNJX_S ) begin
      FloatingPoint#(8,23) r1 = FloatingPoint {sign:  (sV1.sign != sV2.sign),
                            exp:   sV1.exp,
                            sfd:   sV1.sfd};
      Bit #(FPLEN) res = pack (r1);
      resultR     <= tagged Valid (tuple2 (res, 0));
      //resultR.enq(tuple2 (res, 0));
      end
   else if(validReq && isFCVT_S_W ) begin
      Int#(32) v = unpack (truncate ( v1 ));
      match {.f, .e} = Tuple2#(FloatingPoint#(8,23), SSFloating_Point::Exception)'(vFixedToFloat( v, 6'd0, rmd));
      Bit #(FPLEN) res = pack ( f );
      let fcsr = exception_to_fcsr(e);
      resultR     <= tagged Valid (tuple2 (res, fcsr));
      //resultR.enq(tuple2 (res, fcsr));
      end
    else if( validReq &&isFCVT_S_WU )begin
      UInt#(32) v = unpack (truncate ( v1 ));
      match {.f, .e} = Tuple2#(FloatingPoint#(8,23), SSFloating_Point::Exception)'(vFixedToFloat( v, 6'd0, rmd));
      Bit #(FPLEN) res = pack ( f );
      let fcsr = exception_to_fcsr(e);
      resultR     <= tagged Valid (tuple2 (res, fcsr));
      //resultR.enq(tuple2 (res, fcsr));
      end
    else if( validReq && isFCVT_W_S ) begin
      FloatingPoint#(8,23) f = sV1;
      match {.v, .e} = Tuple2#(Int#(32),SSFloating_Point::Exception)'(vFloatToFixed( 6'd0, f, rmd ));

      // Handle infinity and NaNs
      if (   (isNaN (f))
          || (!(f.sign) && (isInfinity (f)))) 
         v = (1<<31) - 1;

      Bit #(FPLEN) res = signExtend (pack (v));
      let fcsr = exception_to_fcsr(e);
      resultR     <= tagged Valid (tuple2 (res, fcsr));
      //resultR.enq(tuple2 (res, fcsr));
      end
      
     else if(validReq && isFCVT_WU_S ) begin
      FloatingPoint#(8,23) f = sV1;

      // Handle negative operands separately. Pass the absolute value to the
      // converter
      FloatingPoint#(8,23) absf = f; absf.sign = False;
      match {.v, .e} = Tuple2#(UInt#(32),SSFloating_Point::Exception)'(vFloatToFixed( 6'd0, absf, rmd ));

      // Extra work if the operand is negative. The original convert function
      // prioritizes the sign of the operand before inexact checks
      if (f.sign) begin
         // Result is zero
         v = 0;
         // No exceptions were signalled, signal invalid exception, otherwise
         // let original exception remain
         if (pack (e) == 0)
            e.invalid_op = True;
      end

      // Handle infinity and NaNs
      if (   (isNaN (f))
          || (!(f.sign) && (isInfinity (f)))) 
         v = 32'hffffffff;

      // This is meant for the GPR. If the GPR is 64-bit, the 32-bit result is
      // stored, sign-extended as per the v2.2 of the spec
      Bit #(FPLEN) res = signExtend(pack (v));
      let fcsr = exception_to_fcsr(e);
      resultR     <= tagged Valid (tuple2 (res, fcsr));
      //resultR.enq(tuple2 (res, fcsr));
      end
    
    else if( validReq && isFMIN_S ) begin
      Bit #(FPLEN) res = ?;
      let rs1IsPos0 = fv_FDataIsPositiveZero (sV1);
      let rs2IsPos0 = fv_FDataIsPositiveZero (sV2);
      let rs1IsNeg0 = isNegativeZero (sV1);
      let rs2IsNeg0 = isNegativeZero (sV2);
      // One or both of the values are NaNs
      if ( isSNaN (sV1) && isSNaN (sV2) )
         res = pack ( canonicalNaN32 );
      else if ( isSNaN (sV1) )
         res = pack ( sV2 );
      else if ( isSNaN (sV2) )
         res = pack ( sV1 );
      else if ( isQNaN (sV1) && isQNaN (sV2) )
         res = pack ( canonicalNaN32 );
      else if ( isQNaN (sV1) )
         res = pack ( sV2 );
      else if ( isQNaN (sV2) )
         res = pack ( sV1 );
      else if ( rs1IsNeg0 && rs2IsPos0 )
         res = pack ( sV1 );
      else if ( rs2IsNeg0 && rs1IsPos0 )
         res = pack ( sV2 );
      else
         res = (cmpres_s == LT) ? pack (sV1)
                                : pack (sV2);

      // flag generation
      SSFloating_Point::Exception e = defaultValue;
      if ( isSNaN (sV1) || isSNaN (sV2) ) e.invalid_op = True;
      let fcsr = exception_to_fcsr(e);

      resultR     <= tagged Valid (tuple2 (res, fcsr));
      //resultR.enq(tuple2 (res, fcsr));
      end
   
    else if( validReq && isFMAX_S ) begin
      Bit #(FPLEN) res = ?;
      let rs1IsPos0 = fv_FDataIsPositiveZero (sV1);
      let rs2IsPos0 = fv_FDataIsPositiveZero (sV2);
      let rs1IsNeg0 = isNegativeZero (sV1);
      let rs2IsNeg0 = isNegativeZero (sV2);

      // One or both of the values are NaNs
      if ( isSNaN (sV1) && isSNaN (sV2) )
         res = pack ( canonicalNaN32 );
      else if ( isSNaN (sV1) )
         res = pack ( sV2 );
      else if ( isSNaN (sV2) )
         res = pack ( sV1 );
      else if ( isQNaN (sV1) && isQNaN (sV2) )
         res = pack ( canonicalNaN32 );
      else if ( isQNaN (sV1) )
         res = pack ( sV2 );
      else if ( isQNaN (sV2) )
         res = pack ( sV1 );
      else if ( rs1IsNeg0 && rs2IsPos0 )
         res = pack ( sV2 );
      else if ( rs2IsNeg0 && rs1IsPos0 )
         res = pack ( sV1 );
      else
         res = (cmpres_s == LT) ? pack (sV2)
                                : pack (sV1);

      // flag generation
      SSFloating_Point::Exception e = defaultValue;
      if ( isSNaN (sV1) || isSNaN (sV2) ) e.invalid_op = True;
      let fcsr = exception_to_fcsr(e);

      resultR     <= tagged Valid (tuple2 (res, fcsr));
      //resultR.enq(tuple2 (res, fcsr));
      end
   
   else if(validReq &&  isFMV_W_X ) begin
      Bit #(FPLEN) res = pack ( v1 );
      resultR     <= tagged Valid (tuple2 (res, 0));
      //resultR.enq(tuple2 (res, 0));
      end
   
   else if(validReq &&  isFMV_X_W ) begin
      // The FMV treats the data in the FPR and GPR as raw data and does not
      // interpret it. So for this instruction we use the raw bits coming from
      // the FPR
      Bit #(FPLEN) res = signExtend ( v1[31:0] );

      resultR     <= tagged Valid (tuple2 (res, 0));
      //resultR.enq(tuple2 (res, 0));
      end
 
   else if( validReq && isFEQ_S ) begin
      // Generate the results
      Bit #(FPLEN) res = ?;
      
      if (  isSNaN (sV1)
         || isSNaN (sV2)
         || isQNaN (sV1)
         || isQNaN (sV2)) res = 0;
      else
         res = (cmpres_s == EQ) ? 1 : 0; 

      // Generate the flags
      SSFloating_Point::Exception e = defaultValue;
      if (isSNaN(sV1) || isSNaN(sV2)) e.invalid_op = True;
      let fcsr = exception_to_fcsr(e);

      resultR     <= tagged Valid (tuple2 (res, fcsr));
      //resultR.enq(tuple2 (res, fcsr));
      end
   
   else if( validReq &&isFLT_S ) begin
      // Generate the results
      Bit #(FPLEN) res = ?;
      
      if (  isSNaN (sV1)
         || isSNaN (sV2)
         || isQNaN (sV1)
         || isQNaN (sV2)) res = 0;
      else
         res = (cmpres_s==LT) ? 1 : 0;

      // Generate the flags
      SSFloating_Point::Exception e = defaultValue;
      if (isNaN(sV1) || isNaN(sV2)) e.invalid_op = True;
      let fcsr = exception_to_fcsr(e);

      resultR     <= tagged Valid (tuple2 (res, fcsr));
      //resultR.enq(tuple2 (res, fcsr));
      end
   
   else if( validReq && isFLE_S ) begin
      // Generate the results
      Bit #(FPLEN) res = ?;
      
      if (  isSNaN (sV1)
         || isSNaN (sV2)
         || isQNaN (sV1)
         || isQNaN (sV2)) res = 0;
      else
         res = ((cmpres_s==LT) || (cmpres_s==EQ)) ? 1 : 0;

      // Generate the flags
      SSFloating_Point::Exception e = defaultValue;
      if (isNaN(sV1) || isNaN(sV2)) e.invalid_op = True;
      let fcsr = exception_to_fcsr(e);

      resultR     <= tagged Valid (tuple2 (res, fcsr));
      //resultR.enq(tuple2 (res, fcsr));
      end
   
   else if( validReq && isFCLASS_S ) begin
      Bit #(FPLEN) res = 1;
      if (isNaN(sV1)) begin
	 res = isQNaN(sV1) ? (res << 9) : (res << 8);
      end
      else if (isInfinity(sV1)) begin
	 res = sV1.sign ? res        : (res << 7);
      end
      else if (isZero(sV1)) begin
	 res = sV1.sign ? (res << 3) : (res << 4);
      end
      else if (isSubNormal(sV1)) begin
	 res = sV1.sign ? (res << 2) : (res << 5);
      end
      else begin
	 res = sV1.sign ? (res << 1) : (res << 6);
      end

      resultR     <= tagged Valid (tuple2 (res, 0));
      //resultR.enq(tuple2 (res, 0));
     end

   endrule


   method Action req (
        Opcode    opcode
      , Bit #(7)  funct7
      , Bit #(3)  rounding_mode
      , Bit #(5)  rs2_name
      , Bit #(FPLEN) val1
      , Bit #(FPLEN) val2
      , Bit #(FPLEN) val3
   ) if (!isValid(requestR));
      // Legal instruction
      requestR <= tagged Valid (tuple7 (opcode, funct7, rs2_name, rounding_mode, val1, val2, val3));
      //requestR.enq(tuple7 (opcode, funct7, rs2_name, rounding_mode, val1, val2, val3));
         `ifdef verbose $display("FPU %b %b %b %b %h %h %h", opcode, funct7, rs2_name, rounding_mode, val1, val2, val3); `endif

      // Start processing the instruction
      resultR  <= tagged Invalid;
   endmethod

   // MBox interface: response


   method ActionValue#(Tuple2#(Bit#(FPLEN), Bit#(5))) word if (isValid(resultR));
      //resultR.deq;
      //return resultR.first;
      requestR <= tagged Invalid;
      return resultR.Valid;
   endmethod

endmodule

// ================================================================

endpackage
