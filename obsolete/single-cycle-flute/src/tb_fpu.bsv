package tb_fpu;

  import fpu::* ;
  
  module mktb_fpu();
    
    Ifc_fpu fbox <- mkfpu();
    Reg#(int) cycle <- mkReg(0);
    
    rule count_cycle;
      cycle <= cycle + 1;
    endrule
    
    rule fbox_req(cycle == 1);
      Bit#(32) op1 = 'h4e0a72d8;
      Bit#(32) op2 = 'h3f95c008;
      Bit#(32) op3 = 'h3f800000;      
      fbox.req(7'b1010011, 7'h2C, 3'b000, 5'b00000, op1, op2, op3);
    endrule
    
    rule fbox_resp;
      let x <- fbox.word;
      $display("%d %b %b", cycle, tpl_1(x),tpl_2(x));
      $finish;
    endrule
    
  endmodule
  
endpackage
